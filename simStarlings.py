#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
from numba import jit
#import params
import pickle
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import starlings
import datetime



def thProbFX(tt, debug = False):
    tt2 = np.exp(tt)
    tt3 = tt2/np.sum(tt2)
    return(tt3)

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

@jit
def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

@jit
def getPrevCount(birdLoc_2D, prevCellCount, maxN, nsession):
    """
    get intial cell count
    """
    for i in range(1, nsession):
        for cc in range(maxN):
            if birdLoc_2D[(i-1), cc] > 0:
                prevCellCount[cc, i] += 1.0
    return prevCellCount


def matrixsub(arr1, arr2):
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat


def multinomial_pmf(probs, counts):
    probssum = probs.sum()
    if probssum < 0.999 or probssum > 1.0001:
        raise ValueError("probs must sum to 1")
    if probs.size != counts.size:
        raise ValueError("probs and counts must be the same size")
    return gammaln(counts.sum() + 1.0) - gammaln(counts + 1.0).sum() + np.sum(np.log(probs)*counts)


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    """
    e_num = np.exp(-2*rho)
    e_denom = 2 * np.exp(-rho)
    sinh_rho = (1 - e_num) / e_denom
    cosh_rho = (1 + e_num) / e_denom
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc

def calcRelWrapCauchy(wrp_rpara, nsession, uYearIndx, daypi, yearRecruitIndx):
    """
    Calc the rel cauchy value for all sessions for distributing recruits
    """
    relWrpCauchy = np.zeros(nsession)
    for i in uYearIndx:
        # day pi in year i
        daypiTmp = daypi[yearRecruitIndx == i]
        # pdf of daypi in year i
        dc = dwrpcauchy(daypiTmp, wrp_rpara[0], wrp_rpara[1])
        # mask of recruit window in year i
        reldc = dc / np.sum(dc)     #  dc.sum()
        relWrpCauchy[yearRecruitIndx == i] = reldc
    return relWrpCauchy


##############################################################
##############################################################
#####
##
class Params(object):
    def __init__(self):
        """
        parameter class for simulations
        """
        # number of iterations to simulate
        self.iter = 1
        # number of habitat covariates
        self.ncov = 3
        # suppresion threshold to stay below
        self.popThreshold = 20
        # set days on which to base population growth rate
        self.reproDays = range(365-92, 365-60)         # 1 Oct - 31 Oct
        #########   SET THE SCENARIO    ######################
        self.scenario = 1
        print('########## Scenario: ', self.scenario)



##############################################################
##############################################################
class BasicData(object):
    def __init__(self, params, simindataobj, starlingpath):
        """
        Object to read in data and run simulation
        """
        self.starlingpath = starlingpath
        self.params = params
        ###################
        # Run Functions
        self.getGibbsArrays(simindataobj)
        self.getDateTemplate()

        if self.scenario == 1:
            self.readScenario1Data()



    def getGibbsArrays(self, simindataobj):
        """
        get parameter values for each iteration
        """
        self.ndat = len(simindataobj.siggibbs)
        self.sampID = np.random.choice(range(self.ndat), self.params.iter, replace = True)
        # get starting N - Nov 2014
        nn = np.shape(simindataobj.Ngibbs)
        self.NStart = simindataobj.Ngibbs[self.sampID, -1]

        self.bgibbs = simindataobj.bgibbs
        self.bPrevgibbs = simindataobj.bPrevgibbs
        self.rgibbs = simindataobj.rgibbs
        self.rparagibbs = simindataobj.rparagibbs
#        self.igibbs = simindataobj.igibbs
        self.siggibbs = simindataobj.siggibbs
        self.g0Obsgibbs = simindataobj.g0Obsgibbs
        self.g0ObsWrpCgibbs = simindataobj.g0ObsWrpCgibbs
        self.g0Trapgibbs = simindataobj.g0Trapgibbs
        self.g0TrapWrpCgibbs = simindataobj.g0TrapWrpCgibbs
        self.pKillgibbs = simindataobj.pKillgibbs
        self.g0KillWrpCgibbs = simindataobj.g0KillWrpCgibbs
        self.Ngibbs = simindataobj.Ngibbs
#        self.sumN = simindataobj.sumN
#        self.popIndx = simindataobj.popIndx
        self.reproPopgibbs = simindataobj.reproPopgibbs
        # habitat data for simulation
        self.xdat = simindataobj.xdat
        self.cellX = simindataobj.cellX
        self.cellY = simindataobj.cellY

    def getDateTemplate(self):
        """
        get daily julian day from zero on, and julian year
        Start  is 1 Feb 2013, and end is 30 October 2022 - 10 years
        """
        self.startDate = datetime.date(2013, 2, 1)
        self.endDate = datetime.date(2022, 10, 30)
        self.datesFull = self.startDate
        timetup = self.startDate.timetuple()
        self.julianYear = timetup.tm_yday
        self.julian = 0
        date_i = self.startDate
        jul_i = 0
        carryOn = 1
        while carryOn == 1:
            date_i = date_i + datetime.timedelta(days=1)
            timeTup = date_i.timetuple()
            self.julianYear = np.append(self.julianYear, timeTup.tm_yday)
            jul_i += 1
            self.julian = np.append(self.julian, jul_i) 
            if self.params.scenario == 1:




            if date_i == self.endDate:
                carryOn = 0
        self.daypi = self.julianYear / 365.0 * 2.0 * np.pi


    #############################
    #############################       scenario 1 functions
    def readScenario1Data(self):
        """
        if scenario1 read in scenario 1 details
        """
        scenario1File = os.path.join(self.starlingpath,'currentScenario.csv')
        self.detailsDat = np.genfromtxt(scenario1File,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8'])
        self.septDays = self.detailsDat['septDays']
        self.septTraps = self.detailsDat['septTraps']
        self.septSurveys = self.detailsDat['septSurveys']
        self.septFte = self.detailsDat['septFte']
        self.febDays = self.detailsDat['febDays']
        self.febTraps = self.detailsDat['febTraps']
        self.febSurveys = self.detailsDat['febSurveys']
        self.febFte = self.detailsDat['febFte']


    def makeScenario1Intervention(self):
        """
        make scenario1 masks and intervention effor arrays
        """
        ndays = len(self.septDays)
        startFeb





























class Simulate(object):
    def __init__(self, params, basicdata):
        """
        Class and functions to simulate population and trapping dynamics
        """
        ################
        # Call functions
#        self.wrapperSimul(params, basicdata)




class Results(object):
    def __init__(self, params, basicdata, simulate, starlingpath):
        """
        Class and functions to process results
        """
         ################
        # Call functions
        self.wrapperResults(params, basicdata, simulate, starlingpath)

        ################
    def wrapperResults(self, params, basicdata, simulate, starlingpath):
        """
        wrapper function to call simulate functions
        """
        # instances of classes
        self.params = params
        self.basicdata = basicdata
        self.simulate = simulate
        self.starlingpath = starlingpath
 


########            Main function
#######
def main():
    # paths and data to read in
    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')

    # initiate
    simInData = os.path.join(starlingpath, 'out_simInData.pkl')
    fileobj = open(simInData, 'rb')
    simindataobj = pickle.load(fileobj)
    fileobj.close()

    print('sim1', simindataobj.siggibbs[0:3])

    params = Params()

    basicdata = BasicData(params, simindataobj, starlingpath)

    simulate = Simulate(params, basicdata)

    results = Results(params, basicdata, simulate, starlingpath)



if __name__ == '__main__':
    main()





