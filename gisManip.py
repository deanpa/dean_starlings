#!/usr/bin/env python



import os
import numpy as np
from rios import applier
#from scipy import ndimage
#from numba import autojit


def sumInCell(infile, outfile):
    """
    Object to read in and calculate neighbourhood statistics
    """
    # Set up input and output filenames. 
    infiles = applier.FilenameAssociations()
    infiles.image1 = infile

    outfiles = applier.FilenameAssociations()
    outfiles.outimage = outfile
    # see controls page on Rios
    controls = applier.ApplierControls()
    controls.drivername = "GTiff"
    controls.windowxsize = 40
    controls.windowysize = 40
    applier.apply(rios_sumInCell, infiles, outfiles, controls=controls)

def rios_sumInCell(info, inputs, outputs):
    """

    """
    out_sum = np.sum(inputs.image1[0])
    outputs.outimage = np.empty_like(inputs.image1)
    outputs.outimage.fill(out_sum) 

########            Main function
#######
def main():
    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')
    # paths and data to read in
    starlingVegFile = os.path.join(starlingpath,'swamps_R50_gda94.tif')
    outTiff = os.path.join(starlingpath,'swamps_sum_R50.tif')
    print(outTiff)

    sumInCell(starlingVegFile, outTiff)
    

if __name__ == '__main__':
    main()

