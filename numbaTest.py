#!/usr/bin/env python

from scipy import stats
import numpy as np
from numba import jit

def wrapper():
    """
    create data and run numba fx
    """
    a = 1   # np.arange(28).reshape(7,4)
    b = 1
    p = .5
    ii = 40
    c = np.zeros(ii, dtype = int)
#    c = numbaFX(a, b, p, ii)
#    print('c', c)

    e = 25.0
    f = 2.0
    g = np.empty(ii)
#    g = numbaFX2(e,f, g, ii)
#    print('g', g)

    a3d = np.arange(36).reshape(3,4,3)
#    a2d = a3d[0] * 0.0
    dim0 = np.shape(a3d)[0]
    (a2d, sa, b2d) = numbaFX3(a3d, dim0)
    print('a2d', a2d, 'sa', sa, 'b2d', b2d)

@jit
def numbaFX3(a3d, dim0):
    for i in range(dim0):
#        a2d = a3d[0] * 0.0
        a2d = a3d[i] *2
        sa = np.sum(a2d)
        b2d = a2d * 1
        b2d[0,0] = 99
        #print(a2d)
    return(a2d, sa, b2d)



@jit
def numbaFX(a, b, p, ii):
    c = np.empty(ii)
    for i in range(ii):
        r = np.random.binomial(3, p)     # np.array([2,5])     #
        c[i] = r 
    return(c)


@jit
def numbaFX2(e, f, g, ii):

    for i in range(ii):
        ff = i * 2
        r = np.greater(e, ff)     # np.array([2,5])     #
        if r:
            g[i] = ff
        else:
            g[i] = e 
#    del r
    return(g)





wrapper()


