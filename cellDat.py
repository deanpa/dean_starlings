#!/usr/bin/env python

import os
import numpy as np
from rios import applier
import pickle
#from numba import autojit



def getxy(infile, outPickle):
    """
    Object to read in and calculate neighbourhood statistics
    """
    # Set up input and output filenames. 
    infiles = applier.FilenameAssociations()
    infiles.image1 = infile

    outfiles = applier.FilenameAssociations()
    # see controls page on Rios
    controls = applier.ApplierControls()
    controls.drivername = "GTiff"

    # Passing other data wiki
    otherargs = applier.OtherInputs()
    otherargs.xdat = np.array([])
    otherargs.ydat = np.array([])

    applier.apply(rios_getxy, infiles, outfiles, otherargs, controls=controls)

    fileobj = open(outPickle, 'wb')
    pickle.dump(otherargs, fileobj)
    fileobj.close()



def rios_getxy(info, inputs, outputs, otherargs):
    """

    """
    xBlock, yBlock = info.getBlockCoordArrays()
    xdat = xBlock[inputs.image1[0] == 1]
    ydat = yBlock[inputs.image1[0] == 1]
    otherargs.xdat = np.append(otherargs.xdat, xdat)
    otherargs.ydat = np.append(otherargs.ydat, ydat)

########            Main function
#######
def main():
    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')
    # paths and data to read in
    maskFile = os.path.join(starlingpath,'extentMask.tif')
    outPickle = os.path.join(starlingpath,'xydat.pkl')

    getxy(maskFile, outPickle)
    

if __name__ == '__main__':
    main()

