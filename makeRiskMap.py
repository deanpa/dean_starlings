#!/usr/bin/env python

import os
import numpy as np
import gdal
import pickle


# projection definition
GDA94_WKT = 'PROJCS["GDA94_MGA_zone_50",GEOGCS["GCS_GDA_1994",DATUM["Geocentric_Datum_of_Australia_1994",SPHEROID["GRS 1980",6378137,298.2572221010002,AUTHORITY["EPSG","7019"]],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0],UNIT["degree",0.0174532925199433]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",117],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],UNIT["metre",1,AUTHORITY["EPSG","9001"]]]'

class MakeMap(object):
    def __init__(self, starlingpath, starlingdata):
        """
        Object to read in, manipulate and read out data.
        """
        ###########################################################
        # Run makemap functions
        self.defineParameters(starlingpath)
        self.readData(starlingdata)     
        self.makeNorthings()
        self.riskCalc()
#        self.makeEffortRemove()

        self.writeTif(self.riskRaster, self.riskFName)
#        self.writeTif(self.effortRast, self.effortFName)
#        self.writeTif(self.removeRast, self.removeFName)
#        self.writeTif(self.birdobserved, self.birdObsFName)
        ##########################################################

    def defineParameters(self, starlingpath):
        """
        set parameters
        """
        self.betas = np.array([-1.75, 0.581])
        self.starlingpath = starlingpath
        (self.xmin, self.ymin, self.xmax,
                self.ymax) = [774000., 6222000., 1082000., 6376000.]
        self.resol = 500.0
        self.nrows = np.int((self.ymax - self.ymin) / self.resol)
        self.ncols = np.int((self.xmax - self.xmin) / self.resol)
        self.match_geotrans = [self.xmin, self.resol, 0., self.ymax, 0.,
                                -self.resol]
        

    def readData(self, starlingdata):
        """
        read in extent mask, swamp and native veg covariates
        """
#        self.starlingdata = starlingdata
        extentTifFname = os.path.join(self.starlingpath,'extentMaskR500.tif')
        natVegTifFname = os.path.join(self.starlingpath,'NatVeg_mean_R500_1KmSearch.tif')
        swampTifFname = os.path.join(self.starlingpath,'swamps_mean_R500_1KmSearch.tif')
        self.extMask = gdal.Open(extentTifFname).ReadAsArray()
        self.natVeg2D = gdal.Open(natVegTifFname).ReadAsArray()
        self.swamp2D = gdal.Open(swampTifFname).ReadAsArray()
        self.surveyObs = starlingdata.surveyObs
        self.surveyRemoved = starlingdata.surveyRemoved
        self.surveyX = starlingdata.surveyX
        self.surveyY = starlingdata.surveyY
        self.trapRemoved = starlingdata.trapRemoved
        self.trapX = starlingdata.trapX
        self.trapY = starlingdata.trapY

    def makeNorthings(self):
        """
        make raster of northings
        """
        self.northings = np.zeros((self.nrows, self.ncols))
        for i in range(self.nrows):
            north_i = self.ymax - (i * self.resol)
            self.northings[i] = north_i
#            print('north_i', self.northings[i, 155], 'swamp', self.swamp2D[i, 155])

    def riskCalc(self):
        """
        make risk map 
        """
        # make scale swamp
        self.riskFName = 'riskMap500.tif'
        meanSwamp = np.mean(self.swamp2D[self.extMask == 1])
        sdSwamp = np.std(self.swamp2D[self.extMask == 1])
        self.scaleSwamp = np.zeros((self.nrows, self.ncols))
        self.scaleSwamp[self.extMask == 1] = (self.swamp2D[self.extMask == 1] - meanSwamp) / sdSwamp
        # make scale northings
        meanNorth = np.mean(self.northings[self.extMask == 1])
        sdNorth = np.std(self.northings[self.extMask == 1]) 
        print('sdnorth', sdNorth)
        self.scaleNorth = np.zeros((self.nrows, self.ncols))
        self.scaleNorth[self.extMask == 1] = (self.northings[self.extMask == 1] - meanNorth) / sdNorth  
        self.riskRaster = (self.scaleSwamp * self.betas[1]) + (self.scaleNorth * self.betas[0])
        self.riskRaster[self.extMask == 0] = -9999
#        for i in range(self.nrows):
#            print('north_i', self.northings[i, 155], 'scaleNorth', self.scaleNorth[i, 155], 
#                'swamp', self.scaleSwamp[i, 155],
#                'risk', self.riskRaster[i, 155])
    
    def makeEffortRemove(self):
        """
        make rasters of effort and numbers removed
        """
        self.effortFName = 'effortMap.tif'
        self.removeFName = 'removeMap.tif'
        self.birdObsFName = 'birdObsMap.tif'

        self.effortRast = np.zeros((self.nrows, self.ncols))
        self.removeRast = np.zeros((self.nrows, self.ncols))
        self.birdobserved = np.zeros((self.nrows, self.ncols))
        ntrap = len(self.trapX)
        nsurvey = len(self.surveyX)
        nDiff = nsurvey - ntrap
        # loop thru traps
        for i in range(ntrap):
            xmask = (self.trapX[i] > (self.xmin - (self.resol/2.0))) &  (self.trapX[i] < (self.xmax + (self.resol/2.0)))
            ymask = (self.trapY[i] > (self.ymin - (self.resol/2.0))) &  (self.trapY[i] < (self.ymax + (self.resol/2.0)))
            if xmask & ymask:
                (col, row) = self.getColsAndRows(self.trapX[i], self.trapY[i])
                self.effortRast[row, col] += 1.0
                self.removeRast[row, col] += self.trapRemoved[i]
            # loop thru surveys
        for i in range(nsurvey):
            xmask = (self.surveyX[i] > (self.xmin - (self.resol/2.0))) &  (self.surveyX[i] < (self.xmax + (self.resol/2.0)))
            ymask = (self.surveyY[i] > (self.ymin - (self.resol/2.0))) &  (self.surveyY[i] < (self.ymax + (self.resol/2.0)))
            if xmask & ymask:
                (col, row) = self.getColsAndRows(self.surveyX[i], self.surveyY[i])
                self.effortRast[row, col] += 1.0
                self.removeRast[row, col] += self.surveyRemoved[i]
                if self.surveyObs[i] > 0:
                    self.birdobserved[row, col] += 1.0
        print('maxEffort', np.max(self.effortRast))
        print('maxRemoved', np.max(self.removeRast))
        print('maxbirdObs', np.max(self.birdobserved))


    def getColsAndRows(self, easting, northing):
        """
        given an array of eastings and an array of northings
        return the cols and rows
        """
        col = np.round((easting - self.xmin) / self.resol).astype(np.int)
        row = np.round((self.ymax - northing) / self.resol).astype(np.int)
        return(col, row)




    def writeTif(self, raster, fName):
        """
        write tif to directory
        """
        #########      write array to tif in directory
        dst_filename = os.path.join(self.starlingpath, fName)
        dst = gdal.GetDriverByName('GTiff').Create(dst_filename, self.ncols,
                    self.nrows, 1, gdal.GDT_Float32)
        dst.SetGeoTransform( self.match_geotrans )
        dst.SetProjection( GDA94_WKT )
        band = dst.GetRasterBand(1)
        NoData_value = -9999                # add no data
        band.SetNoDataValue(NoData_value)   # add no data
        band.WriteArray(raster)
        del dst # Flush

########            Main function
#######
def main():
    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')

    starlingTrapFile = os.path.join(starlingpath,'out_manipStarling.pkl')
    fileobj = open(starlingTrapFile, 'rb')
    starlingdata = pickle.load(fileobj)
    fileobj.close()
 


    # initiate rawdata class and object
    makemap = MakeMap(starlingpath, starlingdata)



if __name__ == '__main__':
    main()

