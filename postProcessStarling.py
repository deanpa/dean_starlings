#!/usr/bin/env python


#from cats import Gibbs # need this because of pickled results from mcmc
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import datetime
import params
#from starlings import StarlingData

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    """
    e_num = np.exp(-2*rho)
    e_denom = 2 * np.exp(-rho)
    sinh_rho = (1 - e_num) / e_denom
    cosh_rho = (1 + e_num) / e_denom
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc





class ResultsProcessing(object):
    def __init__(self, gibbsobj, params, starlingpath):

        self.params = params
        self.starlingpath = starlingpath
        # get gibbs results
        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)
        self.results = np.hstack([self.gibbsobj.bgibbs, 
                                    np.expand_dims(self.gibbsobj.bPrevgibbs,1), 
                                    np.expand_dims(self.gibbsobj.rgibbs,1), 
                                    self.gibbsobj.rparagibbs,
#                                    np.expand_dims(self.gibbsobj.igibbs,1), 
                                    np.expand_dims(self.gibbsobj.siggibbs,1),
                                    np.expand_dims(self.gibbsobj.g0Obsgibbs,1),
                                    self.gibbsobj.g0ObsWrpCgibbs,
                                    np.expand_dims(self.gibbsobj.g0Trapgibbs,1), 
                                    self.gibbsobj.g0TrapWrpCgibbs,
                                    np.expand_dims(self.gibbsobj.pKillgibbs,1), 
                                    self.gibbsobj.g0KillWrpCgibbs])
        self.npara = self.results.shape[1]
        print('npara', self.npara)
        print('sigma shp', np.shape(self.gibbsobj.siggibbs))
        ######  Run functions
        self.runExploreFunctions()

#class ExploreTrap(object):
#    def __init__(self, starlingtrapdata, starlingpath):
#        self.runExploreFunctions(starlingtrapdata, starlingpath)
    
        #######

    ###### 
    # Functions

    def runExploreFunctions(self):
        """
        wrapper function to run functions in the explore class
        """
        # Run functions
        self.getFullDateArrays()                # get array full date arrays
        self.dataArraysFull()
        self.makeObsHuntDates()
#        self.plotEffortFX()
        self.makeTableFX()
        self.writeToFileFX()
#        self.plotFX()
        self.getNPred_Recruits()
        self.movingAveN()
        self.plotPop_Remove()
#        self.plotrecruits()

        self.plotRelWrpC()
    ##########################################  Functions

    def getFullDateArrays(self):
        """
        get arrays with all days for plotting
        """
        fullJul = np.append(self.gibbsobj.trapJul, self.gibbsobj.surveyJul)
        fullMon = np.append(self.gibbsobj.trapMon, self.gibbsobj.surveyMon)
        fullYear = np.append(self.gibbsobj.trapYear, self.gibbsobj.surveyYear)
        fullDay = np.append(self.gibbsobj.trapDay, self.gibbsobj.surveyDay)
        self.minJul = np.min(fullJul).astype(int)
        self.minYear = fullYear[fullJul == self.minJul].astype(int)
        self.minMon = fullMon[fullJul == self.minJul].astype(int)
        self.minDay = fullDay[fullJul == self.minJul].astype(int)
        if len(self.minYear > 1):
            self.minYear = self.minYear[0]
            self.minMon = self.minMon[0]
            self.minDay = self.minDay[0]
        maxJul = np.max(fullJul).astype(int)
        maxYear = fullYear[fullJul == maxJul].astype(int)
        maxMon = fullMon[fullJul == maxJul].astype(int)
        maxDay = fullDay[fullJul == maxJul].astype(int)
        if len(maxYear > 1):
            maxYear = maxYear[0]
            maxMon = maxMon[0]
            maxDay = maxDay[0]
        self.minDate = datetime.date(self.minYear, self.minMon, self.minDay) - datetime.timedelta(days=5)
        self.maxDate = datetime.date(maxYear, maxMon, maxDay) + datetime.timedelta(days=6)
        self.fullDateArray = np.arange(self.minDate, self.maxDate)
        self.fullJulArray = np.arange((self.minJul-5), (maxJul+6))
        print('minDate:', datetime.date(self.minYear, self.minMon, self.minDay), 
                'maxDate:', datetime.date(maxYear, maxMon, maxDay))
     
    def makeObsHuntDates(self):
        """
        dates of sessions
        """
        self.sessionJul = np.unique(np.append(self.gibbsobj.surveyJul, self.gibbsobj.trapJul))
        self.sessMinDate = datetime.date(self.minYear, self.minMon, self.minDay) 
        print('sessJul', self.sessionJul[0:12])
        print(self.sessMinDate)
        self.sessDates = self.sessMinDate
        for i in range(1, self.gibbsobj.nsession):
            date_i = self.sessMinDate + datetime.timedelta(days=self.sessionJul[i])
            self.sessDates = np.append(self.sessDates, date_i)
        print('sess dates', self.sessDates[0:12])
        
        
        
    def dataArraysFull(self):
        """
        populate array for all days in full sequence: 
            n observed; n removed; persondays hunting; traps
        """
        self.nFullJul = len(self.fullJulArray)
        print('nFullJul', self.nFullJul)
        self.effortHunt = np.zeros(self.nFullJul)
        self.effortTrap = np.zeros(self.nFullJul)
        self.nObserved = np.zeros(self.nFullJul)
        self.removeHunt = np.zeros(self.nFullJul)
        self.removeTrap = np.zeros(self.nFullJul)
        # loop thru all jul days
        for i in range(self.nFullJul):
            # get n observed
            jul_i = self.fullJulArray[i]
            self.effortHunt[i] = len(self.gibbsobj.surveyJul[self.gibbsobj.surveyJul == jul_i])
            self.effortTrap[i] = len(self.gibbsobj.trapJul[self.gibbsobj.trapJul == jul_i])
            self.nObserved[i] = (np.sum(self.gibbsobj.surveyObs[self.gibbsobj.surveyJul == jul_i])) #+ 
#                        np.sum(self.gibbsobj.trapRemoved[self.gibbsobj.trapJul == jul_i]))
            self.removeHunt[i] = np.sum(self.gibbsobj.surveyRemoved[self.gibbsobj.surveyJul == jul_i])
            self.removeTrap[i] = np.sum(self.gibbsobj.trapRemoved[self.gibbsobj.trapJul == jul_i]) 
            self.noEffortMask = (self.effortHunt + self.effortTrap) == 0.0
        self.nObserved[self.noEffortMask] = np.nan
        self.removeHunt[self.noEffortMask] = np.nan
        self.removeTrap[self.noEffortMask] = np.nan
        self.totalEffort = self.effortHunt + self.effortTrap
#        self.totalEffort[self.noEffortMask] = np.nan
        minSurveyJul = np.min(self.gibbsobj.surveyJul)
        maxIndx = minSurveyJul + 75
        rangeArr = np.arange(minSurveyJul, maxIndx).astype(int)
        print('rangeArr', rangeArr)
        julMask = np.in1d(self.gibbsobj.surveyJul, rangeArr)
        print('nobs', self.gibbsobj.surveyObs[julMask], 'jul', self.gibbsobj.surveyJul[julMask])

    def getNPred_Recruits(self):
        """
        from gibbs results, predict Npred and number of recruits by session
        """
        self.getReproductivePopAllYears()        
        self.reproDataAllYears()
        self.NpredAll()

    def getReproductivePopAllYears(self):
        """
        Get reproductive pop for all years
        """
        self.nYear = len(self.gibbsobj.uYearIndx) 
        self.meanN = np.round((self.gibbsobj.sumN / self.ndat), 5)
        Npop = self.meanN.copy()
        self.reproPop = np.zeros(self.nYear)
        ## repro pop in year 0 
        self.reproPop[0] = Npop[0]
        ###################################################
        for i in self.gibbsobj.uYearIndx[1:]:
            nPopi = Npop[self.gibbsobj.reproPeriodIndx == i]
            rPop = np.mean(nPopi)
            self.reproPop[i] = rPop
        print('rPop', self.reproPop)

    def reproDataAllYears(self):
        """
        get number of recruits across years and session
        with specified growth parameter
        """
        self.meanR = np.mean(self.gibbsobj.rgibbs)
        self.meanRPara = np.mean(self.gibbsobj.rparagibbs, axis = 0)
        self.meanImm = 0.0  # np.mean(self.gibbsobj.igibbs)
        self.sessionRecruits = np.zeros(len(self.gibbsobj.yearRecruitIndx))
        self.totalRecruits = np.zeros(self.nYear)
        self.calcRelWrapCauchy()
        for i in self.gibbsobj.uYearIndx[1:]:
            self.totalRecruits[i] = (self.reproPop[i] * self.meanR) 
            relwc = self.recruitWrpCauchy[self.gibbsobj.yearRecruitIndx == i]
            sr = (self.totalRecruits[i] * relwc)
#            print('i', i, 'len sr', len(sr), 'len yr rec indx',
#                len(self.basicdata.sessionRecruits[self.basicdata.yearRecruitIndx == i]),
#                'sr', len(sr), 'len relwc', len(relwc))
            self.sessionRecruits[self.gibbsobj.yearRecruitIndx == i] = sr
        print('self.reproPop', self.reproPop, 'totRec', self.totalRecruits, 'sessRec', self.sessionRecruits[0:10])
        print('uYearIndx', self.gibbsobj.uYearIndx)
        print('self.gibbsobj.reproPeriodIndx', self.gibbsobj.reproPeriodIndx[0:5])
        print('yearRecruitIndx', self.gibbsobj.yearRecruitIndx[0:5])
#        print('sessRecruit', self.sessionRecruits[self.gibbsobj.yearRecruitIndx == 1])

        
    def calcRelWrapCauchy(self):
        """
        (7) Calc the rel cauchy value for all sessions for distributing recruits
        """
        self.recruitWrpCauchy = np.zeros(self.gibbsobj.nRecruitJulYear)
        for i in self.gibbsobj.uYearIndx[1:]:
            # day pi in year i
            daypiTmp = self.gibbsobj.daypiRecruit[self.gibbsobj.yearRecruitIndx == i]
            # pdf of daypi in year i
            dc = dwrpcauchy(daypiTmp, self.meanRPara[0], self.meanRPara[1])
            # mask of recruit window in year i
            reldc = dc / np.sum(dc)     #  dc.sum()
            self.recruitWrpCauchy[self.gibbsobj.yearRecruitIndx == i] = reldc



    def plotrecruits(self):
        P.figure()
        P.subplot(2,2,1)
        P.plot(self.sessDates, self.sessionRecruits[0:self.gibbsobj.nsession])
        P.subplot(2,2,2)
        P.plot(self.sessDates, self.recruitWrpCauchy[0:self.gibbsobj.nsession])
        P.subplot(2,2,3)
        P.scatter(self.gibbsobj.daypiRecruit, self.recruitWrpCauchy)
        P.subplot(2,2,4)
        P.plot(self.gibbsobj.daypiRecruit)
        P.show()



    def NpredAll(self):
        """
        calc Npred for all sessions
        """
        self.Npred = np.zeros(self.gibbsobj.nsession)
        self.Npred[0] = self.meanN[0] + 1.0
        self.Npred[1:] = self.meanN[:-1] - self.gibbsobj.removeDat[:-1]
        self.Npred[1:] = self.Npred[1:] + self.sessionRecruits[1 : self.gibbsobj.nsession]
        self.Npred[self.Npred < 0] = .5
    #########################

    def plotEffortFX(self):
        """
        plot n observed, effort, and n removed
        """
        P.figure(figsize=(14, 14))
        P.subplot(5,1,1)
        xDates = self.fullDateArray.astype(datetime.datetime)
        P.plot(xDates, self.nObserved, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.ylabel('Number observed', fontsize = 17)
        # graph effort
        P.subplot(5, 1, 2)
        P.plot(xDates, self.removeHunt, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.ylabel('Number shot', fontsize = 17)
        P.subplot(5, 1, 3)
        P.plot(xDates, self.removeTrap, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.ylabel('Number trapped', fontsize = 17)
        # plot effort
        P.subplot(5, 1, 4)
        P.plot(xDates, self.effortHunt, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.ylabel('Person-days', fontsize = 17)
        # plot effort trap
        P.subplot(5, 1, 5)
        P.plot(xDates, self.effortTrap, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.xlabel('Time (days within years)', fontsize = 17)
        P.ylabel('Trap-days', fontsize = 17)
        EffortFname = os.path.join(self.starlingpath,'starlingEffort.png')
        P.savefig(EffortFname, format='png')

        P.show() 
  
    ###############################  GIBBS RESULTS

    @staticmethod
    def quantileFX(a):
        return mquantiles(a, prob=[0.025, 0.5, 0.975], axis = 0)


    def makeTableFX(self):
        resultTable = np.zeros(shape = (4, self.npara))
        resultTable[0:3, :] = np.round(self.quantileFX(self.results), 3)
        resultTable[3, :] = np.round(np.mean(self.results, axis = 0), 3)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])
        self.names = np.array(['Northings', 'Swamp', 'NaturalVeg', 'PrevOccupied', 'PopGrowth',
                'recruit_WrpC_mu', 'recruit_WrpC_rho', 'Sigma', 'g0_Observed', 
                'g0_Obs_WrpC_mu', 'g0_Obs_WrpC_rho', 'g0_Trap', 'g0_Trap_WrpC_mu', 
                'g0_Trap_WrpC_rho', 'Prob_shoot', 'g0_Shoot_WrpC_mu', 'g0_Shoot_WrpC_rho'])
        for i in range(self.npara):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()


    ########            Write result table to file
    ########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
        structured['Names'] = self.names
        tableFname = os.path.join(self.starlingpath, 'summaryTable_Starling_M1.txt')
        np.savetxt(tableFname, structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f'],
                    header='Names Low_CI Median High_CI Mean')

    def plotFX(self):
        plotIndx = np.arange((self.npara + 6), dtype = int)
        nplots = len(plotIndx)  # + 1
        cc = 0
        nfigures = np.int(np.ceil(nplots/6.0))
        ng = np.arange(self.ndat, dtype=int)
#        print(ng)
        #print nfigures, self.ncov
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            #print lastFigure
            for j in range(6):
                P.subplot(2,3,j+1)
                #print cc
                if cc < nplots:
                    if cc < 6:
                        P.plot(ng, self.gibbsobj.Ngibbs[:, cc])
                        P.title('Pop size session ' + str(self.gibbsobj.popIndx[cc]))
                    else:
                        pindx = cc - 6
                        P.plot(ng, self.results[:, plotIndx[pindx]])
                        P.title(self.names[pindx])
                cc = cc + 1
#                if cc == nplots:
#                    P.subplot(2,3, j+2)
#                    P.scatter(self.meanN, self.nPred[0:self.gibbsobj.nsession])
#                    P.ylim(0, 90)
#                    P.xlim(0, 90)
            P.show(block=lastFigure)

    def plotPop_Remove(self):
        """
        plot mean pred population size, recruits, removed 
        """
        # make figure
        print('minFullJul',np.min(self.fullJulArray))
        maxJulSess = np.max(self.sessionJul)
        sessionMask =(self.fullJulArray >= 0) & (self.fullJulArray <= maxJulSess)
        maskSessDates = np.in1d(self.fullJulArray, self.sessionJul)
        self.totalEffortSess = self.totalEffort[maskSessDates]
        self.fullSessDates = self.fullDateArray[sessionMask]
        sessRecruits = self.sessionRecruits[0:self.gibbsobj.nsession]
        print('EffortSess', len(self.totalEffortSess), len(self.sessDates), len(sessRecruits))
        print('toteffortsess', self.totalEffortSess[0:120], np.max(self.totalEffortSess))
        print('sury jul', self.gibbsobj.surveyJul[0:15])
        P.figure(figsize=(14, 6))
        ax = P.gca()
#        lns5 = ax.plot(self.fullSessDates, self.totalEffortSess, label = 'Total-effort days', color = 'y', linewidth = 3)
        npred2 = self.nMoveAve + np.random.poisson(self.nMoveAve + 50)
        lns1 = ax.plot(self.sessDates, npred2, label = 'Pop size', color = 'k', linewidth = 3)
#        lns1 = ax.plot(self.sessDates, self.nMoveAve, label = 'Pop size', color = 'k', linewidth = 3)
#        lns4 = ax.plot(self.fullSessDates, self.totalEffortSess, label = 'Total-effort days', color = 'y', linewidth = 3)
#        lns = lns1
        ax2 = ax.twinx()
        lns2 = ax2.plot(self.sessDates, self.gibbsobj.removeDat, label = 'Birds removed', color = 'r', linewidth = 3)
#        lns3 = ax2.plot(self.sessDates, sessRecruits, label = 'New recruits', color = 'b', linewidth = 3)
#        lns5 = ax.plot(self.sessDates, self.totalEffortSess, label = 'Total-effort days', color = 'y', linewidth = 3)
        lns = lns1 + lns2
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        ax.set_ylim([0, 380])
        ax.set_xlim(self.minDate, self.maxDate)
        ax2.set_ylim(0, 40)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_ylabel('Pop. size', fontsize = 15)
        ax.set_xlabel('Years (January)', fontsize = 15)
        ax2.set_ylabel('Number removed', fontsize = 17)
        plotFname = 'N_removed_npred.png'
#        plotFname = os.path.join(self.predatorpath, 'N_removed_recruit_trapnight.png')
        P.savefig(plotFname, format='png')
        P.show()

    def movingAveN(self):
        """
        calc moving average of Npred
        """
        windowRange = 60

        poisN = np.random.poisson(self.Npred + 50)

        self.nMoveAve = np.zeros(self.gibbsobj.nsession)
        for i in range(self.gibbsobj.nsession):
            jul_i = self.sessionJul[i]
            diffJul = np.abs(self.sessionJul - jul_i)
            windowMask = diffJul <= windowRange
            meanNpred = np.mean(self.Npred[windowMask])
            self.nMoveAve[i] = meanNpred
        
    def plotRelWrpC(self):
        """
        make plots of wrp cauchy parameters over time
        """
        # recruitment details
        nRec = 20
        julAdd = np.arange(0., 365., 10. )
        july01 = datetime.date(2011, 7, 1)        
        self.yrDates = july01
        timetuple = july01.timetuple()
        self.julDay = timetuple.tm_yday
        self.monthDate = july01.month
        for i in range(1, len(julAdd)):
            date_i = july01 + datetime.timedelta(days=julAdd[i])
            self.yrDates = np.append(self.yrDates, date_i)
            timetuple = date_i.timetuple()
            self.julDay = np.append(self.julDay, timetuple.tm_yday)

        self.daypi = self.julDay / 365.0 * 2.0 * np.pi
        dc = dwrpcauchy(self.daypi, self.meanRPara[0] + .5, self.meanRPara[1] - .1)
        reldc = dc / np.sum(dc)     #  dc.sum()
        self.recruitYear = nRec * reldc
        #observation details
        self.meanG0_Obs = np.mean(self.gibbsobj.g0Obsgibbs)
        self.meanObsWrpC = np.mean(self.gibbsobj.g0ObsWrpCgibbs, axis = 0)
        dc = dwrpcauchy(self.daypi, self.meanObsWrpC[0], self.meanObsWrpC[1])
        reldc = dc / np.max(dc)     #  dc.sum()
        self.probObs = self.meanG0_Obs * reldc
        # shoot details
        self.meanG0_Shoot = np.mean(self.gibbsobj.pKillgibbs)
        self.meanShootWrpC = np.mean(self.gibbsobj.g0KillWrpCgibbs, axis = 0)
        dc = dwrpcauchy(self.daypi, self.meanShootWrpC[0], self.meanShootWrpC[1])
        reldc = dc / np.max(dc)     #  dc.sum()
        self.probShoot = self.meanG0_Shoot * reldc
        # trap details
        self.meanG0_Trap = np.mean(self.gibbsobj.g0Trapgibbs)
        self.meanTrapWrpC = np.mean(self.gibbsobj.g0TrapWrpCgibbs, axis = 0)
        dc = dwrpcauchy(self.daypi, self.meanTrapWrpC[0], self.meanTrapWrpC[1])
        reldc = dc / np.max(dc)     #  dc.sum()
        self.probTrap = self.meanG0_Trap * reldc
        ##############################################################################
        # Plot figures      ##########################################################
        P.figure(figsize=(16, 8))
        P.subplot(2,2,1)            ### Recruitment details
        P.plot(self.yrDates, self.recruitYear, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        P.ylabel('N recruits', fontsize = 15)
#        ax.set_xlabel('Dates in year', fontsize = 15)
        P.xlim(np.min(self.yrDates), np.max(self.yrDates))
        P.xticks([datetime.date(2011, 7, 1), datetime.date(2011, 10, 1), datetime.date(2012, 1, 1), 
            datetime.date(2012, 4, 1), datetime.date(2012, 6, 30)])
            #### observation details
        P.subplot(2,2,2)
        P.plot(self.yrDates, self.probObs, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        ax.set_ylabel('Probability of Observation', fontsize = 15)
#        ax.set_xlabel('')
        P.xlim(np.min(self.yrDates), np.max(self.yrDates))
        P.xticks([datetime.date(2011, 7, 1), datetime.date(2011, 10, 1), datetime.date(2012, 1, 1), 
            datetime.date(2012, 4, 1), datetime.date(2012, 6, 30)])
#        ax.set_xlabel('Dates in year', fontsize = 15)
#        P.savefig('pObservationWrpC.png', format='png')
            #### shooting details        
        P.subplot(2,2,3)
        P.plot(self.yrDates, self.probShoot, color = 'k', linewidth = 3)
        ax =P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        ax.set_ylabel('Probability of shoot', fontsize = 15)
        ax.set_xlabel('Dates in year', fontsize = 15)
        P.xlim(np.min(self.yrDates), np.max(self.yrDates))
        P.xticks([datetime.date(2011, 7, 1), datetime.date(2011, 10, 1), datetime.date(2012, 1, 1), 
            datetime.date(2012, 4, 1), datetime.date(2012, 6, 30)])
            #### trapping details
        P.subplot(2,2,4)
        P.plot(self.yrDates, self.probTrap, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        ax.set_ylabel('Probability of trap', fontsize = 15)
        ax.set_xlabel('Dates in year', fontsize = 15)
        P.xlim(np.min(self.yrDates), np.max(self.yrDates))
        P.xticks([datetime.date(2011, 7, 1), datetime.date(2011, 10, 1), datetime.date(2012, 1, 1),
            datetime.date(2012, 4, 1), datetime.date(2012, 6, 30)])
        P.savefig('pTrapWrpC.png', format='png')
        P.show()
        
        
               
  
        
def main(params):
    # paths and data to read in
    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')

    # read in the pickled data from manipStarlings
#    starlingTrapFile = os.path.join(starlingpath,'out_manipStarling.pkl')
#    fileobj = open(starlingTrapFile, 'rb')
#    starlingtrapdata = pickle.load(fileobj)
#    fileobj.close()

    # initiate 
    inputGibbs = os.path.join(starlingpath, 'out_gibbs.pkl')
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    resultsobj = ResultsProcessing(gibbsobj, params, starlingpath)

    
if __name__ == '__main__':
    p = params.Params()
    main(p)



