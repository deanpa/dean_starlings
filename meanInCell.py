#!/usr/bin/env python

import os
import numpy as np
from rios import applier
from scipy import ndimage
#from numba import autojit



def meanNeigbour(infile, outfile):
    """
    Object to read in and calculate neighbourhood statistics
    """
    # Set up input and output filenames. 
    infiles = applier.FilenameAssociations()
    infiles.image1 = infile

    outfiles = applier.FilenameAssociations()
    outfiles.outimage = outfile
    # see controls page on Rios
    controls = applier.ApplierControls()
    controls.overlap = 1
    controls.drivername = "GTiff"
    applier.apply(rios_meanNeighbour, infiles, outfiles, controls=controls)


def rios_meanNeighbour(info, inputs, outputs):
    """

    """
    out = ndimage.filters.uniform_filter(inputs.image1[0], size = 3)
    outputs.outimage = np.expand_dims(out, 0)

########            Main function
#######
def main():
    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')
    # paths and data to read in
    starlingVegFile = os.path.join(starlingpath,'nativeveg_R50.tif')
    outTiff = os.path.join(starlingpath,'nativeveg_avg_R50.tif')

    meanNeigbour(starlingVegFile, outTiff)
    

if __name__ == '__main__':
    main()

