#!/usr/bin/env python

import os
import numpy as np
from rios import applier

def sumInCell(infile, outfile):
    """
    Object to read in and calculate neighbourhood statistics
    """
    # Set up input and output filenames. 
    infiles = applier.FilenameAssociations()
    infiles.image1 = infile

    outfiles = applier.FilenameAssociations()
    outfiles.outimage = outfile
    # see controls page on Rios
    controls = applier.ApplierControls()
    controls.drivername = "GTiff"
    # get sum in window of following size : 1 km search = 20 cells with 50-m resol
    controls.windowxsize = 20       # 40
    controls.windowysize = 20       # 40
    applier.apply(rios_sumInCell, infiles, outfiles, controls=controls)

def rios_sumInCell(info, inputs, outputs):
    """

    """
    out_sum = np.sum(inputs.image1[0])
    outputs.outimage = np.empty_like(inputs.image1)
    outputs.outimage.fill(out_sum) 

########            Main function
#######
def main():
    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')
    # paths and data to read in
    starlingVegFile = os.path.join(starlingpath,'nativeveg_R50.tif')
    outTiff = os.path.join(starlingpath,'NatVeg_sum_R50_1KmSearch.tif')
    print(outTiff)

    sumInCell(starlingVegFile, outTiff)
    

if __name__ == '__main__':
    main()

