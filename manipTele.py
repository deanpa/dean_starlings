#!/usr/bin/env python

import os
import numpy as np
import pickle
#from hrmcmc import starlingTeleData
#from numba import autojit


class RawTele(object):
    def __init__(self, teleFname):
        """
        Object to read in telemetry data
        """
        # cat capture data, trap id, week, avail etc
        self.teledat = np.genfromtxt(teleFname,  delimiter=',', names=True,
            dtype=['i8', 'S10', 'S10', 'i8', 'i8', 'f8', 'f8','f8','i8'])

        self.id = self.teledat['Starling_ID']

        print('id', self.id)

        ###########################################################
        # Run rawdata functions


    ###############################################################
    ################# Functions



########            Main function
#######
def main():
    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')
    # paths and data to read in
    starlingTeleFile = os.path.join(starlingpath,'StarlingTelemetry0607.csv')
    # initiate rawdata class and object 
    rawtele = RawTele(starlingTeleFile)

#    starlingteledata = starlingTeleData(rawdata)
#    # pickle basic data to be used in cats.py
#    outManipdata = os.path.join(starlingpath,'out_maniptele.pkl')
#    fileobj = open(outManipdata, 'wb')
#    pickle.dump(starlingtrapdata, fileobj)
#    fileobj.close()

if __name__ == '__main__':
    main()

