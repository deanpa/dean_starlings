#!/usr/bin/env python

import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
from numba import jit
import pickle
import datetime
import calendar
import pylab as P

def thProbFX(tt, debug = False):
    tt2 = np.exp(tt)
    tt3 = tt2/np.sum(tt2)
    return(tt3)

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

def getXY(xmin, ymax, resol, halfres, row, col):
    """
    get x and y from row and col
    """
    x = (xmin + halfres) + (resol * col)
    y = (ymax - halfres) - (resol * row)
    return(x, y)

@jit
def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

@jit
def getPrevCount(birdLoc_2D, prevCellCount, maxN, nsession):
    """
    get intial cell count
    """
    for i in range(1, nsession):
        for cc in range(maxN):
            if birdLoc_2D[(i-1), cc] > 0:
                prevCellCount[cc, i] += 1.0
    return prevCellCount


def matrixsub(arr1, arr2):
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat


def initialPPredatorTrapCaptFX(basicdata, availTrapNights, location, g0Param, debug = False):      
    """
    # prob that predator was capt in trap
    """
    distToTraps = basicdata.distTrapToCell2[:, location]      # dist from cell j to all traps
    eterm = np.exp(-(distToTraps) / basicdata.var2)           # prob predator-trap pair
    pNoCapt = 1. - g0Param * eterm
    pNoCaptNights = pNoCapt**(availTrapNights)
    pNoCaptNights[pNoCaptNights >= .9999] = 0.9999
    pcapt = 1 - pNoCaptNights
    pcapt[pcapt > .97] = 0.97
    if debug == True:
        print("basicdata.distTrapToCell2.shp", basicdata.distTrapToCell2.shape)
        print("distToTraps.shp", distToTraps.shape)
    return pcapt

def multinomial_pmf(probs, counts):
    probssum = probs.sum()
    if probssum < 0.999 or probssum > 1.0001:
        raise ValueError("probs must sum to 1")
    if probs.size != counts.size:
        raise ValueError("probs and counts must be the same size")
    return gammaln(counts.sum() + 1.0) - gammaln(counts + 1.0).sum() + np.sum(np.log(probs)*counts)


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    """
    e_num = np.exp(-2*rho)
    e_denom = 2 * np.exp(-rho)
    sinh_rho = (1 - e_num) / e_denom
    cosh_rho = (1 + e_num) / e_denom
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc

def calcRelWrapCauchy(wrp_rpara, nsession, uYearIndx, daypi, yearRecruitIndx):
    """
    Calc the rel cauchy value for all sessions for distributing recruits
    """
    relWrpCauchy = np.zeros(nsession)
    for i in uYearIndx:
        # day pi in year i
        daypiTmp = daypi[yearRecruitIndx == i]
        # pdf of daypi in year i
        dc = dwrpcauchy(daypiTmp, wrp_rpara[0], wrp_rpara[1])
        # mask of recruit window in year i
        reldc = dc / np.sum(dc)     #  dc.sum()
        relWrpCauchy[yearRecruitIndx == i] = reldc
    return relWrpCauchy


@jit
def drawOneMultinom(randArr, prob, nsurveys, nRiskSites, extentSide, adjustRisk, riskMask):
    """
    draw multinom variates where only 1 can be drawn
    """
    trialsRemain = nRiskSites
    nRemain = nsurveys
    sumProbsUsed = 0.0
    sumAdjustRisk = 0.0
    for f in range(extentSide):
        for g in range(extentSide):
            if riskMask[f, g]:
                prob_fg = prob[f, g] / (1.0 - sumProbsUsed)
                prob_fg = 1.0 - (1.0 - prob_fg)**nRemain
                if nRemain == trialsRemain:
                    X_fg = 1
                else:
                    X_fg = np.random.binomial(1, prob_fg)
                if X_fg == 1:
                    sumAdjustRisk += adjustRisk[f, g]
                sumProbsUsed += prob[f, g]
                randArr[f, g] = X_fg
                nRemain -= X_fg
                trialsRemain -= 1
    return(randArr, sumAdjustRisk)


@jit
def ln_poissonPMF(dat, datGammaLn, ll):
    """
    calc log pmf for poission
    """
    ln_ppmf = ((dat * np.log(ll)) - ll) - datGammaLn
#    ln_ppmf = np.log((ll**dat) * np.exp(-ll)) - datGammaLn
    return(ln_ppmf)         


@jit
def pObsTrapID_3D(sessions, sessionJulYear, sessionRecruitIndx, 
        birdPres_2D, birdRow_2D, birdCol_2D, birdX_2D, birdY_2D, sessionNTraps, 
        sessionNSurveys, nFlocks,
        surveyX_2D, surveyY_2D, trapX_2D, trapY_2D, g0Obs_Session, 
        g0Trap_Session, birdPObservedID_3D, birdPTrappedID_3D, birdTotPObserved_2D,
        birdTotPTrapped_2D, var2):
#        sumMultinomPObs_2D, sumMultinomPTrap_2D):
    """
    Numba function to calc the pair-wise prob of observation and trap of birds
    and searchers and traps
    """
    # loop thru level in 3D - sessions - dimension 0
    for i in sessions:
        yrIndx = sessionRecruitIndx[i]
        nsurvey_i = sessionNSurveys[i]
        ntrap_i = sessionNTraps[i]
        g0ObsSess_i = g0Obs_Session[i]
        g0TrapSess_i = g0Trap_Session[i]
        # loop thru maxN - dimension 1
        for j in range(nFlocks):
            pNoObs = 1.0                        # p not observed for total prob calc
            pNoTrap = 1.0                       # update with traps
            birdRow_ij = birdRow_2D[yrIndx, j]       # rows are maxN, cols session
            birdCol_ij = birdCol_2D[yrIndx, j]
            xBirdLoc = birdX_2D[yrIndx, j]
            yBirdLoc = birdY_2D[yrIndx, j]
            ## loop thru maxTraps number to get pair-wise probs
            if nsurvey_i >= ntrap_i:
                nloops = nsurvey_i
            else:
                nloops = ntrap_i
            for k in range(nloops):
                ######## Calc survey obs probability if surveyor present
                if k < nsurvey_i:
                    sx = surveyX_2D[i, k]
                    sy = surveyY_2D[i, k]       # survey k northings
                    distBS = distxy(xBirdLoc, yBirdLoc, sx, sy)                    
                    pObs = g0ObsSess_i * np.exp(-(distBS**2.0) / var2)
                    if pObs < 1.0e-50:
                        pObs = 1.0e-50
                    birdPObservedID_3D[i, j, k] = pObs
                    pNoObs = pNoObs * (1.0 - pObs)
#                    sumMultinomPObs_2D[i, k] += pObs
                ######## Calc trap probability if trap present
                if k < ntrap_i:
                    tx = trapX_2D[i, k]
                    ty = trapY_2D[i, k]       # survey k northings
                    distBT = distxy(xBirdLoc, yBirdLoc, tx, ty)                    
                    ptrap = g0TrapSess_i * np.exp(-(distBT**2.0) / var2)
                    if ptrap < 1.0e-50:
                        ptrap = 1.0e-50
                    birdPTrappedID_3D[i, j, k] = ptrap
                    pNoTrap = pNoTrap * (1.0 - ptrap)
#                    sumMultinomPTrap_2D[i, k] += ptrap
            birdTotPObserved_2D[i, j] = 1.0 - pNoObs
            birdTotPTrapped_2D[i, j] = 1.0 - pNoTrap
    # return 3-D arrays
    return(birdPObservedID_3D, birdPTrappedID_3D, birdTotPObserved_2D, birdTotPTrapped_2D)
#        sumMultinomPObs_2D, sumMultinomPTrap_2D)



@jit
def distribRecruits(nFlocks, birdPres_2D, birdTotRecruit_2D, birdRecruitSessFlock_2D, 
            sessionJul, nsession, recruitSession, nRecruitJulYear,
            yearRecruitIndx, relWrpCauchy, birdSessFlock_2D, sessionRecruitIndx):
    """
    ## distribute recruits across sessions for each flock
    ## can get number distib < total recruits because last session may be 
    ## before end of recruit year. Assume too late to survive.
    """
    ## loop thru flocks
    for i in range(nFlocks):
        nRemainDistrib = birdTotRecruit_2D[0, i]
        addProbs = 0.0
        sessCounter = 0
        ## populate first session with adult birds
        birdSessFlock_2D[0, i] = birdPres_2D[0, i]
        for j in range(nRecruitJulYear):        # loop sessions
            ## if j is on survey or hunt session
            if (sessionJul[sessCounter] == recruitSession[j]):
                probs_j = relWrpCauchy[j] / (1.0 - addProbs)
                if probs_j > 1.0:
                    probs_j = 1.0
                nRec_ij = np.random.binomial(nRemainDistrib, probs_j)
                birdRecruitSessFlock_2D[sessCounter, i] = nRec_ij
                nRemainDistrib -= nRec_ij
                ## populate session 0 with recruits
                if sessCounter == 0:
                    birdSessFlock_2D[0, i] += nRec_ij
                sessCounter += 1
                ## if at end of survey sessions
                if (sessCounter == nsession):
                    break
            addProbs += relWrpCauchy[j] 
            ## if arrive at new year, reset number of recruits and probs.
            yrIndx_j1 = yearRecruitIndx[j+1]
            if (yrIndx_j1 > yearRecruitIndx[j]):
                nRemainDistrib = birdTotRecruit_2D[yrIndx_j1, i]
                addProbs = 0.0
    return(birdRecruitSessFlock_2D, birdSessFlock_2D)


@jit
def getLatentObsShotTrap(nsession, sessions, nFlocks, maxTraps, 
        maxSurvey, observedData_2D,
        birdObservedID_3D, shotData_2D, birdShotID_3D, trappedData_2D,
        birdTrappedID_3D, birdObserved_2D, birdShot_2D,
        birdTrapped_2D, minNPresent,
        birdRecruitSessFlock_2D, 
        birdSessFlock_2D, birdPres_2D, pKill_Session, sessionRecruitIndx,
        birdPObservedID_3D, birdPTrappedID_3D):
    """
    get initial latent obs, trap, and shot ID and populate 3D arrays
    """
    for i in sessions:
        minNPresent[i] = 0      # reset the minNPresent
        yrIndx = sessionRecruitIndx[i]
        # loop thru traps and surveys in session i
        for j in range(maxTraps):
            nBirdsTrapRemain = 0
            nBirdsObsRemain = 0
            nBirdsShotRemain = 0
            ## get latent trap Id data
            ntrapped_ij = trappedData_2D[i, j]
            if ntrapped_ij > 0:
                addProbs = 0.0
                nRemain = ntrapped_ij
                ##### Have to loop thru flocks to get sum multinom probs
                sumMultiProb = 0.0
                for n in range(nFlocks):
                    nBirdRemain_n = birdSessFlock_2D[i, n] - birdTrapped_2D[i, n]
                    if nBirdRemain_n > 0:
                        ## total prob of trapped across flocks for multinom prob
                        nBirdsTrapRemain += nBirdRemain_n
                        sumMultiProb += birdPTrappedID_3D[i, n, j] * nBirdRemain_n
                ##### Loop second time to distribute captured birds across flocks
                for n in range(nFlocks):
                    ## birds avail to be trapped by j in sess i and flock n
                    nBird_in = birdSessFlock_2D[i, n] - birdTrapped_2D[i, n]
                    if nBird_in > 0:
                        prob_n = birdPTrappedID_3D[i, n, j] / sumMultiProb
                        for k in range(nBird_in):
                            if addProbs >= 1.0:
                                binomProb_n = 1.0
                            else:
                                binomProb_n = prob_n / (1.0 - addProbs)
                            ## each bird trialled could be one of the remaining birds trapped
                            binomPr_nk = 1.0 - (1.0 - binomProb_n)**nRemain
                            if binomPr_nk > 1.0:
                                binomPr_nk = 1.0
                            if nBirdsTrapRemain == nRemain:
                                nTrap_n = 1
                            else:
                                nTrap_n = np.random.binomial(1, binomPr_nk)
                            nBirdsTrapRemain -= 1
                            nRemain -= nTrap_n
                            addProbs += prob_n
                            birdTrappedID_3D[i, n, j] += nTrap_n    # (session x maxN x nTraps)
                            birdTrapped_2D[i, n] += nTrap_n              # (session x maxN)
                            if nRemain == 0:
                                break
                    if nRemain == 0:
                        break
#                        minNPresent[i] += nTrap_n
            ## do survey data aftertrapping
            if j < maxSurvey:
                ## if birds obs in session i and survey j
                nobs_ij = observedData_2D[i, j] 
                if nobs_ij > 0:
                    addProbs = 0.0
                    nRemain = nobs_ij
                    sumMultiProb = 0.0
                    ## loop thru flocks to get sum multinom prob
                    for n in range(nFlocks):
                        ## birds avail to be observerd
                        nBird_in = birdSessFlock_2D[i, n] - birdTrapped_2D[i, n]
                        if nBird_in > 0:
                            nBirdsObsRemain += nBird_in
                            sumMultiProb += birdPObservedID_3D[i, n, j] * nBird_in
                    ## loop thru flocks again to distribute observations from survey j
                    for n in range(nFlocks):
                        ## birds available to be observed
                        nBird_in = birdSessFlock_2D[i, n] - birdTrapped_2D[i, n]
                        if nBird_in > 0:
                            prob_n = birdPObservedID_3D[i, n, j] / sumMultiProb
                            ## loop thru birds in flock n'
                            for k in range(nBird_in):
                                if addProbs >= 1.0:
                                    binomProb_n = 1.0
                                else:
                                    binomProb_n = prob_n / (1.0 - addProbs)
                                binomPr_nk = 1.0 - (1.0 - binomProb_n)**nRemain
                                if binomPr_nk > 1.0:
                                    binomPr_nk = 1.0
                                if nBirdsObsRemain == nRemain:
                                    obs_kn = 1
                                else:
                                    obs_kn = np.random.binomial(1, binomPr_nk)
                                nBirdsObsRemain -= 1
                                nRemain -= obs_kn
                                addProbs += prob_n

                                birdObservedID_3D[i, n, j] += obs_kn    # (session x nFlock x nSurvey)
                                birdObserved_2D[i, n] += obs_kn        # (session x nFlock)
                                minNPresent[i] += obs_kn              # 1-d nsession
                                if nRemain == 0:
                                    break
                        if nRemain == 0:
                            break    
                nshot_ij = shotData_2D[i, j]                # n shot in survey j
                ## if bird shot, have to ID flocks from which the birds came
                if nshot_ij > 0:
                    addProbs = 0.0
                    nRemain = nshot_ij
                    sumMultiProb = 0.0
                    for n in range(nFlocks):
                        nBirdObs_ijn = birdObservedID_3D[i, n, j]
                        if nBirdObs_ijn > 0:
                            sumMultiProb += pKill_Session[i] * nBirdObs_ijn
                            nBirdsShotRemain += nBirdObs_ijn
                    ## loop thru flocks to distribute shot individuals
                    prob_n = pKill_Session[i] / sumMultiProb
                    for n in range(nFlocks):
                        ## birds available to be shot
                        nBirdObs_ijn = birdObservedID_3D[i, n, j] - birdShotID_3D[i, n, j]
                        if nBirdObs_ijn > 0:
                            for k in range(nBirdObs_ijn):
                                binomProb_n = prob_n / (1.0 - addProbs)
                                binomPr_nk = 1.0 - (1.0 - binomProb_n)**nRemain
                                if binomPr_nk > 1.0:
                                    binomPr_nk = 1.0

                                if nBirdsShotRemain == nRemain:
                                    shot_kn = 1
                                else:
                                    shot_kn = np.random.binomial(1, binomPr_nk)
                                nRemain -= shot_kn
                                nBirdsShotRemain -= 1
                                addProbs += prob_n            
                                birdShotID_3D[i, n, j] += shot_kn  # (session x maxN x nTraps)
                                birdShot_2D[i, n] += shot_kn       # (session x maxN)
                                if nRemain == 0:
                                    break
                        if nRemain == 0:
                            break
        ## loop thru flocks to get starting pop for next session   
        for n in range(nFlocks):
            if i < (nsession - 1):
                if (yrIndx < sessionRecruitIndx[(i + 1)]):
                    birdSessFlock_2D[(i + 1)] = (birdPres_2D[(yrIndx + 1), n] +
                        birdRecruitSessFlock_2D[(i + 1), n])
                else:
                    birdSessFlock_2D[(i + 1), n] = (birdSessFlock_2D[i, n] +
                        birdRecruitSessFlock_2D[(i + 1), n] -
                        birdTrapped_2D[i, n] - birdShot_2D[i, n])
#                birdSessFlock_2D[(i + 1), n] = (birdSessFlock_2D[i, n] +
#                    birdRecruitSessFlock_2D[(i + 1), n] -
#                    birdTrapped_2D[i, n] - birdShot_2D[i, n])
#                nrem_in_up1 = birdTrapped_2D[(i+1), n] + birdShot_2D[(i+1), n]
#                if birdSessFlock_2D[(i + 1), n] < nrem_in_up1:
#                    birdSessFlock_2D[(i + 1), n] = nrem_in_up1 + 1
                    
    return(birdObservedID_3D, birdShotID_3D, birdTrappedID_3D, birdObserved_2D,
            birdShot_2D, birdTrapped_2D, minNPresent, birdSessFlock_2D)                    



############################################################################
######      MCMC NUMBA FUNCTIONS


@jit
def getSessionRecruits(reproPop, totalRecruits, yearRecruitIndx, relWrpCauchy, sessionRecruits,
        sessionJul, maxSessionJul):
    """
    loop thru sessions to get n recruits for all days, then populate the sessionRecruits array
    """
    sumRecruits = 0.0
    sessionCounter = 1
    for i in range(1, (maxSessionJul + 1)):
        yrIndx = yearRecruitIndx[i]
        recruits_i =  totalRecruits[yrIndx] * relWrpCauchy[i]
        sumRecruits += recruits_i
        if (sessionJul[sessionCounter] == i):
            sessionRecruits[sessionCounter] = sumRecruits
            sessionCounter += 1
            sumRecruits = 0.0
    return sessionRecruits            


##########################################
##########################################  UPDATING N
##########################################

@jit
def proposeBirdPres(sessions, birdTotPObserved_2D, birdPObservedID_3D, 
    birdTotPTrapped_2D, birdPTrappedID_3D, surveyX_2D, 
    surveyY_2D, trapX_2D, trapY_2D, observedData_2D, trappedData_2D, N, nsample, 
    birdPres_2D, cellX, cellY, sessionNSurveys, sessionNTraps, g0Obs_Session, 
    g0Trap_Session, var2, nCells, 
    birdObserved_2D, birdTrapped_2D, birdLoc_2D, lnGammaArray, minNPresent, maxN, 
    Npred, Npred_s, reproMask, reproPeriodIndx, yearRecruitIndx, uYearIndx, 
    relWrpCauchy, sessionRecruits, sessionRecruits_s, totalRecruits, reproPop, 
    ig, rg, nsession, removeDat, PObsID_3D_s, PTrapID_3D_s, sessionJul, maxSessionJul,
    nSessionsInYear, nRecruitSessInYear):

    """
    (20) propose new N and get corresponding latent variables and update
    """
    for i in nFlocks:
#        for j in nYears:
    
        # get proposed N
        (Ns, Ni, g0ObsSess_i, g0TrapSess_i, nsurvey_i, 
            ntrap_i) = NsConditioned(i, nsample, N, 
            g0Obs_Session, g0Trap_Session, sessionNSurveys, 
            sessionNTraps)
        if Ns > Ni:
            # get id and probabilities of obs and trap
            (birdID_s, cellAdd, PObsID_3D_s, PTrapID_3D_s,
                birdTotPObserved_2D_s, birdTotPTrapped_2D_s) = NsGreaterNi(i, 
                Ns, Ni, cellX, cellY, surveyX_2D, 
                surveyY_2D, trapX_2D, trapY_2D, sessionNSurveys, sessionNTraps, 
                g0ObsSess_i, g0TrapSess_i, var2, birdPres_2D, maxN, nCells, 
                nsurvey_i, ntrap_i, PObsID_3D_s, PTrapID_3D_s)
        if Ni > Ns:
            # get id and probabilities to remove
            (birdID_s, cellAdd, birdTotPObserved_2D_s, 
                birdTotPTrapped_2D_s) = NsLessNiMEM(i, Ni, 
                birdPres_2D, minNPresent, sessionNSurveys, sessionNTraps, 
                birdObserved_2D, birdTrapped_2D, birdTotPObserved_2D, 
                birdTotPTrapped_2D, nsurvey_i, ntrap_i)
        # calc log binom 
        (nLogLik_i, nLogLik_i_s, prevCellCount_2D) = NLogLikFX(i,  Ns, Ni, birdPres_2D, 
            birdID_s, birdTotPObserved_2D_s, 
            birdTotPTrapped_2D_s, birdTotPObserved_2D, birdTotPTrapped_2D, 
            birdObserved_2D, birdTrapped_2D, maxN, lnGammaArray, prevCellCount_2D,
            birdLoc_2D)
        # calc poisson likelihood for Ni and Ns
        (Ns, Npred_s, sessionRecruits_s, totalRecruits_s, reproPop_s, sumReproPop_s, 
            totNPois_i, totNPois_i_s, yrIndx, isRepro) = NPredPoisson(i, Ni, Ns, N, 
            Npred, Npred_s, 
            reproMask, reproPeriodIndx, yearRecruitIndx, uYearIndx, relWrpCauchy, 
            sessionRecruits, sessionRecruits_s, totalRecruits, reproPop, ig, rg, 
            nsession, removeDat, sumReproPop, nSessInReproPeriod, lnGammaArray,
            sessionJul, maxSessionJul)
        # calc N pnow pnew, IR and update
        (N, Npred, Npred_s, sessionRecruits, totalRecruits, reproPop, 
            sumReproPop, birdPObservedID_3D, birdPTrappedID_3D, 
            birdTotPObserved_2D, birdTotPTrapped_2D, birdLoc_2D, 
            birdPres_2D) = N_Pnow_Pnew(i, nLogLik_i, nLogLik_i_s, N, Ni, Ns, Npred, 
            Npred_s, sessionRecruits_s, cellAdd,
            totalRecruits_s, reproPop_s, sumReproPop_s, totNPois_i, totNPois_i_s,
            yrIndx, isRepro, sessions, nsession, yearRecruitIndx, sessionRecruits,
            totalRecruits, reproPop, sumReproPop, PObsID_3D_s, PTrapID_3D_s, 
            birdTotPObserved_2D_s, birdTotPTrapped_2D_s, 
            birdPObservedID_3D, birdPTrappedID_3D, birdTotPObserved_2D,
            birdTotPTrapped_2D, birdLoc_2D, birdPres_2D, birdID_s, nsurvey_i,
            ntrap_i)
    ### return updated arrays from Nupdate    
    return(N, Npred, Npred_s, sessionRecruits, totalRecruits, reproPop,
            sumReproPop, birdPObservedID_3D, birdPTrappedID_3D,
            birdTotPObserved_2D, birdTotPTrapped_2D, birdLoc_2D,
            birdPres_2D, prevCellCount_2D)
    ############
    ############    END N WRAPPER


    
    
    
    
#### WORKING HERE ####################################
    
    
@jit
def NsConditioned(i, j, nsample, g0Obs_Session,
        g0Trap_Session, sessionNSurveys, sessionNTraps):
    """
    (21) Get Ns_i and meet conditions
    """
#    if i==0:
#        print(21)
    Ni = N[i]
    randDN = np.random.randint(-10, 11)
    if randDN <= 0:
        randDN = 1
    else:
        randDN = -1
    Ns = randDN + Ni 
    thresholdN = minNPresent[i]
    if Ns <= 0:
        Ns = Ni + 1
    if Ns <= thresholdN:
        Ns = thresholdN + 1
    if Ns == Ni:
        Ns = Ni + 1
    if Ns >= maxN:
        Ns = Ni - 1
    if Ns == Ni:
        Ns = Ni - 1
#    print('Ni', Ni, 'Ns', Ns, 'threshld', thresholdN)
    # get g0Obs_Session, g0Trap_Session,
    g0ObsSess_i = g0Obs_Session[i]
    g0TrapSess_i = g0Trap_Session[i]
    nsurvey_i = sessionNSurveys[i]
    ntrap_i = sessionNTraps[i]
    return(Ns, Ni, g0ObsSess_i, g0TrapSess_i, nsurvey_i, ntrap_i)


@jit
def NsGreaterNi(i, Ns, Ni, cellX, cellY, surveyX_2D, surveyY_2D, trapX_2D, 
        trapY_2D, sessionNSurveys, sessionNTraps, g0ObsSess_i, g0TrapSess_i, 
        var2, birdPres_2D, maxN, nCells, nsurvey_i, ntrap_i,
        PObsID_3D_s, PTrapID_3D_s):
    """
    (21.1.2) When Ns > Ni:  add individuals to proposed pop and get probs.
    """
    cellAdd = np.random.randint(0, nCells)        
    birdID_s = np.random.randint(0, maxN)
    contFX = 0
    while contFX == 0:
        # find first open ID spot to put new individual
        if birdPres_2D[i, birdID_s] == 1:
            birdID_s += 1
            if birdID_s == maxN:
                birdID_s = 0
        else:
            contFX = 1    # get proposed probs of obs and trap
    (PObsID_3D_s, PTrapID_3D_s, birdTotPObserved_2D_s,
        birdTotPTrapped_2D_s) = calcPObsTrap_ijMEM(i, 
        cellAdd, cellX, cellY, surveyX_2D, surveyY_2D, trapX_2D, trapY_2D, 
        sessionNSurveys, sessionNTraps, g0ObsSess_i, g0TrapSess_i, var2,
        nsurvey_i, ntrap_i, PObsID_3D_s, PTrapID_3D_s)
    return(birdID_s, cellAdd, PObsID_3D_s, PTrapID_3D_s, 
        birdTotPObserved_2D_s, birdTotPTrapped_2D_s)


@jit 
def calcPObsTrap_ijMEM(i, cellAdd, cellX, cellY, surveyX_2D, surveyY_2D, 
        trapX_2D, trapY_2D, sessionNSurveys, sessionNTraps, g0ObsSess_i, 
        g0TrapSess_i, var2, nsurvey_i, ntrap_i, PObsID_3D_s, PTrapID_3D_s):
    """
    (22) added bird j in session i, calc prob obs and trap
    """
#    if i==0:
#        print(22)
    xBirdLoc_s = cellX[cellAdd]
    yBirdLoc_s = cellY[cellAdd]
    pNoObs = 1.0
    pNoTrap = 1.0
    ## loop thru Traps or surveys  number to get pair-wise probs
    # make empty arrays: 1-d arrays for probability pairs
#    birdPObservedID_3D_s = np.zeros(nsurvey_i) 
#    birdPTrappedID_3D_s = np.zeros(ntrap_i)
    if nsurvey_i >= ntrap_i:
        nloop = nsurvey_i
    else:
        nloop = ntrap_i
    for k in range(nloop):      # loop thru number survey or traps
        ######## Calc survey obs probability if surveyor present
        if k < nsurvey_i:
            sx = surveyX_2D[i, k]
            sy = surveyY_2D[i, k]       # survey k northings
            distBS = distxy(xBirdLoc_s, yBirdLoc_s, sx, sy)                    
            pObs_s = g0ObsSess_i * np.exp(-(distBS**2.0) / var2)
            if pObs_s < 1.0e-200:
                pObs_s = 1.0e-200
            PObsID_3D_s[k] = pObs_s  # pobs bird j in survey k
            pNoObs = pNoObs * (1.0 - pObs_s)
        ######## Calc trap probability if trap present
        if k < ntrap_i: 
            tx = trapX_2D[i, k]
            ty = trapY_2D[i, k]       # survey k northings
            distBT = distxy(xBirdLoc_s, yBirdLoc_s, tx, ty)                    
            ptrap = g0TrapSess_i * np.exp(-(distBT**2.0) / var2)
            if ptrap < 1.0e-200:
                ptrap = 1.0e-200
            PTrapID_3D_s[k] = ptrap
            pNoTrap = pNoTrap * (1.0 - ptrap)
    birdTotPObserved_2D_s = 1.0 - pNoObs
    birdTotPTrapped_2D_s = 1.0 - pNoTrap
    # return probability arrays
    
    return(PObsID_3D_s, PTrapID_3D_s, birdTotPObserved_2D_s,
            birdTotPTrapped_2D_s)

@jit
def NsLessNiMEM(i, Ni, birdPres_2D, minNPresent, sessionNSurveys, sessionNTraps,
        birdObserved_2D, birdTrapped_2D, birdTotPObserved_2D, birdTotPTrapped_2D,
        nsurvey_i, ntrap_i):
    """
    (21.1.1) When Ni > Ns:  rm individuals from proposed pop and remove probs.
             Function ids present individuals that can be removed.
             Present individual can't have been seen or trapped.
    """
#    remainingToTrial = (Ni - minNPresent[i]) * 1.0
    nRemoved = 0
    ind = 0 #np.random.randint(0, maxN)    #0                 # individual ID
#    moreSurvey_i = np.greater(sessNSurvey, sessNTraps)
    while nRemoved == 0:
        if birdPres_2D[i, ind] == 1:          # bird present
            #if not detected and present, then remove with multinomial prob
            detected_iInd = birdObserved_2D[i, ind] + birdTrapped_2D[i, ind]
#            if i < 5:
#                print(ind, birdPres_2D[i, ind], birdObserved_2D[i, ind], birdTrapped_2D[i, ind],
#                    detected_iInd)

            if detected_iInd == 0:
                nRemoved = 1
#                updateProb = 1.0 / remainingToTrial
#                if updateProb > 1.0:
#                    print(0.999999, updateProb)
#                    updateProb = 0.99999999
#                if updateProb < 0.0:
#                    print(-0.000, updateProb, Ni, nrem, nRemoved, minNPresent[i],
#                        remainingToTrial)
#                nRemoved = np.random.binomial(1, updateProb)
#                if i<6:
#                    print('i', i, 'ind', ind, 'Ni', Ni, 'nReved', nRemoved, 
#                        'nobTr', minNPresent[i],
#                        'remTrial', remainingToTrial, 'updProb', updateProb)
#                remainingToTrial = remainingToTrial - 1.0     # update multinom probs
        if nRemoved == 0:
            ind += 1
#            if ind == maxN:
#                ind = 0
    cellAdd = 0            # place holder for pnow updater
    birdID_s = ind
    birdTotPObserved_2D_s = birdTotPObserved_2D[i, birdID_s] 
    birdTotPTrapped_2D_s = birdTotPTrapped_2D[i, birdID_s]
#    birdPObservedID_3D_s = np.zeros(nsurvey_i)
#    birdPTrappedID_3D_s = np.zeros(ntrap_i)
    return(birdID_s, cellAdd, birdTotPObserved_2D_s, birdTotPTrapped_2D_s)

@jit
def NLogLikFX(i,  Ns, Ni, birdPres_2D, birdID_s, birdTotPObserved_2D_s, 
            birdTotPTrapped_2D_s, birdTotPObserved_2D, birdTotPTrapped_2D, 
            birdObserved_2D, birdTrapped_2D, maxN, lnGammaArray,
            prevCellCount_2D, birdLoc_2D):
    """
    (21.2)  calc binomial loglik for present and proposed data
    """
    binomNProb = 0.0                                    # set starting prob = 0.0
    binomNProb_s = 0.0
    sumObs_i = 0
    sumObs_i_s = 0
    sumTrapped_i = 0
    sumTrapped_i_s = 0
    for j in range(maxN):
        # loop thru individuals
        if birdPres_2D[i, j] == 1:                      # If pres individ
            probObs_ij = ((birdObserved_2D[i, j] * birdTotPObserved_2D[i, j]) +
                ((1.0 - birdObserved_2D[i, j]) * (1.0 - birdTotPObserved_2D[i, j])))
            probTrap_ij = ((birdTrapped_2D[i, j] * birdTotPTrapped_2D[i, j]) +
                ((1.0 - birdTrapped_2D[i, j]) * (1.0 - birdTotPTrapped_2D[i, j])))
            logTotProb_ij = np.log(probObs_ij) + np.log(probTrap_ij)
            binomNProb += logTotProb_ij
            sumObs_i += birdObserved_2D[i, j] 
            sumTrapped_i += birdTrapped_2D[i, j]
        # if added or removed bird
        if j == birdID_s:
            probObs_s = 1.0 - birdTotPObserved_2D_s # because not seen
            probTrap_s = 1.0 - birdTotPTrapped_2D_s # p not trapped
            logTotProb_s = (np.log(probObs_s) + np.log(probTrap_s))
        ####### get previous cell count for updating habitat
        if i > 0:
            if birdPres_2D[(i - 1), j] == 1:
                cellloc = birdLoc_2D[(i - 1), j]
                prevCellCount_2D[cellloc, i] += 1.0

    # get proposed population binom prob        
    if Ns > Ni:                                     # Add bird
        binomNProb_s = binomNProb + logTotProb_s    # add to make more negative
    else:
        binomNProb_s = binomNProb - logTotProb_s    # subtract to make less negative

    if i == 602:
        print(binomNProb, binomNProb_s, logTotProb_s)

    # calc Combinatorial term
    nNotObs = Ni - sumObs_i                         # n not observed for denominator
    nNotObs_s = Ns - sumObs_i                     # proposed not seen
    nNotTrapped = Ni - sumTrapped_i                 # n not trapped - denominator
    nNotTrapped_s = Ns - sumTrapped_i             # proposed not trapped
    numerator_i = lnGammaArray[Ni]                  # current numerator
    numerator_i_s = lnGammaArray[Ns]                # proposed numerator
    comboObs = numerator_i - lnGammaArray[nNotObs]
    comboObs_s = numerator_i_s - lnGammaArray[nNotObs_s] 
    comboTrap = numerator_i - lnGammaArray[nNotTrapped]
    comboTrap_s = numerator_i_s - lnGammaArray[nNotTrapped_s]
    nLogLik_i = (comboObs + comboTrap + binomNProb)
    nLogLik_i_s = (comboObs_s + comboTrap_s + binomNProb_s)
    return(nLogLik_i, nLogLik_i_s, prevCellCount_2D)


@jit
def NPredPoisson(i, Ni, Ns, N, Npred, Npred_s, reproMask, reproPeriodIndx, yearRecruitIndx,
        uYearIndx, relWrpCauchy, sessionRecruits, sessionRecruits_s, totalRecruits,
        reproPop, ig, rg, nsession, removeDat, sumReproPop, nSessInReproPeriod,
        lnGammaArray, sessionJul, maxSessionJul):
    """
    (23) given proposed Ns, get npred, recruits, reproPop, and calc Poisson likelihood
    """
    yrIndx = 0                                                  # not updated if not repro
    totalRecruits_s = 0.0                                         # not updated
    reproPop_s = 0.0                                            # not updated
    sumReproPop_s = 0
    # present Npred
    # if first session make Poisson = 0.0
    isRepro = reproMask[i]
    if i == 0:
        nPois_i = 0.0                                                   # first sess; pois=0.0
        nPois_i_s = 0.0
    else:                                                               # not first session 
        nGammaLn_i = lnGammaArray[Ni]
        nPois_i = ln_poissonPMF(Ni, nGammaLn_i, Npred[i])                     # poisson for Ni
        nGammaLn_i_s = lnGammaArray[Ns]
        nPois_i_s = ln_poissonPMF(Ns, nGammaLn_i_s, Npred[i])                 # poisson for Ns
    if (i < (nsession - 1)):                                            # not last session
        recruit = sessionRecruits[i+1]
        Npred_i1_s = Ns - removeDat[i] + recruit
        Npred_s[i+1] = Npred_i1_s
        Ni1 = N[i + 1]
        nGammaLn_i1 = lnGammaArray[Ni1]
        nPois_i1 = ln_poissonPMF(Ni1, nGammaLn_i1, Npred[i + 1])              # pois of 1 sess up
        nPois_i1_s = ln_poissonPMF(Ni1, nGammaLn_i1, Npred_i1_s)              # proposed poisson 1 up
        # if session is in repro period, predict for entire recruitment year
        if isRepro:
            yrIndx = reproPeriodIndx[i]
#            print('sumrepro', sumReproPop, 'yrindx', yrIndx)
            sumReproPop_s = (sumReproPop[yrIndx] - Ni + Ns)     #   )(yrIndx-1)
            reproPop_s = (sumReproPop_s / nSessInReproPeriod[yrIndx])
            totalRecruits_s = (reproPop_s * rg)
            nPoisRecruitPMF = 0.0
            nPoisRecruitPMF_s = 0.0
            ## cycle thru recruitment year
#            for j in range(nsession):

            sumRecruits = 0.0
            sessionCounter = 1      #i
            for qq in range(1, (maxSessionJul + 1)):
                OnSessJulDay = (sessionJul[sessionCounter] == qq)
                if yearRecruitIndx[qq] == yrIndx:
                    recruits_qq =  totalRecruits_s * relWrpCauchy[qq]
                    sumRecruits += recruits_qq
                    if OnSessJulDay:
                        sessionRecruits_s[sessionCounter] = sumRecruits
                        sumRecruits = 0.0
                        Npred_s[sessionCounter] = N[sessionCounter-1] - removeDat[sessionCounter-1] + sessionRecruits_s[sessionCounter]
                        NsessionCounter = N[sessionCounter]
                        nLnGamma = lnGammaArray[NsessionCounter]
                        nPoisRecruitPMF += ln_poissonPMF(NsessionCounter, nLnGamma, Npred[sessionCounter])
                        nPoisRecruitPMF_s += ln_poissonPMF(NsessionCounter, nLnGamma, Npred_s[sessionCounter])
                if OnSessJulDay:
                    sessionCounter += 1
            # If next is a recruitment - don't double count
            if not reproMask[i+1]:
                totNPois_i = nPois_i + nPoisRecruitPMF                  # recruit period and present N
                totNPois_i_s = nPois_i_s + nPoisRecruitPMF_s            # but not 1 session up
            else:                                                       # next is still in repro
                totNPois_i = nPois_i + nPois_i1 + nPoisRecruitPMF       # recruit, next and present N
                totNPois_i_s = nPois_i_s + nPois_i1_s + nPoisRecruitPMF_s
        else:                                                           # not repro session
            totNPois_i = nPois_i + nPois_i1                             # next and present N
            totNPois_i_s = nPois_i_s + nPois_i1_s
    ### if very last session
    else:
        totNPois_i = nPois_i                                            # next and present N
        totNPois_i_s = nPois_i_s
    return(Ns, Npred_s, sessionRecruits_s, totalRecruits_s, reproPop_s, sumReproPop_s, 
            totNPois_i, totNPois_i_s, yrIndx, isRepro)

@jit
def N_Pnow_Pnew(i, nLogLik_i, nLogLik_i_s, N, Ni, Ns, Npred, Npred_s, 
        sessionRecruits_s, cellAdd,
        totalRecruits_s, reproPop_s, sumReproPop_s, totNPois_i, totNPois_i_s,
        yrIndx, isRepro, sessions, nsession, yearRecruitIndx, sessionRecruits,
        totalRecruits, reproPop, sumReproPop, PObsID_3D_s, PTrapID_3D_s, 
        birdTotPObserved_2D_s, birdTotPTrapped_2D_s, 
        birdPObservedID_3D, birdPTrappedID_3D, birdTotPObserved_2D,
        birdTotPTrapped_2D, birdLoc_2D, birdPres_2D, birdID_s, nsurvey_i,
        ntrap_i):
    """
    (24) Calc N importance ratio, and update parameters
    """
#    if Ns > Ni:                           
#        sumP = np.sum(birdPObservedID_3D_s)
#        if sumP == 0.0:
#            print(-4.444, sumP)
#    if i < 4:
#        print(i, birdPObservedID_3D_s)
    

    pnow = nLogLik_i + totNPois_i
    pnew = nLogLik_i_s + totNPois_i_s
    # Calc IR
    rValue = np.exp(pnew - pnow)        
    zValue = np.random.random()

    if i==602:
        print(pnow, pnew, nLogLik_i, nLogLik_i_s, totNPois_i, totNPois_i_s)


    if rValue > zValue:                                                 # update parameters 
        Npred[i] = Npred_s[i]                                           # updata npred all i
        N[i] = Ns         
        if (i < (nsession - 1)):                                        # Not last session 
            Npred[i + 1] = Npred_s[i + 1]                               # update npred if not last
            if isRepro:
                reproPop[yrIndx] = reproPop_s                           # update repro pop values
                sumReproPop[yrIndx] = sumReproPop_s
                totalRecruits[yrIndx] = totalRecruits_s
                reproPop[yrIndx] = reproPop_s 
                sumReproPop[yrIndx] =  sumReproPop_s
                ## cycle thru recruitment year
                for j in range(nsession):
                    if yearRecruitIndx[j] == yrIndx:                    # if in recruitment period
                        Npred[j] = Npred_s[j]
                        sessionRecruits[j] = sessionRecruits_s[j]
        #### update probability and latent arrays
        if Ni > Ns:                                                     # if removed ind
            birdLoc_2D[i, birdID_s] = 0
            birdPres_2D[i, birdID_s] = 0
            if nsurvey_i >= ntrap_i:
                nloop = nsurvey_i
            else:
                nloop = ntrap_i
            for k in range(nloop):      # loop thru number survey or traps
                ######## Calc survey obs probability if surveyor present
                if k < nsurvey_i:
                    birdPObservedID_3D[i, birdID_s, k] = 0.0 
                ######## Calc trap probability if trap present
                if k < ntrap_i:
                    birdPTrappedID_3D[i, birdID_s, k] = 0.0 
            birdTotPObserved_2D[i, birdID_s] = 0.0
            birdTotPTrapped_2D[i, birdID_s] = 0.0
        ###### update probability and latent arrays if add individual
        if Ns > Ni:                           
#            if i < 8:
                       
            birdLoc_2D[i, birdID_s] = cellAdd
            birdPres_2D[i, birdID_s] = 1
            if nsurvey_i >= ntrap_i:
                nloop = nsurvey_i
            else:
                nloop = ntrap_i
            for k in range(nloop):      # loop thru number survey or traps
                ######## Calc survey obs probability if surveyor present
                if k < nsurvey_i:
                    pfill = PObsID_3D_s[k]
#                    pfill = pfill[0]
                    birdPObservedID_3D[i, birdID_s, k] = pfill

#                    sumP = np.sum(pfill)
#                    if (sumP == 0.0): 
#                        print(i, -3.333, Ns, Ni, nsurvey_i, birdPObservedID_3D_s)
#                        print(-4.444, sumP)
                    if pfill == 0.0:
                        print(8.888, i, Ns, Ni, nsurvey_i, k, pfill) 
                ######## Calc trap probability if trap present
                if k < ntrap_i:
                    birdPTrappedID_3D[i, birdID_s, k] = PTrapID_3D_s[k]
            birdTotPObserved_2D[i, birdID_s] = birdTotPObserved_2D_s
            birdTotPTrapped_2D[i, birdID_s] = birdTotPTrapped_2D_s
#    if i == 500:
#        print(Ni, Ns, totNPois_i, totNPois_i_s, rValue - zValue)
    return(N, Npred, Npred_s, sessionRecruits, totalRecruits, reproPop, 
        sumReproPop, birdPObservedID_3D, birdPTrappedID_3D, 
        birdTotPObserved_2D, birdTotPTrapped_2D, birdLoc_2D, birdPres_2D)

##########################################
##########################################  END UPDATING N
##########################################


##########################################
##########################################  UPDATE LOCATION AND HABITAT
#@profile
@jit
def locUpdateNumba(N, birdPObservedID_3D, birdPTrappedID_3D, birdTotPObserved_2D, 
        birdTotPTrapped_2D, birdLoc_2D, birdPres_2D, prevCellCount_2D, 
        LThMulti_2D, birdObserved_2D, birdTrapped_2D,  maxN, nCells,
        sessionNSurveys, sessionNTraps, g0Obs_Session, g0Trap_Session, cellX,
        cellY, surveyX_2D, surveyY_2D, trapX_2D, trapY_2D, var2, 
        birdObservedID_3D, birdTrappedID_3D, nsession, bPrev, bPrev_s,  bird_lth, 
        bird_lth_s, LThMulti_2D_s, sumExpTotHab, sumExpTotHab_s,
        PObsID_3D_s, PTrapID_3D_s):
    """
    (31) function to propose and update locations of birds in Session i
    """
    # initialise log likelihood for beta habitat update - below
    logLikHab = 0.0
    logLikHab_s = 0.0
    # loop thru sessions
    for i in range(nsession):
        g0ObsSess_i = g0Obs_Session[i]
        g0TrapSess_i = g0Trap_Session[i]
        nsurvey_i = sessionNSurveys[i]
        ntrap_i = sessionNTraps[i]
        if nsurvey_i >= ntrap_i:
            nloop = nsurvey_i
        else:
            nloop = ntrap_i
        # make empty arrays: 1-d arrays for probability pairs
#        birdPObservedID_3D_s = np.zeros(nsurvey_i)
#        birdPTrappedID_3D_s = np.zeros(ntrap_i)
        for j in range(maxN):                               # loop thru individuals
            if birdPres_2D[i, j] == 1:
                newCell = np.random.randint(0, nCells)
                xBirdLoc_s = cellX[newCell]
                yBirdLoc_s = cellY[newCell]
                pNoObs_s = 1.0
                pNoTrap_s = 1.0
                LLObsEvent = 0.0
                LLObsEvent_s = 0.0
                LLTrapEvent = 0.0
                LLTrapEvent_s = 0.0
                presentCell = birdLoc_2D[i, j]
                birdTrapped = birdTrapped_2D[i, j]      # trapped by any trap
                for k in range(nloop):      # loop thru number survey or traps
                    ######## Calc survey obs probability if surveyor present
                    if k < nsurvey_i:
                        sx = surveyX_2D[i, k]
                        sy = surveyY_2D[i, k]       # survey k northings
                        distBS = distxy(xBirdLoc_s, yBirdLoc_s, sx, sy)
                        pObs_s = g0ObsSess_i * np.exp(-(distBS**2.0) / var2)   #pObs_s 1 person
                        if pObs_s < 1.0e-200:
                            pObs_s = 1.0e-200
                        PObsID_3D_s[k] = pObs_s                # pobs bird j in survey k
                        pNoObs_s = pNoObs_s * (1. - pObs_s)
                        obsEvent = birdObservedID_3D[i, j, k]
                        pObsID = birdPObservedID_3D[i, j, k]
                        pObsEvent = (pObsID * obsEvent) + ((1. - pObsID) * (1. - obsEvent)) 
                        LLObsEvent = LLObsEvent + np.log(pObsEvent)
                        pObsEvent_s = (pObs_s * obsEvent) + ((1. - pObs_s) * (1. - obsEvent))
                        LLObsEvent_s = LLObsEvent_s + np.log(pObsEvent_s)
                    ######## Calc trap probability if trap present
                    if k < ntrap_i:
                        tx = trapX_2D[i, k]
                        ty = trapY_2D[i, k]       # trap k northings
                        distBS = distxy(xBirdLoc_s, yBirdLoc_s, tx, ty)
                        pTrap_s = g0TrapSess_i * np.exp(-(distBS**2.0) / var2)   #pTrap_s 1 person
                        if pTrap_s < 1.0e-200:
                            pTrap_s = 1.0e-200
                        PTrapID_3D_s[k] = pTrap_s                    # pobs bird j in trap k
                        pNoTrap_s = pNoTrap_s * (1.0 - pTrap_s)
                        trapEvent = birdTrappedID_3D[i, j, k]
                        if trapEvent == 1:
                            pTrapID = birdPTrappedID_3D[i, j, k]
                            LLTrapEvent = np.log(pTrapID)
                            LLTrapEvent_s = np.log(pTrap_s)
                # total prob of obs and trap across all survey and traps
                if nsurvey_i > 0:
                    birdTotPObserved_2D_s = 1.0 - pNoObs_s
                if ntrap_i > 0:         # get total prob of capture across traps
                    pTotTrap_s = 1.0 - pNoTrap_s
                    if pTotTrap_s < 1.0e-200:
                        pTotTrap_s = 1.0e-200
                    birdTotPTrapped_2D_s = pTotTrap_s
                    totPTrap = birdTotPTrapped_2D[i, j]
                    LLTotTrap = (np.log((totPTrap * birdTrapped) + 
                        ((1. - totPTrap) * (1. - birdTrapped))))  # total LL trapped
                    LLTotTrap_s = (np.log((birdTotPTrapped_2D_s * birdTrapped) + 
                        ((1. - birdTotPTrapped_2D_s) * (1. - birdTrapped))))     # total LL trapped
                else:
                    LLTotTrap = 0.0
                    LLTotTrap_s = 0.0
                ## Habitat likelihood
                LLCell = LThMulti_2D[presentCell, i]
                if LLCell < 1.0e-200:
                    LLCell = 1.0e-200
                LLCell = np.log(LLCell)
                LLCell_s = LThMulti_2D[newCell, i]
                if LLCell_s < 1.0e-200:
                    LLCell_s = 1.0e-200
                LLCell_s = np.log(LLCell_s)
#                print((LLObsEvent), (LLTotTrap), (LLTrapEvent), LLCell)
#                print((LLObsEvent_s), (LLTotTrap_s), (LLTrapEvent_s), (LLCell_s))
                ## calc likelihoods and IR
                LLTot = LLObsEvent + LLTotTrap + LLTrapEvent + LLCell
                LLTot_s = LLObsEvent_s + LLTotTrap_s + LLTrapEvent_s + LLCell_s
                # Calc IR
                rValue = np.exp(LLTot_s - LLTot)
                zValue = np.random.random()
                if rValue > zValue:                                     # update parameters
                    birdLoc_2D[i, j] = newCell
                    if i < (nsession - 1):
                        prevCellCount_2D[presentCell, (i + 1)] += -1.0
                        prevCellCount_2D[newCell, (i + 1)] += 1.0
                        # update LthMulti up one session
                        ######## present cell    
                        cellCountTmp =  prevCellCount_2D[presentCell, (i + 1)]
                        
                        prevCellPredict = (bPrev * cellCountTmp) 
                        lthCellPresent = bird_lth[presentCell]
#                        if i == 0:
#                            print(bPrev, cellCountTmp, prevCellPredict , lthCellPresent)
                        expTotHabPresentCell = np.exp(prevCellPredict + lthCellPresent)

#                        if i == 0:
#                            print('prescell', presentCell, 'newcell', newCell)
#                            print('expPresent', expTotHabPresentCell, np.shape(expTotHabPresentCell))
#                            print('quotient', expTotHabPresentCell/sumExpTotHab[(i + 1)])
#                            print('prevcellcount', prevCellCount_2D[presentCell, (i + 1)])
#                            print('new prevcellcount', prevCellCount_2D[newCell, (i + 1)])
                        sumHabTmp = sumExpTotHab[(i + 1)]
#                            print('sumhab', sumExpTotHab[(i + 1)])
                        lthprestmp = expTotHabPresentCell / sumHabTmp
                        LThMulti_2D[presentCell, (i + 1)] = lthprestmp[0] 
                        ####### newCell
                        cellCountNew = prevCellCount_2D[newCell, (i + 1)]
                        lth_new =  bird_lth[newCell]
                        prevCellPredictNew = bPrev * cellCountNew
                        expTotHabNewCell = np.exp(prevCellPredictNew + lth_new) 
                        lth_up1 = expTotHabNewCell / sumHabTmp
                        LThMulti_2D[newCell, (i + 1)] = lth_up1[0] 
                        #################
                        #################     PROPOSED HABITAT VALUES UPDATED FOR BETA UPDATE
                        # update LthMulti_s up one session
                        ######## present cell    
                        lth_pres_s = bird_lth_s[presentCell]
                        prevPredPres_s = bPrev_s * cellCountTmp
                        expTotHabPresentCell_s = np.exp(prevPredPres_s + lth_pres_s)
                        sumhab_s = sumExpTotHab_s[(i + 1)]
                        lthPres_s = expTotHabPresentCell_s / sumhab_s
                        LThMulti_2D_s[presentCell, (i + 1)] = lthPres_s[0] 
                        ####### newCell
                        lth_new_s = bird_lth_s[newCell]
                        prevPredNew_s = bPrev_s * cellCountNew
                        expTotHabNewCell_s = np.exp(prevPredNew_s + lth_new_s)
                        newLTh_up1_s = expTotHabNewCell_s / sumhab_s


#                        if i == 0:
#                            print('prescell', presentCell, 'newcell', newCell)
#                            print('expPresent', expTotHabPresentCell_s, np.shape(expTotHabPresentCell_s))
#                            print('quotient', expTotHabPresentCell_s/sumExpTotHab_s[(i + 1)])
#                            print('prevcellcount', prevCellCount_2D[presentCell, (i + 1)])
#                            print('new prevcellcount', prevCellCount_2D[newCell, (i + 1)])
#                            print('sumhab_s', sumExpTotHab_s[(i + 1)])
#                        if i == 0:
#                            print(expTotHabNewCell_s, sumhab_s , expTotHabNewCell_s/sumhab_s)                        


                        LThMulti_2D_s[newCell, (i + 1)] = newLTh_up1_s[0]

#                        LThMulti_2D_s[newCell, (i + 1)] = (expTotHabNewCell_s / 
#                            sumExpTotHab_s[(i + 1)]) 
                        #################     END UPDATING HABITAT VALUES FOR BETA UPDATE
                        #################
                    # update probability values
                    if ntrap_i > 0:
                        birdTotPTrapped_2D[i, j] = birdTotPTrapped_2D_s
                    if nsurvey_i > 0:
                        birdTotPObserved_2D[i, j] = birdTotPObserved_2D_s 
                    for m in range(nloop):      # loop thru number survey or traps
                        ######## populate survey obs probability if surveyor present
                        if m < nsurvey_i:
                            birdPObservedID_3D[i, j, m] = PObsID_3D_s[m]
                        ######## populate trap probability if trap present
                        if m < ntrap_i:
                            birdPTrappedID_3D[i, j, m] = PTrapID_3D_s[m]
        #################
        #################     LIKELIHOODS FOR HABITAT BETAS FOR SESSION i
        (logLikHab, logLikHab) = betaHabLogLik(i, prevCellCount_2D, nCells, 
                        LThMulti_2D, LThMulti_2D_s, logLikHab, logLikHab_s)
            
    ### return Arrays
    return(birdLoc_2D, birdTotPTrapped_2D, birdTotPObserved_2D, prevCellCount_2D, 
        birdPObservedID_3D, birdPTrappedID_3D, LThMulti_2D, LThMulti_2D_s, logLikHab,
        logLikHab_s)

@jit
def betaHabLogLik(i, prevCellCount_2D, nCells, LThMulti_2D, LThMulti_2D_s,
        logLikHab, logLikHab_s):
    """
    (31.1) get multinomial likelihood for habitat betas by session
    """
    for cc in range(nCells):
        nbirds = prevCellCount_2D[cc, i]
        if nbirds > 0:
            logLikHab += (np.log(LThMulti_2D[cc, i]) * nbirds)
            logLikHab_s += (np.log(LThMulti_2D_s[cc, i]) * nbirds)
    return(logLikHab, logLikHab_s)
##########################################  END HABITAT AND LOCATION UPDATE
##########################################


##########################################
##########################################  OBSERVER - OBSERVED-BIRD PAIRS
@jit
def updateObserverBird(nsession, birdPres_2D, birdPObservedID_3D, 
        birdObserved_2D, birdObservedID_3D, 
        sessionNSurveys, maxN, birdShotID_3D):
    """
    (32) update bird ID that are observed by each observer
    """
    for i in range(nsession):
        nsurvey_i = sessionNSurveys[i]
        for j in range(nsurvey_i):
            for k in range(maxN):
                ## only change obs bird that is seen but not shot
                if (birdObservedID_3D[i, k, j] == 1) & (birdShotID_3D[i, k, j] == 0):
                    bernProb = birdPObservedID_3D[i, k, j]
                    ### find alternative bird
                    randID = np.random.randint(0, maxN)
                    stopSearch = 0
                    cc = 0
                    while stopSearch == 0:
                        if birdPres_2D[i, randID] == 1:
                            if randID == k:
                                randID += 1
                                if randID == maxN:
                                    randID = 0
                            else:
                                bernProb_s = birdPObservedID_3D[i, randID, j]
                                stopSearch = 1
                        else:
                            randID += 1
                            if randID == maxN:
                                randID = 0
                            cc += 1 
                            if cc == maxN:
                                stopSearch = 1

#                    if bernProb == 0.0:
#                        print('change obs bird: i j k, and bernP', i, j, k, bernProb)
#                    if bernProb_s == 0.0:
#                        print('change obs bird_s: i j k, and bernP_s', i, j, k, bernProb_s)

                    # Calc IR
                    rValue = np.exp(np.log(bernProb_s) - np.log(bernProb))
                    zValue = np.random.random()
                    if rValue > zValue:                                     # update parameters
                        birdObservedID_3D[i, randID, j] == 1
                        birdObservedID_3D[i, k, j] == 0
                        birdObserved_2D[i, randID] = 1
                        nTimesObs = 0
                        for m in range(nsurvey_i):
                            nTimesObs += birdObservedID_3D[i, k, m]
                        if nTimesObs == 0:
                            birdObserved_2D[i, k] = 0
    return(birdObservedID_3D, birdObserved_2D) 
##########################################
##########################################  END OBSERVER - OBSERVED-BIRD PAIRS


##########################################
##########################################  UPDATE PKILL AND RELWRPCAUCHY PARAMETERS
@jit
def pkill_LogLik(pKill_Session, pKill_Session_s, 
        birdObservedID_3D, birdShotID_3D,
        nsession, sessionNSurveys, 
        maxN, birdPres_2D):
    """
    (33.1) calc binomial log lik of kill by shooting data given pkill and relwrpC
    """
    sumLLKill = 0.0
    sumLLKill_s = 0.0
    for i in range(nsession):
        pKillSess_i = pKill_Session[i]
        pKillSess_i_s = pKill_Session_s[i]
        nsurvey_i = sessionNSurveys[i]
        for j in range(maxN):
            if birdPres_2D[i, j] == 1:
                for k in range(nsurvey_i):
                    if birdObservedID_3D[i, j, k] == 1:
                        shotEvent = birdShotID_3D[i, j, k]
                        sumLLKill += np.log((pKillSess_i * shotEvent) + 
                            ((1.0 -pKillSess_i) * (1.0 - shotEvent)))
                        sumLLKill_s += np.log((pKillSess_i_s * shotEvent) + 
                            ((1.0 -pKillSess_i_s) * (1.0 - shotEvent)))
    return(sumLLKill, sumLLKill_s)
##########################################
##########################################  END PKILL AND RELWRPCAUCHY PARAMETERS



##########################################
##########################################  UPDATE g0 TRAP AND RELWRPCAUCHY PARAMETERS
@jit
def g0_LogLik(g0Trap_Session, g0Trap_Session_s,
        birdPTrappedID_3D, birdTrappedID_3D, birdTrapped_2D,
        birdTotPTrapped_2D, birdPTrappedID_3D_s, birdTotPTrapped_2D_s,
        nsession, sessionNTraps, maxN, birdPres_2D):
    """
    (34.1) calc binomial log lik of kill by shooting data given pkill and relwrpC
    """
    sumLLg0 = 0.0
    sumLLg0_s = 0.0
    for i in range(nsession):
        g0TrapSess_i = g0Trap_Session[i]
        g0TrapSess_i_s = g0Trap_Session_s[i]
        ntrap_i = sessionNTraps[i]
        for j in range(maxN):
            if birdPres_2D[i, j] == 1:
                pNoTrap_s = 1.0
                birdTrapped = birdTrapped_2D[i, j]
                multiNomProb = 1.0
                multiNomProb_s = 1.0
                sumMultiNomProb = 0.0
                sumMultiNomProb_s = 0.0
                if birdTrapped == 0:
                    sumMultiNomProb = 1.0
                    sumMultiNomProb_s = 1.0
                for k in range(ntrap_i):
                    pairwiseTrapEvent = birdTrappedID_3D[i, j, k]
                    pT_ijk = birdPTrappedID_3D[i, j, k]
#                    pT_ijk_s = np.exp(np.log(pT_ijk) - np.log(g0TrapSess_i) + np.log(g0TrapSess_i_s))            
                    pT_ijk_s = pT_ijk / g0TrapSess_i * g0TrapSess_i_s            
                    if pT_ijk_s < 1.0e-200:
                        pT_ijk_s = 1.0e-200
                    birdPTrappedID_3D_s[i, j, k] = pT_ijk_s
                    pNoTrap_s = pNoTrap_s * (1.0 - pT_ijk_s)
                    #### if bird trapped get multinom probs
                    if birdTrapped == 1:
                        sumMultiNomProb += pT_ijk
                        sumMultiNomProb_s += pT_ijk_s
                    if pairwiseTrapEvent == 1:
                        multiNomProb = pT_ijk
                        multiNomProb_s = pT_ijk_s    
                multiNomProb = multiNomProb / sumMultiNomProb
                multiNomProb_s = multiNomProb_s / sumMultiNomProb_s
                if multiNomProb < 1.e-20:
                    multiNomProb = 1.e-20
                if multiNomProb_s < 1.e-20:
                    multiNomProb_s = 1.e-20
                pTottrap_s = 1.0 - pNoTrap_s
                if pTottrap_s < 1.0e-200:
                    pTottrap_s = 1.0e-200
                birdTotPTrapped_2D_s[i, j] = pTottrap_s                # proposed tot pTrap
                logBinomProb = np.log((birdTotPTrapped_2D[i, j] * birdTrapped) +
                    (1.0 - birdTotPTrapped_2D[i, j]) * (1.0 -  birdTrapped))
                logBinomProb_s = np.log((birdTotPTrapped_2D_s[i, j] * birdTrapped) +
                    (1.0 - birdTotPTrapped_2D_s[i, j]) * (1.0 -  birdTrapped))
                logMultiNomProb = np.log(multiNomProb)
                logMultiNomProb_s = np.log(multiNomProb_s)

                ### get additive LL across all birds
                sumLLg0 += logBinomProb + logMultiNomProb
                sumLLg0_s += logBinomProb_s + logMultiNomProb_s
    return(sumLLg0, sumLLg0_s, birdPTrappedID_3D_s, birdTotPTrapped_2D_s)
##########################################
##########################################  END g0trap AND RELWRPCAUCHY PARAMETERS

##########################################
##########################################  UPDATE g0 OBSERVED AND RELWRPCAUCHY PARAMETERS
@jit
def obs_g0_LogLik(g0Obs_Session,
            g0Obs_Session_s, birdPObservedID_3D,
            birdObservedID_3D, birdTotPObserved_2D, birdPObservedID_3D_s,
            birdTotPObserved_2D_s, nsession,
            sessionNSurveys, maxN, birdPres_2D):
    """
    (34.1) calc binomial log lik of OBSERVATION DATA G0OBS and relwrpC
    """
    sumLLg0 = 0.0
    sumLLg0_s = 0.0
    for i in range(nsession):
        g0ObsSess_i = g0Obs_Session[i]
        g0ObsSess_i_s = g0Obs_Session_s[i]
        nsurvey_i = sessionNSurveys[i]
        for j in range(maxN):
            if birdPres_2D[i, j] == 1:
                pNoObs_s = 1.0
                for k in range(nsurvey_i):
                    birdObserved = birdObservedID_3D[i, j, k]
                    pO_ijk = birdPObservedID_3D[i, j, k]
#                    if pO_ijk == 0.0:
#                        print(9.99999, i, j, k, pO_ijk)
                 



                    pO_ijk_s = pO_ijk / g0ObsSess_i * g0ObsSess_i_s            
                    if pO_ijk_s < 1.0e-200:
                        pO_ijk_s = 1.0e-200
                    birdPObservedID_3D_s[i, j, k] = pO_ijk_s
                    pNoObs_s = pNoObs_s * (1.0 - pO_ijk_s)
                    sumLLg0 += np.log((pO_ijk * birdObserved) +
                        ((1.0 - pO_ijk) * (1.0 -  birdObserved)))
                    sumLLg0_s += np.log((pO_ijk_s * birdObserved) +
                        ((1.0 - pO_ijk_s) * (1.0 -  birdObserved)))
                birdTotPObserved_2D_s[i, j] = 1.0 - pNoObs_s                # proposed tot pTrap
    return(sumLLg0, sumLLg0_s, birdPObservedID_3D_s, birdTotPObserved_2D_s)
##########################################
##########################################  END g0 OBSERVED AND RELWRPCAUCHY PARAMETERS


##########################################
##########################################  UPDATE SIGMA HOME-RANGE PARAMETER
@jit
def sigma_LogLik(g0Trap_Session, g0Obs_Session, var2, var2_s,
        birdPTrappedID_3D, birdTrappedID_3D, birdTrapped_2D,
        birdTotPTrapped_2D, birdPTrappedID_3D_s, birdTotPTrapped_2D_s,
        birdPObservedID_3D, birdObservedID_3D, birdTotPObserved_2D, 
        birdPObservedID_3D_s, birdTotPObserved_2D_s, nsession, 
        sessionNTraps, sessionNSurveys, maxN, birdPres_2D):
    """
    (34.1) calc binomial log lik of kill by shooting data given pkill and relwrpC
    """
    sumLLObs = 0.0
    sumLLObs_s = 0.0
    sumLLTrap = 0.0
    sumLLTrap_s = 0.0
    for i in range(nsession):
        g0TrapSess_i = g0Trap_Session[i]
        g0ObsSess_i = g0Obs_Session[i]
        ntrap_i = sessionNTraps[i]
        nsurvey_i = sessionNSurveys[i]
        if nsurvey_i >= ntrap_i:
            nloops = nsurvey_i
        else:
            nloops = ntrap_i
        for j in range(maxN):
            if birdPres_2D[i, j] == 1:
                pNoTrap_s = 1.0
                pNoObs_s = 1.0
                birdTrapped = birdTrapped_2D[i, j]
                multiNomProb = 1.0
                multiNomProb_s = 1.0
                sumMultiNomProb = 0.0
                sumMultiNomProb_s = 0.0
                if birdTrapped == 0:
                    sumMultiNomProb = 1.0
                    sumMultiNomProb_s = 1.0
                for k in range(nloops):
                    if k < ntrap_i:
                        ############### GET TRAP PROBABILITIES
                        pairwiseTrapEvent = birdTrappedID_3D[i, j, k]
                        pT_ijk = birdPTrappedID_3D[i, j, k]
                        logPT = np.log(pT_ijk)
                        logG0 = np.log(g0TrapSess_i)

                        expterm = logPT - logG0     # get new expterm
                        expterm_s = expterm * var2 / var2_s                 # with new sigma_s
                        pT_ijk_s = g0TrapSess_i * np.exp(expterm_s)
                        if pT_ijk_s < 1.0e-200:
                            pT_ijk_s = 1.0e-200
                        birdPTrappedID_3D_s[i, j, k] = pT_ijk_s
                        pNoTrap_s = pNoTrap_s * (1.0 - pT_ijk_s)
                        #### if bird trapped get multinom probs
                        if birdTrapped == 1:
                            sumMultiNomProb += pT_ijk
                            sumMultiNomProb_s += pT_ijk_s
                        if pairwiseTrapEvent == 1:
                            multiNomProb = pT_ijk
                            multiNomProb_s = pT_ijk_s
                    ############## GET OBSERVATION PROBS
                    if k < nsurvey_i:
                        birdObserved = birdObservedID_3D[i, j, k]
                        pO_ijk = birdPObservedID_3D[i, j, k]
#                        if pO_ijk == 0.0:
#                            print('i j k', i, j, k)

                        logPO = np.log(pO_ijk)
                        logG0_Obs =  np.log(g0ObsSess_i)
                        expterm = logPO - logG0_Obs      # obs expterm
                        expterm_s = expterm * var2 / var2_s                 # new sigma_s
                        pO_ijk_s = g0ObsSess_i * np.exp(expterm_s)
                        if pO_ijk_s < 1.0e-200:
                            pO_ijk_s = 1.0e-200
                        birdPObservedID_3D_s[i, j, k] = pO_ijk_s
                        pNoObs_s = pNoObs_s * (1.0 - pO_ijk_s)
                        sumLLObs += np.log((pO_ijk * birdObserved) +
                            ((1.0 - pO_ijk) * (1.0 -  birdObserved)))
                        sumLLObs_s += np.log((pO_ijk_s * birdObserved) +
                            ((1.0 - pO_ijk_s) * (1.0 -  birdObserved)))
                birdTotPObserved_2D_s[i, j] = 1.0 - pNoObs_s                # proposed tot pObser
                ####### GET TRAP DATA LL  
                multiNomProb = multiNomProb / sumMultiNomProb
                multiNomProb_s = multiNomProb_s / sumMultiNomProb_s

                if multiNomProb < 1.e-200:
                    multiNomProb = 1.e-200
                if multiNomProb_s < 1.e-200:
                    multiNomProb_s = 1.e-200
                pTrap_ij_s = 1.0 - pNoTrap_s 
                if pTrap_ij_s < 1.0e-200:
                    pTrap_ij_s = 1.0e-200               
                birdTotPTrapped_2D_s[i, j] = pTrap_ij_s                # proposed tot pTrap
                logBinomProb = np.log((pTrap_ij_s * birdTrapped) +
                    (1.0 - pTrap_ij_s) * (1.0 -  birdTrapped))
                logBinomProb_s = np.log((pTrap_ij_s * birdTrapped) +
                    (1.0 - pTrap_ij_s) * (1.0 -  birdTrapped))
                logMultiNomProb = np.log(multiNomProb)
                logMultiNomProb_s = np.log(multiNomProb_s)
                ### get additive TRAP AND OBS LL across all birds
                sumLLTrap += logBinomProb + logMultiNomProb
                sumLLTrap_s += logBinomProb_s + logMultiNomProb_s
    totLLSigma = sumLLTrap + sumLLObs
    totLLSigma_s = sumLLTrap_s + sumLLObs_s
    return(totLLSigma, totLLSigma_s, birdPTrappedID_3D_s, birdTotPTrapped_2D_s, 
            birdPObservedID_3D_s, birdTotPObserved_2D_s)
##########################################
##########################################  END SIGMA HOME-RANGE PARAMETERS




@jit
def alignmentTest(birdPObservedID_3D,
            birdObservedID_3D, birdTotPObserved_2D,nsession,
            sessionNSurveys, maxN, birdPres_2D):
    """
    TEST FUNCTION, BUT DOESN'T WORK, NOT SURE WHY
    """

    for i in range(nsession):
        nsurvey_i = sessionNSurveys[i]
        for j in range(maxN):
#            sumBirdObs = 0
            pNoObs = 1.0
            birdPres_j = birdPres_2D[i, j]
            if birdPres_j == 1:
                for k in range(nsurvey_i):
#                    sumBirdObs += birdObservedID_3D[i, j, k]
                    pNoObs = pNoObs * (1.0 - birdPObservedID_3D[i,j,k])
                    if i < 250:
                        if ((birdPres_j == 1) & (birdPObservedID_3D[i,j,k] == 0.0)):
                            print(1.00001, i, j, k, nsurvey_i)
                        if ((birdPres_j == 0) & (birdPObservedID_3D[i,j,k] > 0.0)):
                            print(1.1111111, i, j, k, birdPObservedID_3D[i,j,k])
                        if ((birdObservedID_3D[i,j,k] == 1) & (birdPObservedID_3D[i,j,k] == 0.0)):
                            print(3.33333, i, j, k, nsurvey_i, birdPObservedID_3D[i,j,k])
                totProb = 1.0 - pNoObs
                if (totProb != birdTotPObserved_2D[i, j]):
                    print(2.222222, i, j, totProb, birdTotPObserved_2D[i, j])

##########################################
##########################################  BASICDATA
##########################################                               
class BasicData(object):
    def __init__(self, starlingdata, params):
        """
        Object to read in starling survey data
        Import updatable params from params
        """
        #############################
        #############################
        ##### Run basicdata functions
        ##
        self.getstarlingArrays(starlingdata, params)
#        self.makeReproMask()
#        self.makePseudoSession()
#        self.makeYearRecruitIndx()
        self.removeDatFX()
        self.getInitialN()
#        self.getNData()
        self.make_llArrays()
        self.NgammaLN()
        self.getNSessInYear()
    #################
    #################
    ##  Basicdata function definitions
    #
    def getstarlingArrays(self, starlingdata, params):
        """
        (1) function to create essential data arrays
        """
        self.params = params
        self.moveStarlingToBasicdata(starlingdata)
#        self.makeCovariateArrays()
        self.setInitialParameters()
        self.relWrpCauchy = self.calcRelWrapCauchy(self.rpara)
        self.make_g0_daypiTemplate()

#        self.testRWC()


    def moveStarlingToBasicdata(self, starlingdata):
        """
        (2) move the data in pickle into Basicdata
        """
        # data associated with survey data
#        self.surveyPropertyId = starlingdata.surveyPropertyId
        self.surveyJul = starlingdata.surveyJul
        self.surveyJulYear = starlingdata.surveyJulYear
        self.surveyYear = starlingdata.surveyYear
        self.surveyMon = starlingdata.surveyMon
        self.surveyDay = starlingdata.surveyDay
        self.surveyObs = starlingdata.surveyObs.astype(int)
        self.surveyRemoved = starlingdata.surveyRemoved.astype(int)
        self.surveyX = starlingdata.surveyX
        self.surveyY = starlingdata.surveyY
        self.surveySession = starlingdata.surveySession
        self.nSurvey = starlingdata.nSurvey     #len(self.surveyY)
        self.surveyID = starlingdata.surveyID   #np.arange(self.nSurvey, dtype = int)
        ## trap data
        self.trapid = starlingdata.trapid
        self.trapJul = starlingdata.trapJul
        self.trapJulYear = starlingdata.trapJulYear
        self.trapYear = starlingdata.trapYear
        self.trapMon = starlingdata.trapMon
        self.trapDay = starlingdata.trapDay
#        self.trapAdult = starlingdata.trapAdult
#        self.trapJuvenile = starlingdata.trapJuvenile
        self.trapRemoved = starlingdata.trapRemoved.astype(int)
        self.trapX = starlingdata.trapX
        self.trapY = starlingdata.trapY
        self.trapSession = starlingdata.trapSession
        # session data
        self.sessions = starlingdata.sessions.astype(int)
        self.sessionYear = starlingdata.sessionYear
        self.sessionJulYear = starlingdata.sessionJulYear
        self.sessionJul = starlingdata.sessionJul.astype(int)
        self.maxSessionJul = np.max(self.sessionJul)
        self.nsession = starlingdata.nsession
        self.sessionDayPi = starlingdata.sessionDayPi
        self.sessionRelWrpC = np.zeros(self.nsession)
        self.sessionDates = starlingdata.sessionDates
        print('sessdates', self.sessionDates[:10])

        #        ujul = np.unique(self.sessionJulYear)
        # Recruit data
        self.yearRecruitIndx = starlingdata.yearRecruitIndx
        self.recruitYear = starlingdata.recruitYear
        self.recruitJulYear = starlingdata.recruitJulYear
        self.daypiRecruit = starlingdata.daypiRecruit
        self.nRecruitJulYear = starlingdata.nRecruitJulYear
        self.recruitSession = starlingdata.recruitSession
        self.uYearIndx = starlingdata.uYearIndx
        self.nYears = len(self.uYearIndx)
        # count survey trap session data
        self.minNPresent = starlingdata.minNPresent
        self.maxSurvey = starlingdata.maxSurvey
        self.maxTrap = starlingdata.maxTrap
        self.sessionNSurveys = starlingdata.sessionNSurveys
        self.sessionNTraps = starlingdata.sessionNTraps
        self.sumTrapRem = starlingdata.sumTrapRem
        print('nsess', self.nsession)
        print('maxTrap', self.maxTrap, 'maxSurvey', self.maxSurvey)
#        print('nsess', self.nsession, 'sessjul', self.sessionJul[:5],
#            'survJul', self.surveyJul[:5], 'min surv Jul', np.min(self.surveyJul),
#            'trapJul', self.trapJul[:5], 'mintrapjul', np.min(self.trapJul))

#        print('maxSessionJul', self.maxSessionJul, 'len sessionJul', self.sessionJul)
        # property data
#        self.propPropertyid = starlingdata.propPropertyid
#        self.propX = starlingdata.propX
#        self.propY = starlingdata.propX
        # cell data
        self.nCells = starlingdata.nCells
####        self.cellX = starlingdata.cellX
###        self.cellY = starlingdata.cellY
###        self.cellSwamp = starlingdata.cellSwamp
###        self.cellNatVeg = starlingdata.cellNatVeg
#        self.cellNDVI = starlingdata.cellNdvi
###        self.cellID = np.arange(self.nCells).astype(int)

        ### NEW 2D APPROACH
        self.extent_xmin = starlingdata.extent_xmin
        self.extent_ymin = starlingdata.extent_ymin
        self.extent_xmax = starlingdata.extent_xmax
        self.extent_ymax = starlingdata.extent_ymax
        self.resol = starlingdata.resol
        self.halfRes = self.resol / 2.0
        self.nrows = starlingdata.nrows
        self.ncols = starlingdata.ncols 
        self.extMask = starlingdata.extMask
#        self.logX_2D = starlingdata.scaleX_2D
#        self.logY_2D = starlingdata.scaleY_2D
        self.logNatVeg_2D = starlingdata.logNatVeg_2D
        self.logSwamp_2D = starlingdata.logSwamp_2D

###        self.rowID = starlingdata.rowID
###        self.colID = starlingdata.colID
###        self.extMask = starlingdata.extMask
###        self.swamp2D = starlingdata.swamp2D
###        self.natVeg2D = starlingdata.natVeg2D
####        self.countSurveyInSess()
        self.getSurveyTrapXY()


    def getSurveyTrapXY(self):
        """
        (2.2) make 2-d arrays of survey and trap X and Y data by session
        #     make observed, shot and trapped data arrays by device and session
        #     Data arrays correspond to X and Y data arrays
        """
        self.surveyX_2D = np.zeros((self.nsession, self.maxSurvey))
        self.surveyY_2D = np.zeros((self.nsession, self.maxSurvey))
        self.trapX_2D = np.zeros((self.nsession, self.maxTrap))
        self.trapY_2D = np.zeros((self.nsession, self.maxTrap))
        self.observedData_2D = np.zeros((self.nsession, self.maxSurvey), dtype = int)
        self.trappedData_2D = np.zeros((self.nsession, self.maxTrap), dtype = int)
        self.shotData_2D = np.zeros((self.nsession, self.maxSurvey), dtype = int)
        for i in self.sessions:
            surveyX = self.surveyX[self.surveySession == i]
            nsx = len(surveyX)
            surveyY = self.surveyY[self.surveySession == i]
            trapX = self.trapX[self.trapSession == i]
            trapY = self.trapY[self.trapSession == i]
            observed_i = self.surveyObs[self.surveySession == i]
            trapped_i = self.trapRemoved[self.trapSession == i]
            shot_i = self.surveyRemoved[self.surveySession == i]
            ntx = len(trapX)
            if nsx > 0:
                self.surveyX_2D[i, :nsx] = surveyX
                self.surveyY_2D[i, :nsx] = surveyY
                self.observedData_2D[i, :nsx] = observed_i
                self.shotData_2D[i, : nsx] = shot_i
            if ntx > 0:
                self.trapX_2D[i, :ntx] = trapX
                self.trapY_2D[i, :ntx] = trapY
                self.trappedData_2D[i, :ntx] = trapped_i
#        print('surveyRemoved',self.surveyRemoved[200:1000])
#        print('self.shotData_2D', self.shotData_2D[500:520, :8])
#        print('self.observedData_2D', self.observedData_2D[500:520, :8])
#        dif = self.observedData_2D - self.shotData_2D
#        print('min diff', np.min(dif), 'max', np.max(dif))
#        difSurv = self.surveyObs - self.surveyRemoved
#        print('min Surv diff', np.min(difSurv), 'max', np.max(difSurv))


    def setInitialParameters(self):
        """
        (4) set initial parameters in basicdata
            moved from params so that we can update
        """
        self.sigma = self.params.sigma
        self.var2 = 2.0 * (self.sigma**2.0)
        ## g0 observation parameters and initial values
        self.g0_Obs = self.params.g0_Obs   
        self.g0_ObsWrpC = self.params.g0_ObsWrpC
        ## g0 Trap parameters and initial values
        self.g0_Trap = self.params.g0_Trap
        self.g0_TrapWrpC = self.params.g0_TrapWrpC
        ## probaility of kill parameters and initial values
        self.pKill_Max = self.params.pKill_Max
        self.pKill_WrpC = self.params.pKill_WrpC
        ## immigration parameters
#        self.ig = self.params.ig
        ##################      # Reproduction parameters
        self.rg = self.params.rg
        # latent initial number of recruits ~ uniform(10, 100)
        self.initialOctPop = self.params.initialOctPop
        # wrp cauchy parameters for distributing recruits
        self.rpara = self.params.rpara           # real numbers: normally distributed
        ## covaritat parameters
        self.b = np.expand_dims(self.params.b, 1)
        self.nbcov  = self.params.nbcov
#        self.bPrev = self.params.bPrev
        # maximum number of predators from params
        self.nFlocks = self.params.nFlocks
        self.nFlocksSample = self.params.nFlocksSample
        self.nFlocksTotYear = self.nFlocks + self.nFlocksSample
        self.maxN = self.params.maxN


    def calcRelWrapCauchy(self, wrp_rpara):
        """
        (7) Calc the rel cauchy value for all sessions for distributing recruits
        """
        relWrpCauchy = np.zeros(self.nRecruitJulYear)
        for i in self.uYearIndx:
            # day pi in year i
            daypiTmp = self.daypiRecruit[self.yearRecruitIndx == i]
            # pdf of daypi in year i
            dc = dwrpcauchy(daypiTmp, wrp_rpara[0], wrp_rpara[1])
            # mask of recruit window in year i
            reldc = dc / np.sum(dc)     #  dc.sum()
            relWrpCauchy[self.yearRecruitIndx == i] = reldc
#        sessionRelWrpCauchy = adjustRecruitProbs(self.sessionJul, self.nsession, 
#            self.sessionRelWrpC, self.recruitSession, self.nRecruitJulYear,
#            relWrpCauchy)
#        print('sum', np.sum(sessionRelWrpCauchy))
        return(relWrpCauchy)


    def testRWC(self):
        for i in self.uYearIndx:
            mask = self.sessionYearIndx == i
            sumRWC = np.sum(self.sessionRelWrpCauchy[mask])
            print('i', i, 'sum', sumRWC)


    def make_g0_daypiTemplate(self):
        """
        (8) make template of 366 days, then g0 session arrays of len sessions
        """
        self.g0_daypi_OneYear = np.arange(1., 367.) / 366. * 2.0 * np.pi
        # make initial g0_reduceWrpCauchy
        self.g0Obs_reduceWrpCSess = self.makeG0_reduceWrpCauchy(self.g0_ObsWrpC)
        self.g0Trap_reduceWrpCSess = self.makeG0_reduceWrpCauchy(self.g0_TrapWrpC)
        self.pKill_reduceWrpCSess = self.makeG0_reduceWrpCauchy(self.pKill_WrpC)
        # make initial g0 and pkill parameters by session
        self.g0Obs_Session = self.g0Obs_reduceWrpCSess * self.g0_Obs
        self.g0Trap_Session = self.g0Trap_reduceWrpCSess * self.g0_Trap
        self.pKill_Session = self.pKill_reduceWrpCSess * self.pKill_Max
#        print('goobs', self.g0Obs_Session[190:250])
#        print('gotrap', self.g0Trap_Session[190:250])
#        print('pkill', self.pKill_Session[190:250])

    def makeG0_reduceWrpCauchy(self, g0_WrpC_para):
        """
        (9) make self.g0_relWrpCauchy for each session
            get g0_reduceWrpCauchySession for each session next by multiplying g0_obs
        """
        # pdf of daypi
        dc = dwrpcauchy(self.g0_daypi_OneYear, g0_WrpC_para[0], g0_WrpC_para[1])
        reducedc = dc / np.max(dc)     #  dc.sum()
        # make length of nsession
        g0_reduceWrpCauchy = reducedc[self.sessionJulYear]
        return g0_reduceWrpCauchy

    def removeDatFX(self):
        """ 
        (10) get removeDat per session  
        """
        self.removeDat = np.arange(self.nsession)
        self.removeHuntSession = np.arange(self.nsession)
        self.removeTrapSession = np.arange(self.nsession)
        for i in range(self.nsession):
            nHunt = np.sum(self.surveyRemoved[self.surveySession == i])
            self.removeHuntSession[i] = nHunt
            nTrap = np.sum(self.trapRemoved[self.trapSession == i])
            self.removeTrapSession[i] = nTrap
            self.removeDat[i] = nHunt + nTrap
        print('nhunt', np.sum(self.removeHuntSession))
        print('ntrap', np.sum(self.removeTrapSession))
        print('nRemove', np.sum(self.removeDat))


    def getInitialN(self):
        """
        (12) looping to get reasonable inital N values
        """
        self.N = np.zeros(self.nYears, dtype = np.int)
        ## index of recruitment year for sessions
        self.sessionRecruitIndx = np.zeros(self.nsession, dtype = int)
        # Annual number removed
        self.annualRemoved = np.zeros(self.nYears, dtype = int)
        cc = 0          # counter
        i = 0
        ### loop thru the recruitment years to get min N in year
        while cc < self.nsession:
#        for i in range(self.nRecruitJulYear):
            yrIndx = self.yearRecruitIndx[i]
            if self.sessionJul[cc] == self.recruitSession[i]:
                ## add to minimum present birds
                self.annualRemoved[yrIndx] += self.removeDat[cc]
                ## get session index for recruitment year
                self.sessionRecruitIndx[cc] = yrIndx
                ## add to counter
                cc += 1
            i += 1
        self.N[0] = 700
        for i in range(1, self.nYears):
            self.N[i] = (self.N[i-1] - self.annualRemoved[i-1]) * 1.2
        self.totalRecruits = np.round(self.N * .25).astype(int)
        self.nsample = np.array([-3, -2, -1, 1, 2, 3])
        print('initial N', self.N)
        print('recruits', self.totalRecruits)
        print('annualRemoved', self.annualRemoved)

    def NpredAllSessFX(self, nn):
        """
        (13) calc Npred for all sessions
        """
        Npred = np.zeros(self.nsession)
        Npred[0] = self.reproPop[0] + self.sessionRecruits[0]
        for i in self.sessions[:-1]:
            Nday = nn[i] - self.removeDat[i]
            Nday = np.where(Nday < 0, 0, Nday)
            Nday = Nday + self.sessionRecruits[i+1]        # it[i+1]   # imm upto previous
            Npred[i+1] = Nday
        return(Npred)

    def make_llArrays(self):
        """
        (14) make arrays for storing likelihoods
        """
        self.llikTh = np.empty(self.nYears)
        self.llikTh_s = np.empty(self.nYears)
        self.llikR = np.empty(self.nYears - 1)
        self.llikR_s = np.empty(self.nYears - 1)
        self.llikg0Sig = np.empty(self.nYears)
        self.llikg0Sig_s = np.empty(self.nYears)
 

    def NgammaLN(self):
        """
        (14.1) pre-calculate the gammaln of all possible N values (0 - maxN)
        """
        nArray = np.arange(self.params.maxN + 1)    # make array with last value
                                                    # equal to maxN
        self.lnGammaArray = gammaln(nArray + 1)     # get lngamma + 1
                                                    # will indx using Ni
#        print('nArray', nArray[0:10])
#        print('lngamma', self.lnGammaArray[:10])

    def getNSessInYear(self):
        """
        ## count session and recruitSession for each recruit year
        """
        self.nSessionsInYear = np.zeros(self.nYears, dtype = int)
        self.nRecruitSessInYear = np.zeros(self.nYears, dtype = int)
        for i in range(self.nYears):
            sessMask = self.sessionRecruitIndx == i
            self.nSessionsInYear[i] = np.sum(sessMask)
            recSessMask = self.yearRecruitIndx == i
            self.nRecruitSessInYear[i] = np.sum(recSessMask)            

#        print('nsess', self.nSessionsInYear, self.nRecruitSessInYear)
#        print('u sessrec', np.unique(self.sessionRecruitIndx), self.sessionRecruitIndx[:25])
#        print('sess jul', self.sessionJulYear[-30:])
#        print('u yearRec', (self.yearRecruitIndx[:370]))
#        print('recru jul', self.recruitJulYear[:370])
#        cc = 0
#        for i in range(self.nRecruitJulYear):
#            if self.recruitSession[i] == self.sessionJul[cc]:
#                print('i', i, 'recJul', self.recruitSession[i], 'sessJul', 
#                self.sessionJul[cc], 'recIndx', self.yearRecruitIndx[i],
#                'sessIndx', self.sessionRecruitIndx[cc], 'sesJulYr', self.sessionJulYear[cc],
#                'recJulYr', self.recruitJulYear[i])
#                cc += 1
#            else:
#                print('i', i, 'recJul', self.recruitSession[i], 'recIndx', 
#                self.yearRecruitIndx[i],
#                'recJulYr', self.recruitJulYear[i])

class BirdLocData(object):
    def __init__(self, basicdata, params, debug = False):
        """
        object to keep latent loc, pcaptured, whether trapped, which trap
        """
        ###################################################
        ############### RUN BIRDLOCDATA FUNCTIONS
        self.setBirdArrays(basicdata)

        self.populateFlocks(basicdata)                   # get location

        self.loopBirdSessions(params, basicdata)

        
        self.plotRemovePop(basicdata)
        
        
#        self.distribInitialRecruits()


#        alignmentTest(self.birdPObservedID_3D,
#            self.birdObservedID_3D, self.birdTotPObserved_2D, 
#            basicdata.nsession,
#            basicdata.sessionNSurveys, basicdata.maxN, 
#            self.birdPres_2D)



        ##############  END RUNNING FUNCTIONS
        ###################################################

    ##############################################
    #################   DEFINE BIRDLOC FUNCTIONS
    def setBirdArrays(self, basicdata):
        """
        (15) set up bird loc arrays 
        """
        # a seris of arrays maxN * nsession
        # 
#        birdSession = np.repeat(np.array(range(basicdata.nsession), dtype= int), basicdata.maxN)
#        birdID = np.tile(np.arange(0, basicdata.maxN), basicdata.nsession)
        ## number of birds in each flock at start of year
        self.birdPres_2D = np.zeros((basicdata.nYears, basicdata.nFlocks), dtype=int)
        ## n birds in flock in each session
        self.birdSessFlock_2D = np.zeros((basicdata.nsession, basicdata.nFlocks), dtype = int)
        ## n recruits each session by flock
        self.birdRecruitSessFlock_2D = np.zeros((basicdata.nsession, basicdata.nFlocks), dtype = int)
        self.birdX_2D = np.zeros((basicdata.nYears, basicdata.nFlocksTotYear), dtype = int)
        self.birdY_2D = np.zeros((basicdata.nYears, basicdata.nFlocksTotYear), dtype = int)
        self.birdLogX_2D = np.zeros((basicdata.nYears, basicdata.nFlocksTotYear))
        self.birdLogY_2D = np.zeros((basicdata.nYears, basicdata.nFlocksTotYear))
        self.birdNatVeg_2D = np.zeros((basicdata.nYears, basicdata.nFlocksTotYear))
        self.birdSwamp_2D = np.zeros((basicdata.nYears, basicdata.nFlocksTotYear))
        # probability of bird seen by a particular observer j
        self.birdPObservedID_3D = np.zeros((basicdata.nsession, basicdata.nFlocks,
                        basicdata.maxSurvey))            
        # total probability of bird i seen by at least 1 observer
        self.birdTotPObserved_2D = np.zeros((basicdata.nsession, basicdata.nFlocks))
        # indicator of bird i observed by oberver j
        self.birdObservedID_3D = np.zeros((basicdata.nsession, basicdata.nFlocks, 
                        basicdata.maxSurvey), dtype = int)               # changed from maxSurvey
        # Indicator of bird i shot by hunter j
        self.birdShotID_3D = np.zeros((basicdata.nsession, basicdata.nFlocks, 
                        basicdata.maxTrap), dtype = int)               # changed from maxSurvey
        # probability of bird i trapped by trap k
        self.birdPTrappedID_3D = np.zeros((basicdata.nsession, basicdata.nFlocks, 
                        basicdata.maxTrap))               # changed from maxSurvey
        # Total probability of bird i trapped by all traps
        self.birdTotPTrapped_2D = np.zeros((basicdata.nsession, basicdata.nFlocks))
        # Indicator of bird i trapped by trap k
        self.birdTrappedID_3D = np.zeros((basicdata.nsession, basicdata.nFlocks, 
                basicdata.maxTrap), dtype = int)
        # indicator of bird i observed by ANY oberver
        self.birdObserved_2D = np.zeros((basicdata.nsession, basicdata.nFlocks), dtype = int)
        # Indicator of bird i shot by hunter j
        self.birdShot_2D =  np.zeros((basicdata.nsession, basicdata.nFlocks), dtype = int)
        self.birdTrapped_2D = np.zeros((basicdata.nsession, basicdata.nFlocks), dtype = int)

    def loopBirdSessions(self, params, basicdata):
        """
        (16) loop thru sessions to get bird loc, obs, trap, shot data
        """
        ## get PObsID_3D and PTrapID_3D - Numba function
        (self.birdPObservedID_3D, self.birdPTrappedID_3D, self.birdTotPObserved_2D,
            self.birdTotPTrapped_2D) = pObsTrapID_3D(basicdata.sessions,
            basicdata.sessionJulYear, basicdata.sessionRecruitIndx, self.birdPres_2D,
            self.birdRow_2D, self.birdCol_2D, self.birdX_2D, self.birdY_2D,
            basicdata.sessionNTraps, basicdata.sessionNSurveys, basicdata.nFlocks,
            basicdata.surveyX_2D, basicdata.surveyY_2D, basicdata.trapX_2D, basicdata.trapY_2D,
            basicdata.g0Obs_Session, basicdata.g0Trap_Session, self.birdPObservedID_3D, 
            self.birdPTrappedID_3D, self.birdTotPObserved_2D, self.birdTotPTrapped_2D, 
            basicdata.var2)
        # get latent observed, shot and trapped ID pairs - Numba function
        (self.birdObservedID_3D, self.birdShotID_3D, self.birdTrappedID_3D, 
            self.birdObserved_2D, self.birdShot_2D, self.birdTrapped_2D, 
            basicdata.minNPresent,  
            self.birdSessFlock_2D) = getLatentObsShotTrap(
            basicdata.nsession, basicdata.sessions, basicdata.nFlocks,
            basicdata.maxTrap, basicdata.maxSurvey, basicdata.observedData_2D, 
            self.birdObservedID_3D, basicdata.shotData_2D, self.birdShotID_3D, 
            basicdata.trappedData_2D, self.birdTrappedID_3D, self.birdObserved_2D, 
            self.birdShot_2D,
            self.birdTrapped_2D, basicdata.minNPresent, self.birdRecruitSessFlock_2D,
            self.birdSessFlock_2D, self.birdPres_2D,
            basicdata.pKill_Session, basicdata.sessionRecruitIndx,
            self.birdPObservedID_3D, self.birdPTrappedID_3D)

    def populateFlocks(self, basicdata):
        """
        (17) get initial loc and populate birdLoc_2D array
        """        
        self.getInitialLoc(basicdata)
        self.makeInitialHabitatPrediction(basicdata)   
        self.initialBirdDistribution(basicdata)
        (self.birdRecruitSessFlock_2D, self.birdSessFlock_2D) = distribRecruits(basicdata.nFlocks, 
            self.birdPres_2D, self.birdTotRec_2D, self.birdRecruitSessFlock_2D, 
            basicdata.sessionJul, basicdata.nsession, basicdata.recruitSession, 
            basicdata.nRecruitJulYear, basicdata.yearRecruitIndx, 
            basicdata.relWrpCauchy, self.birdSessFlock_2D, 
            basicdata.sessionRecruitIndx)


    def getInitialLoc(self, basicdata):
        """
        get initial flock locations
        """
        self.birdRow_2D = (np.random.choice(range(basicdata.nrows), 
            (basicdata.nFlocksTotYear * basicdata.nYears)).reshape(basicdata.nYears,
            basicdata.nFlocksTotYear))
        self.birdCol_2D = (np.random.choice(range(basicdata.ncols), 
            (basicdata.nFlocksTotYear * basicdata.nYears)).reshape(basicdata.nYears,
            basicdata.nFlocksTotYear))
        for i in range(basicdata.nYears):
            for j in range(basicdata.nFlocksTotYear):
                row_i = self.birdRow_2D[i, j]
                col_j = self.birdCol_2D[i, j]
                while (basicdata.extMask[row_i, col_j] != 1):
                    row_i = np.random.choice(basicdata.nrows)
                    col_j = np.random.choice(basicdata.ncols)
                self.birdRow_2D[i, j] = row_i
                self.birdCol_2D[i, j] = col_j
                xx = ((basicdata.extent_xmin + basicdata.halfRes) + 
                    (basicdata.resol * col_j))
                self.birdX_2D[i, j] = xx
                yy =  ((basicdata.extent_ymax - basicdata.halfRes) -
                     (basicdata.resol * row_i)) 
                self.birdY_2D[i, j] = yy
                self.birdLogX_2D[i, j] = np.log(xx - basicdata.extent_xmin + 1)
                self.birdLogY_2D[i, j] = np.log(yy - basicdata.extent_ymin + 1)
                self.birdNatVeg_2D[i, j] = basicdata.logNatVeg_2D[row_i, col_j]
                self.birdSwamp_2D[i, j] = basicdata.logSwamp_2D[row_i, col_j]
#        print('x', self.birdX_2D[:, :4])
#        print('log x', np.min(self.birdLogX_2D))
#        print('xmin', basicdata.extent_xmin, basicdata.extent_xmax)
#        print('row', self.birdRow_2D[:, :4])
#        print('col', self.birdCol_2D[:, :4])


    def makeInitialHabitatPrediction(self, basicdata):
        """
        (16.1) make relative habitat values for all flock locations
        """
        # Covariates for habitat model (order: x, y, swamp, nat veg)
        self.habCov = np.zeros((basicdata.nbcov, basicdata.nYears, basicdata.nFlocksTotYear))
        self.habCov[0] = self.birdLogY_2D 
        self.habCov[1] = self.birdSwamp_2D
        self.habCov[2] = self.birdNatVeg_2D
        self.bird_lth = basicdata.b[0] * self.habCov[0]
        for i in range(1, basicdata.nbcov):
            self.bird_lth += basicdata.b[i] * self.habCov[i]
        self.expTotHab = np.exp(self.bird_lth)
        self.sumExpTotHab = np.expand_dims(np.sum(self.expTotHab, axis = 1), 1)
        self.LThMulti_2D = (self.expTotHab / self.sumExpTotHab)

        
    def initialBirdDistribution(self, basicdata):
        """
        ## initial distribution of birds across flocks
        """
        ## loop thru years
        for i in range(basicdata.nYears):
            lth_flocks = self.LThMulti_2D[i, :basicdata.nFlocks].copy()
            lth_flocks = lth_flocks / np.sum(lth_flocks)
            self.birdPres_2D[i] = np.random.multinomial(basicdata.N[i], lth_flocks, size= 1)
#            print('n', self.birdPres_2D[i])
        self.birdTotRec_2D = np.round(self.birdPres_2D * basicdata.rg).astype(int)

        
    def plotRemovePop(self, basicdata):
        """
        Make plot of population and number removed by session
        """
        sessionPopSize = np.sum(self.birdSessFlock_2D, axis = 1)
        ## get plotting dates
        datesPlot = np.empty(8, dtype = datetime.date)
        yr_i = 2006
        for i in range(8):
            datesPlot[i] = datetime.date(yr_i, 1, 1)
            yr_i += 1
        P.figure(figsize=(14, 11))
        ax = P.gca()
        lns1 = ax.plot(basicdata.sessionDates, sessionPopSize, label = 'Population size', 
            color = 'k', linewidth = 5)
        ax2 = ax.twinx()
        lns2 = ax2.plot(basicdata.sessionDates, basicdata.removeDat.astype(int), 
            'ro', label = 'Number removed', ms = 10)
        lns = lns1 + lns2
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        minDate = basicdata.sessionDates[0] - datetime.timedelta(days=10)
        maxDate = basicdata.sessionDates[-1] + datetime.timedelta(days=10)
        ax.set_xlim(minDate, maxDate)
        ax2.set_ylim(0, 20)
        ax.set_xlabel('Dates', fontsize = 17)
        ax.set_ylabel('Population size', fontsize = 17)
        ax2.set_ylabel('Number removed', fontsize = 17)
        P.xticks(datesPlot, fontsize = 12)
        ax2.set_yticks(np.arange(0, 19, 3, dtype = int))
        figFname = 'removePopSize.png'
        P.savefig(figFname, format='png')
        P.show()
        
        
        
#############################
#############################
#############################
#############################

class MCMC(object):
    def __init__(self, params, birdlocdata, basicdata):
        """
        Class to run mcmc
        """
        ###############################
        #### RUN MCMC FUNCTIONS
        self.migrateData(basicdata, params, birdlocdata)
        self.makeProposalArrays()

        # run mcmcFX - gibbs loop
#        self.mcmcFX()


        #### END RUNNING MCMC FUNCTIONS
        ###############################


    ###################################
    ####    DEFINE MCMC FUNCTIONS
    def migrateData(self, basicdata, params, birdlocdata):
        """
        (18) Migrate data and create storage arrays
        """
        self.basicdata = basicdata
        self.params = params
        self.birdlocdata = birdlocdata   
        # storage arrays for parameters
        self.bgibbs = np.zeros([self.params.ngibbs, self.params.nbcov]) # beta parameters
        self.bPrevgibbs = np.zeros(self.params.ngibbs)                  # prev cell count habitat
        self.rgibbs = np.zeros(self.params.ngibbs)                      # pop growth
        self.rparagibbs = np.zeros([self.params.ngibbs, 2])             # wrapped cauchy para for recruitment
#        self.igibbs = np.zeros(self.params.ngibbs)                      # immigration
        self.siggibbs = np.zeros(self.params.ngibbs)                    # home range decay
        self.g0Obsgibbs = np.zeros(self.params.ngibbs)              # g0 obs
        self.g0ObsWrpCgibbs = np.zeros([self.params.ngibbs, 2])     # wrpC observation
        self.g0Trapgibbs = np.zeros(self.params.ngibbs)             # g0 trap
        self.g0TrapWrpCgibbs = np.zeros([self.params.ngibbs, 2])    # wrpC trap
        self.pKillgibbs = np.zeros(self.params.ngibbs)              # prob kill
        self.g0KillWrpCgibbs = np.zeros([self.params.ngibbs, 2])    # wrpC Kill
#        self.Ngibbs = np.zeros((self.params.ngibbs, self.basicdata.nYears), 
#            dtype = int)  # pop size
#        self.sumN = np.zeros(self.basicdata.nsession, dtype = int)
#        self.reproPopgibbs = np.zeros(self.params.ngibbs)

    @staticmethod
    def getBetaFX(ms, std):
        a = mn * ((mn * (1.0 - mn)) / std**2.0 - 1.0)
        b = (1.0 - mn) * ((mn * (1.0 - mn)) / std**2.0 - 1.0)
        return np.random.beta(a, b, size = None)

    def makeProposalArrays(self):
        """
        make some arrays for mcmc class
        """
        self.totalGibbs = self.params.ngibbs * self.params.thinrate + self.params.burnin
        self.pKill_WrpC_s = np.zeros(2)
        self.g0_TrapWrpC_s = np.zeros(2)
        self.g0_ObsWrpC_s = np.zeros(2)
        self.PObsID_3D_s = np.zeros(self.basicdata.maxSurvey)
        self.PTrapID_3D_s = np.zeros(self.basicdata.maxTrap)
        self.rpara_s = np.zeros(2)
#        self.popIndx = np.array([0, 246, 602, 890, 1010, (self.basicdata.nsession - 1)])
    ########            Main mcmc function
    ########
    def mcmcFX(self):
        """
        (19) mcmc function that calls updater functions
        #   and populates storage arrays
        """
#        print('maxN', np.max(self.basicdata.N))
        cc = 0
        for g in range(self.totalGibbs):
            # update N and latent variables by session
            self.N_UpdateWrapper()

#            alignmentTest(self.birdlocdata.birdPObservedID_3D,
#                self.birdlocdata.birdObservedID_3D, self.birdlocdata.birdTotPObserved_2D, 
#                self.basicdata.nsession,
#                self.basicdata.sessionNSurveys, self.basicdata.maxN, 
#                self.birdlocdata.birdPres_2D)


            # update bird locations
            self.Loc_Wrapper()

            # update observer - seen-bird pairs
            (self.birdlocdata.birdObservedID_3D, 
                self.birdlocdata.birdObserved_2D) = updateObserverBird(self.basicdata.nsession, 
                self.birdlocdata.birdPres_2D, self.birdlocdata.birdPObservedID_3D, 
                self.birdlocdata.birdObserved_2D, self.birdlocdata.birdObservedID_3D, 
                self.basicdata.sessionNSurveys, self.basicdata.maxN, 
                self.birdlocdata.birdShotID_3D)
            self.updateMinNPresent()                    # UPDATE minNPresent
            ## update pkill and relwrpCauchy parameters
            self.updateKillParameters()
            ## update g0 trap parameters
            self.updateG0TrapParameters()
            ## update g0 observed parameters
            self.updateG0Observed()
            ### UPDATA SIGMA PARAMETER
            self.updateSigma()
            ###     UPDATE POPULATION GROWTH
            self.rUpdateFX()
            ###     UPDATE IMMIGRATION PARAMETER
#            self.immUpdateFX()
            ###     UPDATE RECRUITMENT REL WRAP CAUCHY RPARA
            self.rparaUpdateFX()

            ## Populate storage arrays
            if g in self.params.keepseq:
                self.bgibbs[cc] = self.basicdata.b.flatten()
                self.bPrevgibbs[cc] = self.basicdata.bPrev
                self.rgibbs[cc] = self.basicdata.rg
                self.rparagibbs[cc] = self.basicdata.rpara
#                self.igibbs[cc] = self.basicdata.ig
                self.siggibbs[cc] = self.basicdata.sigma
                self.g0Obsgibbs[cc] = self.basicdata.g0_Obs
                self.g0ObsWrpCgibbs[cc] = self.basicdata.g0_ObsWrpC
                self.g0Trapgibbs[cc] = self.basicdata.g0_Trap
                self.g0TrapWrpCgibbs[cc] = self.basicdata.g0_TrapWrpC
                self.pKillgibbs[cc] = self.basicdata.pKill_Max
                self.g0KillWrpCgibbs[cc] = self.basicdata.pKill_WrpC
                self.Ngibbs[cc] = self.basicdata.N[self.popIndx]
                self.sumN = self.sumN + self.basicdata.N
                self.reproPopgibbs[cc] = self.basicdata.reproPop[-1]
                cc = cc + 1
#                print('finished g', g)

#        print('N', self.Ngibbs[:, :5])
#        print('b', self.bgibbs)
#        print('bPrev', self.bPrevgibbs)
#        print('pkillmax', self.pKillgibbs)
#        print('killWrpC', self.g0KillWrpCgibbs)
#        print('g0Trap', self.g0Trapgibbs)
#        print('self.g0TrapWrpCgibbs', self.g0TrapWrpCgibbs)
#        print('self.g0Obsgibbs', self.g0Obsgibbs)
#        print('self.g0ObsWrpCgibbs', self.g0ObsWrpCgibbs)
#        print('sigma', self.siggibbs)
#        print('r pop growth', self.rgibbs)
#        print('immigration', self.igibbs)
#        print('rpara', self.rparagibbs)

#        alignmentTest(self.birdlocdata.birdPObservedID_3D,
#            self.birdlocdata.birdObservedID_3D, self.birdlocdata.birdTotPObserved_2D, 
#           self.basicdata.nsession,
#           self.basicdata.sessionNSurveys, self.basicdata.maxN, 
#           self.birdlocdata.birdPres_2D)
        ####
        ###################### END MAIN MCMC FUNCTION

    ###########################     UPDATE N AND LATENT VARIABLES
    ###########################
    def N_UpdateWrapper(self):
        """
        (19) wrapper function to update latent variables by session
        """
        # reset self.LThMulti_2D to zero and populate in Numba update N
#        self.birdlocdata.prevCellCount_2D = self.birdlocdata.prevCellCount_2D * 0.0
        ####### Update N
        # get proposed Ns and birddata_s and llik
        (self.basicdata.sessionRecruits, self.basicdata.totalRecruits,  
            self.birdlocdata.birdPObservedID_3D, self.birdlocdata.birdPTrappedID_3D,
            self.birdlocdata.birdTotPObserved_2D, self.birdlocdata.birdTotPTrapped_2D, 
            self.birdlocdata.birdLoc_2D, self.birdlocdata.birdPres_2D,
            self.birdlocdata.birdRow_2D, self.birdlocdata.birdCol_2D, 
            self.birdlocdata.birdX_2D, 
            self.birdlocdata.birdY_2D) = proposeBirdPres(self.basicdata.sessions, 
            self.birdlocdata.birdTotPObserved_2D, 
            self.birdlocdata.birdPObservedID_3D, self.birdlocdata.birdTotPTrapped_2D, 
            self.birdlocdata.birdPTrappedID_3D, self.basicdata.surveyX_2D, 
            self.basicdata.surveyY_2D, self.basicdata.trapX_2D, 
            self.basicdata.trapY_2D, self.basicdata.observedData_2D, 
            self.basicdata.trappedData_2D, self.basicdata.N, self.basicdata.nsample,
            self.birdlocdata.birdPres_2D, self.basicdata.sessionNSurveys, 
            self.basicdata.sessionNTraps, 
            self.basicdata.g0Obs_Session, self.basicdata.g0Trap_Session, 
            self.basicdata.var2, self.basicdata.nCells,
            self.birdlocdata.birdObserved_2D, self.birdlocdata.birdTrapped_2D, 
            self.birdlocdata.birdLoc_2D, self.basicdata.lnGammaArray, 
            self.basicdata.minNPresent, self.params.maxN,  
            self.basicdata.yearRecruitIndx, self.basicdata.uYearIndx, 
            self.basicdata.relWrpCauchy, self.basicdata.sessionRecruits, 
            self.basicdata.sessionRecruits_s, self.basicdata.totalRecruits, 
            self.basicdata.reproPop, self.basicdata.ig, self.basicdata.rg, 
            self.basicdata.nsession, self.basicdata.removeDat, 
            self.PObsID_3D_s, self.PTrapID_3D_s, self.basicdata.sessionJul,
            self.basicdata.maxSessionJul,self.birdlocdata.birdRow_2D, 
            self.birdlocdata.birdCol_2D, self.birdlocdata.birdX_2D, 
            self.birdlocdata.birdY_2D, self.basicdata.nSessionsInYear, self.basicdata.nRecruitSessInYear)


    #######################     UPDATE LOCATION AND HABITAT PARAMETERS
    #######################
    def Loc_Wrapper(self):
        """
        (30) wrapper function to update bird locations by session
        """
        (LThMulti_2D_s, sumExpTotHab_s) = self.habitatPropose()                   # propose new betas and hab values
        # update location and get LL for habitat beta and beta_s
        (self.birdlocdata.birdLoc_2D, self.birdlocdata.birdTotPTrapped_2D, 
                self.birdlocdata.birdTotPObserved_2D, 
                self.birdlocdata.prevCellCount_2D, self.birdlocdata.birdPObservedID_3D, 
                self.birdlocdata.birdPTrappedID_3D, 
                self.birdlocdata.LThMulti_2D, LThMulti_2D_s, self.logLikHab,
                self.logLikHab_s) = locUpdateNumba(self.basicdata.N, 
                self.birdlocdata.birdPObservedID_3D, 
                self.birdlocdata.birdPTrappedID_3D, self.birdlocdata.birdTotPObserved_2D, 
                self.birdlocdata.birdTotPTrapped_2D, self.birdlocdata.birdLoc_2D, 
                self.birdlocdata.birdPres_2D, self.birdlocdata.prevCellCount_2D, 
                self.birdlocdata.LThMulti_2D, self.birdlocdata.birdObserved_2D, 
                self.birdlocdata.birdTrapped_2D,  self.basicdata.maxN, 
                self.basicdata.nCells, self.basicdata.sessionNSurveys, 
                self.basicdata.sessionNTraps, self.basicdata.g0Obs_Session,
                self.basicdata.g0Trap_Session, self.basicdata.cellX, 
                self.basicdata.cellY, self.basicdata.surveyX_2D, 
                self.basicdata.surveyY_2D, self.basicdata.trapX_2D, 
                self.basicdata.trapY_2D, self.basicdata.var2,
                self.birdlocdata.birdObservedID_3D, self.birdlocdata.birdTrappedID_3D,
                self.basicdata.nsession, self.basicdata.bPrev, self.bPrev_s, 
                self.birdlocdata.bird_lth, self.bird_lth_s, LThMulti_2D_s, 
                self.birdlocdata.sumExpTotHab, sumExpTotHab_s,
                self.PObsID_3D_s, self.PTrapID_3D_s)
        self.habitatPnowPnew(LThMulti_2D_s, sumExpTotHab_s)           # update habitat parameters and values

    # not presently used. could go into adding individuals, not changing loc
    def getCellProposal(self):
        """
        use multinomial draw to get locations close to preferred locations
        """
        cell_s = np.zeros(self.basicdata.nCells, dtype = int)
        for i in range(nsession):
            randMultiNomPr = np.exp(np.random.normal(np.log(self.birdlocdata.LThMulti_2D[i]),
                    0.1, size =  self.basicdata.nCells))
            randMultiNomPr = randMultiNomPr / np.sum(randMultiNomPr)
            cell_s[i] =  np.random.multinomial(1, randMultiNomPr, size = 1).flatten()
        return(cell_s)            


    def habitatPropose(self):
        """
        (31) update relative habitat values given the new previous presence data
        ##  count in column i is count from previous session !!
        """
        self.bs = np.random.normal(self.basicdata.b, 0.1)
        self.bPrev_s = np.random.normal(self.basicdata.bPrev, 0.1)
        self.bird_lth_s = np.dot(self.basicdata.xdat, self.bs)
        LThMulti_2D_s = (self.bird_lth_s + 
                (self.birdlocdata.prevCellCount_2D * self.bPrev_s))
        expTotHab_s = np.exp(LThMulti_2D_s)
        sumExpTotHab_s = np.sum(expTotHab_s, axis = 0)
        LThMulti_2D_s = expTotHab_s / sumExpTotHab_s
        return(LThMulti_2D_s, sumExpTotHab_s)

    def habitatPnowPnew(self, LThMulti_2D_s, sumExpTotHab_s):
        """
        (31.2) calc importance ratio and updata beta parameters and habitat values
        """
        # present parameter prios
        betaPriors = np.sum(stats.norm.logpdf(self.basicdata.b, self.params.bPrior,
            self.params.bPriorSD)) + stats.norm.logpdf(self.basicdata.bPrev, 
            self.params.bPrevPrior[0], self.params.bPrevPrior[1])
        # proposed priors
        betaPriors_s = np.sum(stats.norm.logpdf(self.bs, self.params.bPrior,
            self.params.bPriorSD)) + stats.norm.logpdf(self.bPrev_s, 
            self.params.bPrevPrior[0], self.params.bPrevPrior[1])
        pnow = self.logLikHab + betaPriors
        pnew = self.logLikHab_s + betaPriors_s
        # Calc IR
        rValue = np.exp(pnew - pnow)
        zValue = np.random.random()
        if rValue > zValue:                                     # update parameters
            self.basicdata.b = self.bs.copy()
            self.basicdata.bPrev = self.bPrev_s
            self.basicdata.LThMulti_2D = LThMulti_2D_s.copy()
            self.basicdata.bird_lth = self.bird_lth_s.copy()
            self.basicdata.sumExpTotHab = sumExpTotHab_s.copy()
        del(sumExpTotHab_s, LThMulti_2D_s)

    def updateMinNPresent(self):
        """
        (32.9) UPDATE THE minNPresent
        """
        nObsSession = np.sum(self.birdlocdata.birdObserved_2D, axis = 1)
        self.basicdata.minNPresent = nObsSession + self.basicdata.sumTrapRem

    ###############################     UPDATE KILL/SHOT PARAMETERS
    ###############################
    def updateKillParameters(self):
        """
        (33) update max pKill and associated relWrpCauchy parameters
        """
        # propose new para values
        self.pKill_WrpC_s[0] = np.random.normal(self.basicdata.pKill_WrpC[0], 
            self.params.pKill_WrpCSearch, size = None)
        self.pKill_WrpC_s[1] = np.exp(np.random.normal(np.log(self.basicdata.pKill_WrpC[1]), 
            self.params.pKill_WrpCSearch, size = None))
        logitpKill_Max_s = logit(self.basicdata.pKill_Max)
        pKill_Max_s = inv_logit(np.random.normal(logitpKill_Max_s, self.params.pKill_Search)) 
        pKill_reduceWrpCSess_s = self.basicdata.makeG0_reduceWrpCauchy(self.pKill_WrpC_s)
        pKill_Session_s = pKill_reduceWrpCSess_s * pKill_Max_s
#        if pKill_Session_s < 1.0e-200:
#            pKill_Session = 1.0e-200
#        if pKill_Session < 1.0e-200:
#            pKill_Session = 1.0e-200 
        # get Log lik of kill data
        (sumLLKill, sumLLKill_s) = pkill_LogLik(self.basicdata.pKill_Session, pKill_Session_s, 
            self.birdlocdata.birdObservedID_3D, self.birdlocdata.birdShotID_3D,
            self.basicdata.nsession, self.basicdata.sessionNSurveys, 
            self.basicdata.maxN, self.birdlocdata.birdPres_2D)
        presentPriors = (stats.beta.logpdf(self.basicdata.pKill_Max, 
            self.params.pKill_MaxPrior[0],  self.params.pKill_MaxPrior[1]) + 
            stats.norm.logpdf(self.basicdata.pKill_WrpC[0], self.params.mu_Kill_Wrpc_Prior[0],
            self.params.mu_Kill_Wrpc_Prior[1]) + np.log(gamma_pdf(self.basicdata.pKill_WrpC[1],
            self.params.rho_Kill_Wrpc_Prior[0], self.params.rho_Kill_Wrpc_Prior[1])))
        proposedPriors = (stats.beta.logpdf(pKill_Max_s,
            self.params.pKill_MaxPrior[0],  self.params.pKill_MaxPrior[1]) +
            stats.norm.logpdf(self.pKill_WrpC_s[0], self.params.mu_Kill_Wrpc_Prior[0],
            self.params.mu_Kill_Wrpc_Prior[1]) + np.log(gamma_pdf(self.pKill_WrpC_s[1],
            self.params.rho_Kill_Wrpc_Prior[0], self.params.rho_Kill_Wrpc_Prior[1])))
#        print('sumLLkill', sumLLKill, sumLLKill_s)
        pnow = sumLLKill + presentPriors
        pnew = sumLLKill_s + proposedPriors
        # Calc IR
        rValue = np.exp(pnew - pnow)
        zValue = np.random.random()
        if rValue > zValue:                                     # update parameters
            self.basicdata.pKill_Max = pKill_Max_s
            self.basicdata.pKill_WrpC = self.pKill_WrpC_s.copy()
            self.basicdata.pKill_Session = pKill_Session_s.copy()

    ####################################    UPDATE G0 TRAPPED
    ####################################
    def updateG0TrapParameters(self):
        """
        (34) update g0-trap and associated relWrpCauchy parameters
        """
        # propose new para values
        birdPTrappedID_3D_s = np.zeros_like(self.birdlocdata.birdPTrappedID_3D) 
        birdTotPTrapped_2D_s = np.zeros_like(self.birdlocdata.birdTotPTrapped_2D)
        self.g0_TrapWrpC_s[0] = np.random.normal(self.basicdata.g0_TrapWrpC[0],
            self.params.g0_TrapWrpCSearch, size = None)
        self.g0_TrapWrpC_s[1] = np.exp(np.random.normal(np.log(self.basicdata.g0_TrapWrpC[1]),
            self.params.g0_TrapWrpCSearch, size = None))
        logitg0_Trap_s = logit(self.basicdata.g0_Trap)
        g0_Trap_s = inv_logit(np.random.normal(logitg0_Trap_s, self.params.g0_TrapSearch))
        g0_Trap_reduceWrpCSess_s = self.basicdata.makeG0_reduceWrpCauchy(self.g0_TrapWrpC_s)
        g0Trap_Session_s = g0_Trap_reduceWrpCSess_s * g0_Trap_s
        # get Log lik of trap data
        (sumLLg0, sumLLg0_s, birdPTrappedID_3D_s, 
            birdTotPTrapped_2D_s) = g0_LogLik(self.basicdata.g0Trap_Session, 
            g0Trap_Session_s, self.birdlocdata.birdPTrappedID_3D, 
            self.birdlocdata.birdTrappedID_3D, self.birdlocdata.birdTrapped_2D,
            self.birdlocdata.birdTotPTrapped_2D, birdPTrappedID_3D_s, 
            birdTotPTrapped_2D_s, self.basicdata.nsession, 
            self.basicdata.sessionNTraps, self.basicdata.maxN, self.birdlocdata.birdPres_2D)
        #### Present Priors
        presentPriors = (stats.beta.logpdf(self.basicdata.g0_Trap,
            self.params.g0_TrapPrior[0],  self.params.g0_TrapPrior[1]) +
            stats.norm.logpdf(self.basicdata.g0_TrapWrpC[0], self.params.mu_Trap_Wrpc_Prior[0],
            self.params.mu_Trap_Wrpc_Prior[1]) + 
            np.log(gamma_pdf(self.basicdata.g0_TrapWrpC[1],
            self.params.rho_Trap_Wrpc_Prior[0], self.params.rho_Trap_Wrpc_Prior[1])))
        #### Proposed Priors
        proposedPriors = (stats.beta.logpdf(g0_Trap_s,
            self.params.g0_TrapPrior[0],  self.params.g0_TrapPrior[1]) +
            stats.norm.logpdf(self.g0_TrapWrpC_s[0], self.params.mu_Trap_Wrpc_Prior[0],
            self.params.mu_Trap_Wrpc_Prior[1]) + 
            np.log(gamma_pdf(self.g0_TrapWrpC_s[1],
            self.params.rho_Trap_Wrpc_Prior[0], self.params.rho_Trap_Wrpc_Prior[1])))
#        print('sumllg0', sumLLg0, 'summLLg0_s', sumLLg0_s)
        pnow = sumLLg0 + presentPriors
        pnew = sumLLg0_s + proposedPriors
        # Calc IR
        pdiff = pnew - pnow
        if pdiff > 5.0:
            rValue = .99
            zValue = .01
        else:
            rValue = np.exp(pnew - pnow)
            zValue = np.random.random()
        if rValue > zValue:                                     # update parameters
            self.basicdata.g0_Trap = g0_Trap_s
            self.basicdata.g0_TrapWrpC = self.g0_TrapWrpC_s.copy()
            self.basicdata.g0Trap_Session = g0Trap_Session_s.copy()
            self.birdlocdata.birdPTrappedID_3D = birdPTrappedID_3D_s.copy()
            self.birdlocdata.birdTotPTrapped_2D = birdTotPTrapped_2D_s.copy()
        del(birdPTrappedID_3D_s, birdTotPTrapped_2D_s)


    #############################       UPDATE G0 OBSERVED
    #############################
    def updateG0Observed(self):
        """
        (34) update g0-Observed and associated relWrpCauchy parameters
        """
        # propose new para values
        birdPObservedID_3D_s = np.zeros_like(self.birdlocdata.birdPObservedID_3D) 
        birdTotPObserved_2D_s = np.zeros_like(self.birdlocdata.birdTotPObserved_2D)
        self.g0_ObsWrpC_s[0] = np.random.normal(self.basicdata.g0_ObsWrpC[0],
            self.params.g0_ObsWrpC_Search, size = None)
        self.g0_ObsWrpC_s[1] = np.exp(np.random.normal(np.log(self.basicdata.g0_ObsWrpC[1]),
            self.params.g0_ObsWrpC_Search, size = None))
        logitg0_Obs_s = logit(self.basicdata.g0_Obs)
        g0_Obs_s = inv_logit(np.random.normal(logitg0_Obs_s, self.params.g0_ObsSearch))
        g0Obs_reduceWrpCSess_s = self.basicdata.makeG0_reduceWrpCauchy(self.g0_ObsWrpC_s)
        g0Obs_Session_s = g0Obs_reduceWrpCSess_s * g0_Obs_s

        # get Log lik of Obs data
        (sumLLg0, sumLLg0_s, birdPObservedID_3D_s, 
            birdTotPObserved_2D_s) = obs_g0_LogLik(self.basicdata.g0Obs_Session, 
            g0Obs_Session_s, self.birdlocdata.birdPObservedID_3D, 
            self.birdlocdata.birdObservedID_3D, 
            self.birdlocdata.birdTotPObserved_2D, birdPObservedID_3D_s, 
            birdTotPObserved_2D_s, self.basicdata.nsession, 
            self.basicdata.sessionNSurveys, self.basicdata.maxN, self.birdlocdata.birdPres_2D)
        #### Present Priors
        presentPriors = (stats.beta.logpdf(self.basicdata.g0_Obs,
            self.params.g0_ObsPrior[0],  self.params.g0_ObsPrior[1]) +
            stats.norm.logpdf(self.basicdata.g0_ObsWrpC[0], self.params.mu_Obs_Wrpc_Prior[0],
            self.params.mu_Obs_Wrpc_Prior[1]) +
            np.log(gamma_pdf(self.basicdata.g0_ObsWrpC[1],
            self.params.rho_Obs_Wrpc_Prior[0], self.params.rho_Obs_Wrpc_Prior[1])))
        #### Proposed Priors
        proposedPriors = (stats.beta.logpdf(g0_Obs_s,
            self.params.g0_ObsPrior[0],  self.params.g0_ObsPrior[1]) +
            stats.norm.logpdf(self.g0_ObsWrpC_s[0], self.params.mu_Obs_Wrpc_Prior[0],
            self.params.mu_Obs_Wrpc_Prior[1]) +
            np.log(gamma_pdf(self.g0_ObsWrpC_s[1],
            self.params.rho_Obs_Wrpc_Prior[0], self.params.rho_Obs_Wrpc_Prior[1])))
        pnow = sumLLg0 + presentPriors
        pnew = sumLLg0_s + proposedPriors

        # Calc IR
        rValue = np.exp(pnew - pnow)
        zValue = np.random.random()
        if rValue > zValue:                                     # update parameters
            self.basicdata.g0_Obs = g0_Obs_s
            self.basicdata.g0_ObsWrpC = self.g0_ObsWrpC_s.copy()
            self.basicdata.g0Obs_Session = g0Obs_Session_s.copy()
            self.birdlocdata.birdPObservedID_3D = birdPObservedID_3D_s.copy()
            self.birdlocdata.birdTotPObserved_2D = birdTotPObserved_2D_s.copy()
        del(birdPObservedID_3D_s, birdTotPObserved_2D_s)


    ########################        UPDATE SIGMA
    ########################   
    def updateSigma(self):
        """
        (35) update home-range sigma parameter
        """
        # propose new para values
        birdPTrappedID_3D_s = np.zeros_like(self.birdlocdata.birdPTrappedID_3D) 
        birdTotPTrapped_2D_s = np.zeros_like(self.birdlocdata.birdTotPTrapped_2D)
        birdPObservedID_3D_s = np.zeros_like(self.birdlocdata.birdPObservedID_3D)
        birdTotPObserved_2D_s = np.zeros_like(self.birdlocdata.birdTotPObserved_2D)
        # propose new values for next iteration
        sigma_s = np.random.normal(self.basicdata.sigma, self.params.sigma_Search, size = None)
        var2_s = 2.0 * (sigma_s**2.0)
        #########   GET LL OF TRAP AND OBS DATA GIVEN PROPOSED SIGMA
        (totLLSigma, totLLSigma_s, birdPTrappedID_3D_s, birdTotPTrapped_2D_s, 
            birdPObservedID_3D_s, 
            birdTotPObserved_2D_s) = sigma_LogLik(self.basicdata.g0Trap_Session, 
            self.basicdata.g0Obs_Session, self.basicdata.var2, var2_s,
            self.birdlocdata.birdPTrappedID_3D, self.birdlocdata.birdTrappedID_3D, 
            self.birdlocdata.birdTrapped_2D, self.birdlocdata.birdTotPTrapped_2D, 
            birdPTrappedID_3D_s, birdTotPTrapped_2D_s,
            self.birdlocdata.birdPObservedID_3D, self.birdlocdata.birdObservedID_3D, 
            self.birdlocdata.birdTotPObserved_2D, birdPObservedID_3D_s, birdTotPObserved_2D_s, 
            self.basicdata.nsession, self.basicdata.sessionNTraps, 
            self.basicdata.sessionNSurveys, self.basicdata.maxN, self.birdlocdata.birdPres_2D)
        # PRESENT PRIORS
        sigPrior = stats.norm.logpdf(self.basicdata.sigma, self.params.sigma_Prior[0], 
                    self.params.sigma_Prior[1])
        sigPrior_s = stats.norm.logpdf(sigma_s, self.params.sigma_Prior[0], 
                    self.params.sigma_Prior[1])
        pnow = totLLSigma + sigPrior
        pnew = totLLSigma_s + sigPrior_s
        # Calc IR
        rvDiff = pnew - pnow
        if rvDiff > 2.0:
            rValue = 1.0
            zValue = 0.0
        elif rvDiff < 10.0:
            rValue = 0.01
            zValue = 0.99
        else:
            rValue = np.exp(pnew - pnow)
            zValue = np.random.random()

        if rValue > zValue:                                     # update parameters
            self.basicdata.sigma = sigma_s
            self.birdlocdata.birdPObservedID_3D = birdPObservedID_3D_s.copy()
            self.birdlocdata.birdTotPObserved_2D = birdTotPObserved_2D_s.copy()
            self.birdlocdata.birdPTrappedID_3D = birdPTrappedID_3D_s.copy()
            self.birdlocdata.birdTotPTrapped_2D = birdTotPTrapped_2D_s.copy()
        del(birdPTrappedID_3D_s, birdTotPTrapped_2D_s, birdPObservedID_3D_s,
                 birdTotPObserved_2D_s)


    #############################
    #############################       POP GROWTH PARAMETER UPDATE
    def rUpdateFX(self):
        """
        update rg in mcmc function
        """
        self.rs = np.exp(np.random.normal(np.log(self.basicdata.rg), 
                self.params.r_Search, size = None))
        if self.rs >= 3.0:
            self.rs = self.basicdata.rg - .01
        # get poisson likelihood
        self.rLLikFX()
        pnow = self.llikR + np.log(gamma_pdf(self.basicdata.rg,
            self.params.r_Prior[0], self.params.r_Prior[1]))
        pnew = self.llikR_s + np.log(gamma_pdf(self.rs,
            self.params.r_Prior[0], self.params.r_Prior[1]))
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.rg = self.rs
            self.basicdata.Npred = self.basicdata.Npred_s.copy()
            self.basicdata.sessionRecruits = self.basicdata.sessionRecruits_s.copy()
            self.basicdata.totalRecruits = self.basicdata.totalRecruits_s.copy()

    def rLLikFX(self):
        """
        get llik for population growth parameter and proposed
        """
        # get recruits for each session with proposed rs
        self.reproDataAllYears()
        # get Npred_s
        self.basicdata.Npred_s = self.NpredAllMCMC()
        self.llikR = np.sum(np.log(stats.poisson.pmf(self.basicdata.N[1:], self.basicdata.Npred[1:])))
        self.llikR_s = np.sum(np.log(stats.poisson.pmf(self.basicdata.N[1:], self.basicdata.Npred_s[1:])))

    def reproDataAllYears(self):
        """
        get number of recruits across years and session
        with specified growth parameter
        """
        self.basicdata.totalRecruits_s = (self.basicdata.reproPop * self.rs)

        self.basicdata.sessionRecruits_s = getSessionRecruits(self.basicdata.reproPop, 
            self.basicdata.totalRecruits, 
            self.basicdata.yearRecruitIndx, self.basicdata.relWrpCauchy, 
            self.basicdata.sessionRecruits_s, self.basicdata.sessionJul, self.basicdata.maxSessionJul)


    def NpredAllMCMC(self):
        """
        calc Npred for all sessions
        """
        Npred = np.zeros(self.basicdata.nsession)
        Npred[0] = self.basicdata.N[0] + self.basicdata.sessionRecruits_s[0]
        Npred[1:] = self.basicdata.N[:-1] - self.basicdata.removeDat[:-1]
        Npred[1:] = Npred[1:] + self.basicdata.sessionRecruits_s[1 : self.basicdata.nsession]
        Npred[Npred < 0] = .5
        return (Npred)
    #############################
    #############################    END POP GROWTH PARAMETER UPDATE


    #############################    IMMIGRATION UPDATE
    #############################
    def immLLikFX(self):
        """
        get llik for ig and is
        i in [0, nsession - 1]
        """
        self.i_s = np.exp(np.random.normal(np.log(self.basicdata.ig), 
                self.params.imm_Search, size = None))
        # get proposed total number of recruits per year, and sesion recruits
        self.RecruitsAllYears()
        self.basicdata.Npred_s = self.NpredAllMCMC()
#        print('n', self.basicdata.N)
#        print('npred', self.basicdata.Npred_s)
#        print('sessrecru', self.basicdata.sessionRecruits_s)
#        print('totrecru', self.basicdata.totalRecruits_s)
        self.basicdata.llikImm = np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred))
        self.basicdata.llikImm_s = np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred_s))

    def RecruitsAllYears(self):
        """
        Calculate total number of recruits for all years when updating
        recruitment for reproduction parameter
        """
        for i in self.basicdata.uYearIndx[1:]:
            popi = self.basicdata.reproPop[i]
            totrec = (popi * self.basicdata.rg) + self.i_s
            self.basicdata.totalRecruits_s[i]  = totrec
            relwc = self.basicdata.relWrpCauchy[self.basicdata.yearRecruitIndx == i]
            sr = (self.basicdata.totalRecruits_s[i] * relwc)
            self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr
        self.basicdata.sessionRecruits_s[0] = .5

    def immUpdateFX(self):
        """
        update ig in mcmc function
        """
        self.immLLikFX()
        pnow = np.sum(self.basicdata.llikImm) + stats.norm.logpdf(self.basicdata.ig,
            self.params.imm_Prior[0], self.params.imm_Prior[1])
        pnew = np.sum(self.basicdata.llikImm_s) + np.log(gamma_pdf(self.i_s,
            self.params.imm_Prior[0], self.params.imm_Prior[1]))
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.ig = self.i_s
            self.basicdata.Npred = self.basicdata.Npred_s.copy()
            self.basicdata.sessionRecruits = self.basicdata.sessionRecruits_s.copy()
            self.basicdata.totalRecruits = self.basicdata.totalRecruits_s.copy()
    ##############################  END IMMIGRATION UPDATE
    ##############################


    ###############     wrapped cauchy parameters for distributing recruits
    #####
    def rparaUpdateFX(self):
        """
        update rg in mcmc function
        """
        self.proposeNewRpara()
        self.wrpCauchyLLikFX()
        pnow = (self.llikWrpC + np.log(gamma_pdf(self.basicdata.rpara[1],
            self.params.rho_Rec_Wrpc_Prior[0], (self.params.rho_Rec_Wrpc_Prior[1]))) +
            np.log(stats.norm.pdf(self.basicdata.rpara[0], self.params.mu_Rec_Wrpc_Prior[0], 
            self.params.mu_Rec_Wrpc_Prior[1])))
        pnew = (self.llikWrpC_s + np.log(gamma_pdf(self.rpara_s[1],
            self.params.rho_Rec_Wrpc_Prior[0], (self.params.rho_Rec_Wrpc_Prior[1]))) +
            np.log(stats.norm.pdf(self.rpara_s[0], self.params.mu_Rec_Wrpc_Prior[0], 
            self.params.mu_Rec_Wrpc_Prior[1])))
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
#        print('self.basicdata.llikWrpC and _s', self.basicdata.llikWrpC, self.basicdata.llikWrpC_s)
#        print('pnow and pnew', pnow, pnew)
#        print('recruit diff', self.basicdata.sessionRecruits - self.basicdata.sessionRecruits_s)
        if rValue > zValue:
            self.basicdata.rpara = self.rpara_s.copy()
            self.basicdata.relWrpCauchy = self.relWrpCauchy_s.copy()
            self.basicdata.Npred = self.basicdata.Npred_s.copy()
            self.basicdata.sessionRecruits = self.basicdata.sessionRecruits_s.copy()

    def proposeNewRpara(self):
        """
        propose new rpara values for next iteration
        """
        self.rpara_s[0] = np.random.normal(self.basicdata.rpara[0], 
                self.params.rpara_Search, size = None)
        self.rpara_s[1] = np.exp(np.random.normal(np.log(self.basicdata.rpara[1]),
                self.params.rpara_Search, size = None))
        self.relWrpCauchy_s = self.basicdata.calcRelWrapCauchy(self.rpara_s)  

    def wrpCauchyLLikFX(self):
        """
        get llik for population growth parameter and proposed
        """
        # get recruits for each session with proposed rs
        self.getSessRecWrpCauchy()
        # get Npred_s
        self.basicdata.Npred_s = self.NpredAllMCMC()
        self.llikWrpC = np.sum(np.log(stats.poisson.pmf(self.basicdata.N[1:], self.basicdata.Npred[1:])))
        self.llikWrpC_s = np.sum(np.log(stats.poisson.pmf(self.basicdata.N[1:], self.basicdata.Npred_s[1:])))

    def getSessRecWrpCauchy(self):
        """
        get number of recruits across years and session
        with specified growth parameter
        """
        for i in self.basicdata.uYearIndx[1:]:
            relwc = self.relWrpCauchy_s[self.basicdata.yearRecruitIndx == i]
            sr = self.basicdata.totalRecruits[i] * relwc
        self.basicdata.sessionRecruits_s = getSessionRecruits(self.basicdata.reproPop, 
            self.basicdata.totalRecruits, 
            self.basicdata.yearRecruitIndx, self.relWrpCauchy_s, 
            self.basicdata.sessionRecruits_s, self.basicdata.sessionJul, self.basicdata.maxSessionJul)


    #############################       END RECRUIT REL WRAP CAUCHY - RPARA UPDATE
    #############################

########            Pickle results to directory
########

class Gibbs(object):
    def __init__(self, mcmcobj, basicdata):
        self.bgibbs = mcmcobj.bgibbs
        self.bPrevgibbs = mcmcobj.bPrevgibbs
        self.rgibbs = mcmcobj.rgibbs
        self.rparagibbs = mcmcobj.rparagibbs
#        self.igibbs = mcmcobj.igibbs
        self.siggibbs = mcmcobj.siggibbs
        self.g0Obsgibbs = mcmcobj.g0Obsgibbs
        self.g0ObsWrpCgibbs = mcmcobj.g0ObsWrpCgibbs
        self.g0Trapgibbs = mcmcobj.g0Trapgibbs
        self.g0TrapWrpCgibbs = mcmcobj.g0TrapWrpCgibbs
        self.pKillgibbs = mcmcobj.pKillgibbs
        self.g0KillWrpCgibbs = mcmcobj.g0KillWrpCgibbs
        self.Ngibbs = mcmcobj.Ngibbs
        self.sumN = mcmcobj.sumN
        self.popIndx = mcmcobj.popIndx
        # data associated with survey data
        self.surveyJul = basicdata.surveyJul
        self.surveyJulYear = basicdata.surveyJulYear
        self.surveyYear = basicdata.surveyYear
        self.surveyMon = basicdata.surveyMon
        self.surveyDay = basicdata.surveyDay
        self.surveyObs = basicdata.surveyObs
        self.surveyRemoved = basicdata.surveyRemoved
        self.surveyX = basicdata.surveyX
        self.surveyY = basicdata.surveyY
#        self.surveyID = basicdata.surveyID
        ## trap data
        self.trapid = basicdata.trapid
        self.trapJul = basicdata.trapJul
        self.trapJulYear = basicdata.trapJulYear
        self.trapYear = basicdata.trapYear
        self.trapMon = basicdata.trapMon
        self.trapDay = basicdata.trapDay
#        self.trapAdult = basicdata.trapAdult
#        self.trapJuvenile = basicdata.trapJuvenile
        self.trapRemoved = basicdata.trapRemoved
        self.trapX = basicdata.trapX
        self.trapY = basicdata.trapY
        # for calc npred
        self.removeTrapSession = basicdata.removeTrapSession
        self.removeHuntSession = basicdata.removeHuntSession
        self.removeDat = basicdata.removeDat
        self.nsession = basicdata.nsession

        self.uYearIndx = basicdata.uYearIndx
        self.reproPeriodIndx = basicdata.reproPeriodIndx
        self.daypiRecruit = basicdata.daypiRecruit
        self.yearRecruitIndx = basicdata.yearRecruitIndx
        self.recruitJulYear = basicdata.recruitJulYear
        self.nRecruitJulYear = basicdata.nRecruitJulYear


########            Pickle Results for Simulation
#######

class SimInData(object):
    def __init__(self, mcmcobj, basicdata):
        self.bgibbs = mcmcobj.bgibbs
        self.bPrevgibbs = mcmcobj.bPrevgibbs
        self.rgibbs = mcmcobj.rgibbs
        self.rparagibbs = mcmcobj.rparagibbs
#        self.igibbs = mcmcobj.igibbs
        self.siggibbs = mcmcobj.siggibbs
        self.g0Obsgibbs = mcmcobj.g0Obsgibbs
        self.g0ObsWrpCgibbs = mcmcobj.g0ObsWrpCgibbs
        self.g0Trapgibbs = mcmcobj.g0Trapgibbs
        self.g0TrapWrpCgibbs = mcmcobj.g0TrapWrpCgibbs
        self.pKillgibbs = mcmcobj.pKillgibbs
        self.g0KillWrpCgibbs = mcmcobj.g0KillWrpCgibbs
        self.Ngibbs = mcmcobj.Ngibbs
        self.reproPopgibbs = mcmcobj.reproPopgibbs

#        self.sumN = mcmcobj.sumN
#        self.popIndx = mcmcobj.popIndx
        # habitat data for simulation
        self.xdat = basicdata.xdat
        self.cellX = basicdata.cellX
        self.cellY = basicdata.cellY



class StarlingData(object):
    def __init__(self, rawdata):
        """
        Pickle structure from manipStarlings.py 
        """
        # data associated with survey data
#        self.surveyPropertyId = rawdata.surveyPropertyId
        self.surveyJul = rawdata.surveyJul
        self.surveyJulYear = rawdata.surveyJulYear
        self.surveyYear = rawdata.surveyYear
        self.surveyMon = rawdata.surveyMon
        self.surveyDay = rawdata.surveyDay
        self.surveyObs = rawdata.surveyObs
        self.surveyRemoved = rawdata.surveyRemoved
        self.surveyX = rawdata.surveyX
        self.surveyY = rawdata.surveyY
        self.nSurvey = rawdata.nSurvey
        self.surveyID = rawdata.surveyID
        self.surveySession = rawdata.surveySession
        ## trap data
        self.trapid = rawdata.trapid
        self.trapJul = rawdata.trapJul
        self.trapJulYear = rawdata.trapJulYear
        self.trapMon = rawdata.trapMon
        self.trapDay = rawdata.trapDay
        self.trapYear = rawdata.trapYear
#        self.trapAdult = rawdata.trapAdult
#        self.trapJuvenile = rawdata.trapJuvenile
        self.trapRemoved = rawdata.trapRemoved
        self.trapX = rawdata.trapX
        self.trapY = rawdata.trapY
        self.trapSession = rawdata.trapSession
        # session data
        self.sessions = rawdata.sessions
        self.sessionYear = rawdata.sessionYear
        self.sessionJulYear = rawdata.sessionJulYear
        self.sessionJul = rawdata.sessionJul
        self.nsession = rawdata.nsession
        self.sessionDayPi = rawdata.sessionDayPi
        self.sessionDates = rawdata.sessionDates
        # property data
#        self.propPropertyid = rawdata.propPropertyid
#        self.propX = rawdata.propX
#        self.propY = rawdata.propX
        # cell data
        self.nCells = rawdata.nCells
###        self.cellX = rawdata.cellX
###        self.cellY = rawdata.cellY
###        self.rowID = rawdata.rowID
###        self.colID = rawdata.colID
        self.extent_xmin = rawdata.extent_xmin
        self.extent_ymin = rawdata.extent_ymin
        self.extent_xmax = rawdata.extent_xmax
        self.extent_ymax = rawdata.extent_ymax
        self.resol = rawdata.resol
        self.nrows = rawdata.nrows
        self.ncols = rawdata.ncols 
###        self.cellSwamp = rawdata.cellSwamp
###        self.cellNatVeg = rawdata.cellNatVeg
        self.extMask = rawdata.extMask
#        self.scaleX_2D = rawdata.scaleX_2D
#        self.scaleY_2D = rawdata.scaleY_2D
        self.logNatVeg_2D = rawdata.logNatVeg_2D
        self.logSwamp_2D = rawdata.logSwamp_2D
#        self.swamp2D = rawdata.swamp2D
#        self.natVeg2D = rawdata.natVeg2D
#        self.cellNdvi = rawdata.cellNdvi
        self.yearRecruitIndx = rawdata.yearRecruitIndx
        self.recruitYear = rawdata.recruitYear
        self.recruitJulYear = rawdata.recruitJulYear
        self.daypiRecruit = rawdata.daypiRecruit
        self.nRecruitJulYear = rawdata.nRecruitJulYear
        self.recruitSession = rawdata.recruitSession
        self.uYearIndx = rawdata.uYearIndx
        # count survey trap session data
        self.minNPresent = rawdata.minNPresent
        self.maxSurvey = rawdata.maxSurvey
        self.maxTrap = rawdata.maxTrap
        self.sessionNSurveys = rawdata.sessionNSurveys
        self.sessionNTraps = rawdata.sessionNTraps
        self.sumTrapRem = rawdata.sumTrapRem

