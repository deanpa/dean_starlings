#!/usr/bin/env python

import flocks
import numpy as np
import os
import pickle

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        ###################################################
        ###################################################
        ## set model name and if first run
        self.modelName = 'Mod1'
        self.firstRun = True
        self.useStarlingNLOCfile = False

        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 1      # number of estimates to save for each parameter
        self.thinrate = 1        # thin rate
        self.burnin = 0             # burn in number of iterations
        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################
        ###################################################

        self.basicfile = 'out_basicdata_' + self.modelName + '.pkl'
        self.birdlocfile = 'out_birdlocdata_' + self.modelName + '.pkl'
        self.gibbsfile = 'out_gibbs_' + self.modelName + '.pkl'

        print('basicfile:', self.basicfile)
        print('birdlocfile:', self.birdlocfile)
        print('gibbsfile:', self.gibbsfile)
        print('firstRun:', self.firstRun)
        print('useStarlingLocData:', self.useStarlingNLOCfile)
        print('ngibbs', self.ngibbs, 'thinrate:', self.thinrate, 'burnin:', self.burnin) 


        ##############
        ###############      Initial values parameters for updating
        self.sigma = 1060.0
        self.sigma_Prior = np.array([1060.0, 200])
        self.sigma_Search = 10
        ## g0 observation parameters and initial values
        self.g0_Obs = 0.7   # 1.124   
        self.g0_ObsPrior = np.array([1.0, 1.0])     # beta priors
        self.g0_ObsSearch = 0.01          
        self.g0_ObsWrpC = np.array([np.pi*2.0, 3])
        ## g0_Obs wrapped cauchy parameters: mu priors from normal, rho from a gamma 
        self.mu_Obs_Wrpc_Prior = np.array([np.pi*2.0, 2.0])    # normal priors mu    
        self.rho_Obs_Wrpc_Prior = np.array([3.0, 0.33333])     # gamma priors for rho    
        self.g0_ObsWrpC_Search = 0.01
        ## g0 Trap parameters and initial values
        self.g0_Trap = 0.4   # 1.124   
        self.g0_TrapPrior = np.array([1.0, 3.0])        # beta priors
        self.g0_TrapSearch = 0.015          
        self.g0_TrapWrpC = np.array([np.pi*2.0, 3.0])
        ## g0_Trap wrapped cauchy parameters: mu priors from normal, rho from a gamma 
        self.mu_Trap_Wrpc_Prior = np.array([np.pi*2.0, 3.0])    # normal priors mu    
        self.rho_Trap_Wrpc_Prior = np.array([3.0, 0.33333])     # gamma priors for rho    
        self.g0_TrapWrpCSearch = 0.015
        ## probaility of kill parameters and initial values
        self.pKill_Max = 0.05        # maximum probability of kill given obseration
        self.pKill_MaxPrior = np.array([1.0, 1.0])       # beta priors
        self.pKill_Search = 0.025
        self.pKill_WrpC = np.array([np.pi*2.0, 3.0])
        ## pKill wrapped cauchy parameters: mu priors from normal, rho from a gamma 
        self.mu_Kill_Wrpc_Prior = np.array([np.pi*2.0, 3.0])    # normal priors mu    
        self.rho_Kill_Wrpc_Prior = np.array([3.0, 0.33333])     # gamma priors for rho    
        self.pKill_WrpCSearch = 0.025
#        ## immigration parameters
#        self.ig =  0.0      #/365.0*7.0
#        self.imm_Prior = np.array([0.1, 0.1])         # gamma prior
#        self.imm_Search = 0.1         # search parameter
        ###################
        ##################      # Reproduction parameters
        self.rg = .30
        self.r_Prior = np.array([3.0, 0.45])     # gamma growth rate priors
        self.r_Search = 0.1
        ## latent initial number of recruits ~ uniform(10, 100)
        self.initialOctPop = 10
        self.initialOct_Prior = np.array([7, 120])
        ## wrp cauchy parameters for distributing recruits
        self.rpara = np.array([np.pi*12/7, 2.0])                # real numbers: normally distributed
        ## Recruitment wrapped cauchy parameters: mu priors from normal, rho from a gamma 
        self.mu_Rec_Wrpc_Prior = np.array([np.pi*2.0, 3.0])    # normal priors mu    
        self.rho_Rec_Wrpc_Prior = np.array([3.0, 0.5])     # gamma priors for rho    
        self.rpara_Search = 0.04        
        ######################################
        ######################################
        ## modify  variables used for habitat model
        self.xdatDictionary = {'prevPresent' : 0, 'scaleEast' : 1, 'scaleNorth' : 2, 'scaleSwamp' : 3,
            'scaleNatVeg' : 4}              #, 'scaleNDVI': 4}
        self.previousPresent = self.xdatDictionary['prevPresent']
        self.scaleEast = self.xdatDictionary['scaleEast']
        self.scaleNorth = self.xdatDictionary['scaleNorth']
        self.scaleSwamp = self.xdatDictionary['scaleSwamp']
        self.scaleNatVeg = self.xdatDictionary['scaleNatVeg']
#        self.scaleNDVI = self.xdatDictionary['scaleNDVI']
        ## array of index values to get variables from 2-d array
        self.xdatIndx = np.array([self.scaleNorth, self.scaleSwamp, 
            self.scaleNatVeg], dtype = int)    
        ## indicator to include NDVI in modelling
#        self.includeNDVI = True

        ######################################
        ######################################
        ## covaritat parameters
        self.b = np.array([-1.20, 0.1, 0.1])
#
        self.b = np.expand_dims(self.b, 1)
        ## beta priors on habitat coefficients
        self.bPrior = np.array([-0.075, 0.03, 0.01])        # informed priors
        self.nbcov = np.shape(self.b)[0]
        self.bPriorSD = np.repeat(1.0, self.nbcov)
        #### previous presence parameter
#        self.bPrev = 0.15
#        self.bPrev = np.expand_dims(self.bPrev, 1)
#        self.bPrevPrior = np.array([.1, 1.0])
        self.nFlocks = 30
        self.nFlocksSample = 30
        self.maxN = 700
        ## set days on which to base population growth rate
        self.reproDays = range(365-92, 365-61)         # 1 Oct - 31 Oct



########            Main function
#######
def main():
    ## Instance of Params
    params = Params()

    ## paths and data to read in
    dataPath = os.getenv('STARLINGPROJDIR', default = '../Data')
    resultsPath = os.getenv('STARLINGPROJDIR', default = '../Results' + params.modelName)
    if not os.path.isdir(resultsPath):
        os.mkdir(resultsPath)
    
    ## initiate basicdata class and object when do not read in previous results
    if params.firstRun:
        # read in the pickled capt and trap data'
        starlingTrapFile = os.path.join(dataPath,'out_manipStarling.pkl')  
        fileobj = open(starlingTrapFile, 'rb')
        starlingdata = pickle.load(fileobj)
        fileobj.close()
        # initiate basicdata from script
        basicdata = flocks.BasicData(starlingdata, params)
    else:
        ## read in pickled results (basicdata) from a previous run
        inputBasicdata = os.path.join(resultsPath, params.basicfile)
        fileobj = open(inputBasicdata, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()

    ##########################################
    if params.useStarlingNLOCfile:
        ## read in pickled starling location and N data class
        inputBirddata = os.path.join(resultsPath, params.birdlocfile)
        fileobj = open(inputBirddata, 'rb')
        birdlocdata = pickle.load(fileobj)
        fileobj.close()
    else:
    ## initiate starlingdata class
        birdlocdata = flocks.BirdLocData(basicdata, params)
    
    ###############################
    ###############################         Comment back in later
    ## initiate mcmc class and object
#    mcmcobj = flocks.MCMC(params, birdlocdata, basicdata)


#    gibbsobj = Gibbs(mcmcobj, basicdata)

#    simindata = SimInData(mcmcobj, basicdata)

    # pickle basic data from present run to be used to initiate new runs
    outBasicdata = os.path.join(resultsPath, params.basicfile)
    fileobj = open(outBasicdata, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    # pickle starling data from present run to be used to initiate new runs
    outstarlingdata = os.path.join(resultsPath, params.birdlocfile)
    fileobj = open(outstarlingdata, 'wb')
    pickle.dump(birdlocdata, fileobj)
    fileobj.close()

    # pickle mcmc results for post processing in gibbsProcessing.py
#    outGibbs = os.path.join(resultsPath, params.gibbsfile)
#    fileobj = open(outGibbs, 'wb')
#    pickle.dump(gibbsobj, fileobj)
#    fileobj.close()

    # pickle mcmc and habitat data for Simulation worky
#    outSimInData = os.path.join(resultsPath,'out_simInData.pkl')
#    fileobj = open(outSimInData, 'wb')
#    pickle.dump(simindata, fileobj)
#    fileobj.close()


if __name__ == '__main__':
    main()
