#!/usr/bin/env python

import os
import numpy as np
import pickle
from flocks import StarlingData
import gdal
import datetime
#from numba import jit



def readFromFile(fname):
    """
    Read data out of the first band of the named file
    """
    ds = gdal.Open(fname)
    band = ds.GetRasterBand(1)
    data = band.ReadAsArray()
    del ds
    return data

def getColsAndRows(easting, northing):
    """
    given an array of eastings and an array of northings
    return the cols and rows
    """
    col = numpy.round((easting - MIN_X) / RES).astype(numpy.int)
    row = numpy.round((MAX_Y - northing) / RES).astype(numpy.int)
    return col, row


class RawData(object):
    def __init__(self, surveyFname, trapFname, propertyFname, starlingpath):
        """
        Object to read in telemetry data
        """

        ###########################################################
        # Run rawdata functions
        self.readInData(surveyFname, trapFname, propertyFname, starlingpath)
        self.removeDups()
        self.removeTrapDuplicates()
        self.makeSessions()
        self.readCovariates(starlingpath)
#        self.make3d_Ndvi()
        self.getCellData()
#        print('survX', self.surveyX[self.surveyX == 0])

    ###############################################################
    ################# Functions

    def readInData(self, surveyFname, trapFname, propertyFname, starlingpath):
        """
        Read in data to be manipulated
        """
        # cat capture data, trap id, week, avail etc
        self.surveydat = np.genfromtxt(surveyFname,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'S10', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8'])
        self.trapdat = np.genfromtxt(trapFname,  delimiter=',', names=True,
            dtype=['S10', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'S10', 'f8', 'f8', 
                    'i8', 'i8', 'i8', 'f8', 'f8'])
        self.propertydat = np.genfromtxt(propertyFname,  delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8', 'f8'])
        # property data
        self.propPropertyid = self.propertydat['propertyid']
        self.propX = self.propertydat['easting']
        self.propY = self.propertydat['northing']
        # trap data

    def removeDups(self):
        """
        remove duplicates and get unique survey data
        """
        allseq = np.arange(len(self.surveydat['propertyid']))
        allreport = self.surveydat['reportid']
        allremove = self.surveydat['removed']
        allobs = self.surveydat['observed']
        ## correct if removed > obs
        tmpRem = allremove.copy()
        tmpObs = allobs.copy()

        allremove[tmpRem > tmpObs] = allobs[tmpRem > tmpObs]
        allobs[tmpRem > tmpObs] = allremove[tmpRem > tmpObs]
#        print('dif', np.min(allobs-allremove))
        allday = self.surveydat['day']
        allmon  = self.surveydat['mon']
        allyear = self.surveydat['year']
        alljul  = self.surveydat['jul']
        alljulYear = self.surveydat['julYear']
        allpropertyid = self.surveydat['propertyid']
        # make empty arrays to fill in
        usurvey = np.unique(allreport)
        self.nSurvey = len(usurvey)
        self.surveyPropertyId = np.zeros(self.nSurvey)
        self.surveyJul = np.zeros(self.nSurvey)
        self.surveyJulYear = np.zeros(self.nSurvey)
        self.surveyYear = np.zeros(self.nSurvey)
        self.surveyMon = np.zeros(self.nSurvey)
        self.surveyDay = np.zeros(self.nSurvey)
        self.surveyObs = np.zeros(self.nSurvey)
        self.surveyRemoved = np.zeros(self.nSurvey)
        self.surveyX = np.zeros(self.nSurvey)
        self.surveyY = np.zeros(self.nSurvey)
        # loop through reports
        cc = 0
        for i in np.arange(self.nSurvey):
            rid =  usurvey[i]
            reportseq = allseq[allreport == rid]
            # if no duplicates for report i
            if np.shape(reportseq) == 1:
                self.surveyPropertyId[i] = allpropertyid[reportseq] 
                self.surveyJul[i] = alljul[reportseq]
                self.surveyYearJul[i] = alljulYear[reportseq]
                self.surveyYear[i] = allyear[reportseq]
                self.surveyMon[i] = allmon[reportseq]
                self.surveyDay[i] = allday[reportseq]
                self.surveyObs[i] = allobs[reportseq]
                self.surveyRemoved[i] = allremove[reportseq]
                propID1 = allpropertyid[reportseq] 
#                print(propID1)
#                print(self.propPropertyid[self.propPropertyid == propID1])
#                print(self.propX[self.propPropertyid == propID1])
                xx1 = self.propX[self.propPropertyid == propID1]
                yy1 = self.propX[self.propPropertyid == propID1]
                self.surveyX[i] = xx1
                self.surveyY[i] = yy1
            # if there are duplicates for report i
            else:
                obsKeep = np.max(allobs[reportseq])
                remKeep = np.max(allremove[reportseq])
                firstseq = reportseq[0]
                pid = allpropertyid[firstseq]
#                print(firstseq)
#                print(pid)
                # fill in data
                self.surveyPropertyId[i] = pid 
                self.surveyJul[i] = alljul[firstseq]
                self.surveyJulYear[i] = alljulYear[firstseq]
                self.surveyYear[i] = allyear[firstseq]
                self.surveyMon[i] = allmon[firstseq]
                self.surveyDay[i] = allday[firstseq]
                self.surveyObs[i] = obsKeep
                self.surveyRemoved[i] = remKeep
                xx = self.propX[self.propPropertyid == pid]
                yy = self.propY[self.propPropertyid == pid]
                self.surveyX[i] = xx
                self.surveyY[i] = yy
        print('nsurvey', self.nSurvey, 'pre N', len(alljul))

    def removeTrapDuplicates(self):
        """
        remove duplicate entries in trap data
        """
        Alltrapid = self.trapdat['trapid']
        AlltrapJul = self.trapdat['jul']
        AlltrapJulYear = self.trapdat['julYear']
        AlltrapMon = self.trapdat['mon']
        AlltrapDay = self.trapdat['day']
        AlltrapYear = self.trapdat['year']
        AlltrapAdult = self.trapdat['adult']
        AlltrapJuvenile = self.trapdat['juvenile']
        AlltrapRemoved = self.trapdat['removed']
        AlltrapX = self.trapdat['easting']
        AlltrapY = self.trapdat['northing']
        # set empty arrays
        self.trapid = np.empty(0, dtype = int)
        self.trapJul = np.empty(0, dtype = int)
        self.trapJulYear = np.empty(0, dtype = int)
        self.trapMon = np.empty(0, dtype = int)
        self.trapDay = np.empty(0, dtype = int)
        self.trapYear = np.empty(0, dtype = int)
        self.trapAdult = np.empty(0, dtype = int)
        self.trapJuvenile = np.empty(0, dtype = int)
        self.trapRemoved = np.empty(0, dtype = int)
        self.trapX = np.empty(0)
        self.trapY = np.empty(0)
        # loop through julian days and get 1 entry per trap per day
        utrapJul = np.unique(AlltrapJul)
#        nutrapJul = len(utrapJul)
        for i in utrapJul:
#            jul_i = nutrapJul[i]
            utrapid_i = np.unique(Alltrapid[AlltrapJul == i])
            julMask = AlltrapJul == i
            # loop thru traps to get one entry per trap
            for j in utrapid_i:
                trapMask = Alltrapid == j
                trapJulMask = trapMask & julMask
                trapid_j = Alltrapid[trapJulMask]
                self.trapid = np.append(self.trapid, j)
                self.trapJul = np.append(self.trapJul, i)
                trapJulYear = AlltrapJulYear[trapJulMask]
                trapMon = AlltrapMon[trapJulMask]
                trapDay = AlltrapDay[trapJulMask]
                trapYear = AlltrapYear[trapJulMask]
                trapAdult = AlltrapAdult[trapJulMask]
                trapJuvenile = AlltrapJuvenile[trapJulMask]
                trapRemoved = AlltrapRemoved[trapJulMask]
                trapX = AlltrapX[trapJulMask]
                trapY = AlltrapY[trapJulMask]
                if not np.isscalar(trapid_j):
                    self.trapJulYear = np.append(self.trapJulYear, trapJulYear[0])
                    self.trapMon = np.append(self.trapMon, trapMon[0])
                    self.trapDay = np.append(self.trapDay, trapDay[0])
                    self.trapYear = np.append(self.trapYear, trapYear[0])
                    self.trapAdult = np.append(self.trapAdult, np.max(trapAdult))
                    self.trapJuvenile = np.append(self.trapJuvenile, np.max(trapJuvenile))
                    self.trapRemoved = np.append(self.trapRemoved, np.max(trapRemoved))
                    self.trapX = np.append(self.trapX, trapX[0])
                    self.trapY = np.append(self.trapY, trapY[0])
                else:
                    self.trapJulYear = np.append(self.trapJulYear, trapJulYear)
                    self.trapMon = np.append(self.trapMon, trapMon)
                    self.trapDay = np.append(self.trapDay, trapDay)
                    self.trapYear = np.append(self.trapYear, trapYear)
                    self.trapAdult = np.append(self.trapAdult, trapAdult)
                    self.trapJuvenile = np.append(self.trapJuvenile, trapJuvenile)
                    self.trapRemoved = np.append(self.trapRemoved, trapRemoved)
                    self.trapX = np.append(self.trapX, trapX)
                    self.trapY = np.append(self.trapY, trapY)
        print('nPre', len(Alltrapid), 'nPost', len(self.trapY))                    

    def makeSessions(self):
        """
        make sessions with survey and traps
        """
        ## make julian from start
        minJul = np.min(self.surveyJul)
        self.surveyJul = self.surveyJul - minJul
        self.trapJul = self.trapJul - minJul
        fullJul = np.append(self.surveyJul, self.trapJul)
        self.sessionJul = np.unique(fullJul)
        self.nsession = len(self.sessionJul)
        fullYear = np.append(self.surveyYear, self.trapYear)
        self.uYears = np.unique(fullYear)
        self.sessions = np.arange(self.nsession)
        self.nSurveys = len(self.surveyJul)
        self.nTrapEvents = len(self.trapJul)
        fullJulYear = np.append(self.surveyJulYear, self.trapJulYear)
        # empty survey and trap arrays to populate
        self.surveySession = np.zeros(self.nSurveys, dtype = int)   
        self.trapSession = np.zeros(self.nTrapEvents, dtype = int)
        # empty session arrays to populate
        self.sessionJulYear = np.zeros(self.nsession, dtype = int)
        self.sessionYear = np.zeros(self.nsession, dtype = int)
        ##
        for i in self.sessions:
            sessJul_i = self.sessionJul[i]
            surveyJulMask = self.surveyJul == sessJul_i
            self.surveySession[surveyJulMask] = i       # len of surveys
            trapJulMask = self.trapJul == sessJul_i
            self.trapSession[trapJulMask] = i           # len trap Events
            ## get year
            fullJulMask = fullJul == sessJul_i
            fullYear_iArray = fullYear[fullJulMask]
            fullJulYear_iArray = fullJulYear[fullJulMask]
            if not np.isscalar(fullYear_iArray):
                fullYear_i = fullYear_iArray[0]
                fullJulYear_i = fullJulYear_iArray[0]
            else:
                fullYear_i = fullYear_iArray
                fullJulYear_i = fullJulYear_iArray
            self.sessionYear[i] = fullYear_i
            self.sessionJulYear[i] = fullJulYear_i
        self.sessionJulYear = self.sessionJulYear - 1

    def readCovariates(self, starlingpath):
        """
        read in extent mask, swamp and native veg covariates
        """
        self.starlingpath = starlingpath
        (self.extent_xmin, self.extent_ymin, self.extent_xmax, 
                self.extent_ymax) = [774000, 6222000, 1082000, 6376000]
        self.resol = 1000.0
        self.nrows = np.int((self.extent_ymax - self.extent_ymin) / self.resol)
        self.ncols = np.int((self.extent_xmax - self.extent_xmin) / self.resol)
        extentTifFname = os.path.join(self.starlingpath,'extentMaskR1000.tif')
        natVegTifFname = os.path.join(self.starlingpath,'NatVeg_mean_R1000_1KmSearch.tif')
        swampTifFname = os.path.join(self.starlingpath,'swamps_mean_R1000_1KmSearch.tif')
        self.extMask = gdal.Open(extentTifFname).ReadAsArray()
        self.natVeg2D = gdal.Open(natVegTifFname).ReadAsArray()
        self.swamp2D = gdal.Open(swampTifFname).ReadAsArray()

    def make3d_Ndvi(self):
        """
        import ndvi tifs and make 2-d array by year (2005 - 2013)
        """
        yrIndx = np.array(['0510', '0610', '0709', '0810', '0910', '1010', '1110', '1210', '1310'])
        self.allYears = np.unique(np.append(self.surveyYear, self.trapYear))
        self.nAllYears = len(self.allYears)
        self.ndvi3D = np.empty((self.nAllYears, self.nrows, self.ncols))
        # loop thru years
        for i in range(self.nAllYears):
            fname = 'ndvi' + yrIndx[i] + '_R1K.tif'
            fname2 = os.path.join(self.starlingpath, fname)
            tmpNdvi = gdal.Open(fname2).ReadAsArray()  #  readFromFile(fname2)
            self.ndvi3D[i] = tmpNdvi

    def getCellData(self):
        """
        make 1-d arrays corresponding to grid cells X and Y
        # also get nat veg and swamp covariates for cells
        """
        self.nCells = np.sum(self.extMask)
        self.cellX = np.empty(self.nCells)
        self.cellY = np.empty(self.nCells)
        self.cellSwamp = np.empty(self.nCells)
        self.cellNatVeg = np.empty(self.nCells)
#        self.cellNdvi = np.empty((self.nCells, self.nAllYears))
        cc = 0
        for i in range((self.nrows)):
            for j in range((self.ncols)):
                if self.extMask[i, j] == 1:
                    self.cellX[cc] = self.extent_xmin + (self.resol * j)
                    self.cellY[cc] = self.extent_ymax - (self.resol * i)
                    self.cellSwamp[cc] = self.swamp2D[i, j] 
                    self.cellNatVeg[cc] = self.natVeg2D[i, j]
#                    self.cellNdvi[cc] = self.ndvi3D[:, i, j]
                    cc += 1
#        print('ndvi', self.cellNdvi[0:30])


########            Main function
#######
def main():
    ## paths and data to read in
    starlingpath = os.getenv('STARLINGPROJDIR', default = '../Data')    
    surveyFile = os.path.join(starlingpath,'survey1.csv')
    trapFile = os.path.join(starlingpath,'trapdat4.csv')
    propertyFile = os.path.join(starlingpath,'property1.csv')
    # initiate rawdata class and object 
    rawdata = RawData(surveyFile, trapFile, propertyFile, starlingpath)

    starlingdata = StarlingData(rawdata)
    # pickle basic data to be used in cats.py
    outManipdata = os.path.join(starlingpath,'out_manipStarling.pkl')
    fileobj = open(outManipdata, 'wb')
    pickle.dump(starlingdata, fileobj)
    fileobj.close()

if __name__ == '__main__':
    main()

