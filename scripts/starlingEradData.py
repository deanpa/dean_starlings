#!/usr/bin/env python

import os
import numpy as np
import gdal
import gdalconst
import osr
from numba import jit

def getXY(xmin, ymax, resol, halfres, row, col):
    """
    get x and y from row and col
    """
    x = (xmin + halfres) + (resol * col)
    y = (ymax - halfres) - (resol * row)
    return(x, y)


@jit
def getEastNorth(nrows, ncols, extMask, multinomProb, easting, northing, nCells, 
        nYears, maxDaysInCell, resol, halfRes, xmin, ymax, nTrapsYear, 
        nSurveyYear):
    """
    draw multinom variates where a max of params.maxDaysInCell
    """
    cc = 0
    for i in range(nYears):
        ## loop thru survey and traps
        for j in range(2):
            trialsRemain = nCells
###            nRemain = nSurveyTraps[j]
            if j == 0:
                nRemain = nTrapsYear[i]
            else:
                nRemain = nSurveyYear[i]
            sumProbsUsed = 0.0
            ## loop thru rows and cols
            for k in range((nrows - 1), -1, -1):
                for l in range(ncols):
                    if extMask[k, l] == 1:
                        multiProb_kl = multinomProb[k, l] 
                        for r in range(maxDaysInCell):
                            prob_kl = multiProb_kl / (1.0 - sumProbsUsed)
                            prob_kl = 1.0 - (1.0 - prob_kl)**nRemain        
                            if nRemain == trialsRemain:
                                X_kl = 1
                            else:
                                X_kl = np.random.binomial(1, prob_kl)
                            if X_kl == 1:
#                            for i in range(X_kl):
                                rEast = (halfRes * np.random.choice([-1, 1]) * 
                                    np.random.random())
                                xx = (xmin + halfRes) + (resol * l)
                            
                                easting[cc] = xx + rEast
                                rNorth = (halfRes * np.random.choice([-1, 1]) * 
                                    np.random.random())
                                yy = (ymax - halfRes) - (resol * k)
                                northing[cc] = yy + rNorth 
                                if j == 0:
                                    easting[cc + 1] = xx + rEast
                                    northing[cc + 1] = yy + rNorth
                                    cc += 2
                                else:                                
                                    ## augment cc
                                    cc += 1
                                nRemain -= 1
                                if nRemain == 0:
                                    break
                        trialsRemain -= 1
                        sumProbsUsed += multiProb_kl
                    if nRemain == 0:
                        break
                if nRemain == 0:
                    break
    return(easting, northing)

class Params(object):
    def __init__(self):
        """
        set parameters for creating data
        """
        self.years = np.arange(1, 4, dtype = int)
#        self.nTrapsYear = 66        #1715   # 20000     # 1715    # 11760
#        self.nSurveyYear = 66        #124  # 4400     #124      #  2216
        self.nTrapsYear = np.array([74, 17, 17])        #1715   # 20000     # 1715    # 11760
        self.nSurveyYear = np.array([74, 17, 17])        #124  # 4400     #124      #  2216
###        self.nSurveyTraps = np.array([self.nTrapsYear, self.nSurveyYear])
        self.maxDaysInCell = 1  #3      # return visits to cell
        self.tNights = 40
        self.epsg = 28350
        self.resol = 500
        self.halfres = np.int(self.resol / 2.0)
        self.dataFName = 'starlingScen4Data.csv'

class EradData(object):
    def __init__(self, params, starlingpath, outDataPath):
        """
        Object to read in, manipulate and read out data.
        """
        self.params = params
        self.starlingpath = starlingpath
        self.outDataPath = outDataPath
        ###########################################################
        # Run makemap functions
        self.defineParameters()
        self.readData()     
        self.makeData()
        (self.easting, self.northing) = getEastNorth(self.nrows, self.ncols, self.extMask, 
            self.multinomProb, self.easting, self.northing, self.nCells, 
            self.nYears, self.params.maxDaysInCell, 
            self.resol, self.halfres, self.xmin, self.ymax, self.params.nTrapsYear,
            self.params.nSurveyYear)


        self.writeTxt()

            
        ##########################################################

    def defineParameters(self):
        """
        set parameters
        """
        sr = osr.SpatialReference()
        sr.ImportFromEPSG(self.params.epsg)
        self.wkt = sr.ExportToWkt()

        (self.xmin, self.ymin, self.xmax,
                self.ymax) = [774000., 6222000., 1082000., 6376000.]
        self.resol = 500.0
        self.halfres = self.resol / 2.0
        self.nrows = np.int((self.ymax - self.ymin) / self.resol)
        self.ncols = np.int((self.xmax - self.xmin) / self.resol)
        self.match_geotrans = [self.xmin, self.resol, 0., self.ymax, 0.,
                                -self.resol]
        self.nYears = len(self.params.years)

    def readData(self):
        """
        read in extent mask, swamp and native veg covariates
        """
#        self.starlingdata = starlingdata
        extentTifFname = os.path.join(self.starlingpath,'extentMaskR500.tif')
        riskFname = os.path.join(self.starlingpath,'riskMap500.tif')
        self.extMask = gdal.Open(extentTifFname).ReadAsArray()
        self.riskMap = gdal.Open(riskFname).ReadAsArray()
        print('ncells', np.sum(self.extMask)) 

    def makeData(self):
        """
        make data arrays easting, northing and survey types
        """
#        ## total surveys and traps in one year
#        self.nTotalSearchYear = self.params.nSurveyYear + self.params.nTrapsYear
#        ## year data to go into csv
#        self.yearData = np.repeat(self.params.years, self.nTotalSearchYear)
#        ## number of search efforts across years
#        self.nTotalSearch = len(self.yearData)
#        print('ntot surv', self.nTotalSearch)
        tmptrapobs = np.array(['STAR_TRAP', 'STAR_OBS'])
###        surveyOneYear = np.repeat('STAR_OBS', self.params.nSurveyYear)
###        # Number of traps with obs survey in one year"
###        nTrapObsOneYear = self.params.nTrapsYear * 2
###        trapObsOneYear = np.tile(tmptrapobs, self.params.nTrapsYear)
###        speciesTmp = np.append(trapObsOneYear, surveyOneYear)

        self.nTotalSearch = np.sum((self.params.nTrapsYear * 2)) + np.sum(self.params.nSurveyYear)
        self.speciesData = np.empty(0, dtype = '<U10')
        self.yearData = np.empty(0, dtype = int)
        ## loop years to get species data
        for i in range(self.nYears):
            nDeviceYear = (self.params.nTrapsYear[i] * 2) + self.params.nSurveyYear[i]
            self.yearData = np.append(self.yearData, np.repeat(self.params.years[i], nDeviceYear))
            sppTmp = np.tile(tmptrapobs, self.params.nTrapsYear[i])
            sppTmp = np.append(sppTmp, np.repeat('STAR_OBS', self.params.nSurveyYear[i]))
            self.speciesData = np.append(self.speciesData, sppTmp)

    
###        ## total surveys and traps in one year
###        self.nTotalSearchYear = self.params.nSurveyYear + nTrapObsOneYear
###        ## year data to go into csv
###        self.yearData = np.repeat(self.params.years, self.nTotalSearchYear)
###        ## number of search efforts across years
###        self.nTotalSearch = len(self.yearData)
        print('ntot surv', self.nTotalSearch)
        print('nyears', self.nYears)
        print('nspp data', len(self.speciesData), 'n year data', len(self.yearData))
#        print('nTrapObs one year props: ', nTrapObsOneYear)
#        print('n survey one year props: ', self.params.nSurveyYear)
###        self.speciesData = np.tile(speciesTmp, self.nYears)
        self.ageSexData = np.repeat('NA', self.nTotalSearch)
        self.tnightsData = np.repeat(self.params.tNights, self.nTotalSearch)     # np.ones(self.nTotalSearch, dtype = int)
        self.easting = np.zeros(self.nTotalSearch)
        self.northing = np.zeros(self.nTotalSearch)
        self.nCells = np.sum(self.extMask)
        ## get sum of relative risk map
        self.sumRR()

    def sumRR(self):
        """
        get sum of RR to calc multinom probs
        """
        self.rrCellSum = 0.0
        self.relRiskRaster = np.zeros_like(self.riskMap)
        self.multinomProb = np.zeros_like(self.riskMap)
        minRiskValue = np.abs(np.min(self.riskMap[self.extMask == 1]))
        ## loop thru cells
        for i in range(self.nrows):
            for j in range(self.ncols):
                if self.extMask[i, j] == 1:
                    rr_ij = (self.riskMap[i, j] + minRiskValue + 1)
                    self.rrCellSum += rr_ij
                    self.relRiskRaster[i, j] = rr_ij
        ## loop thru a second time to get multinomprobs
        for i in range(self.nrows):
            for j in range(self.ncols):
                if self.extMask[i, j] == 1:
                    self.multinomProb[i, j] = (self.relRiskRaster[i, j] / self.rrCellSum)
        relativeRiskFName = os.path.join(self.starlingpath, 'relRiskStarling.tif')
        self.writeTifToFile(self.relRiskRaster, relativeRiskFName, 
            gdt_type = gdalconst.GDT_Float32)

    def writeTifToFile(self, Raster, FName, gdt_type):
        """
        write tif to directory
        """
        # write array to tif in directory
        dst = gdal.GetDriverByName('GTiff').Create(FName, self.ncols,
                    self.nrows, 1, gdt_type)
        dst.SetGeoTransform(self.match_geotrans)
        dst.SetProjection(self.wkt)
        band = dst.GetRasterBand(1)
        band.WriteArray(Raster)
        del dst  # Flush
        print('write RR to file')


    def writeTxt(self):
        """
        write starling survey and trap data to csv file
        """
        # self.params.dataFName
        structured = np.empty((self.nTotalSearch,), dtype = [('Year', np.int), 
            ('Species', 'U10'), ('Easting', np.float), ('Northing', np.float),
            ('Age', 'U10'), ('Sex', 'U10'), ('Tnights', np.int)])
        structured['Year'] = self.yearData
        structured['Species'] = self.speciesData
        structured['Easting'] = self.easting
        structured['Northing'] = self.northing
        structured['Age'] = self.ageSexData
        structured['Sex'] = self.ageSexData
        structured['Tnights'] = self.tnightsData
        tableFname = os.path.join(self.outDataPath, self.params.dataFName)
        headTable = np.array(['Year', 'Species', 'Easting', 'Northing', 
            'Age', 'Sex', 'Tnights'])
        np.savetxt(tableFname, structured, fmt=['%i', '%s', '%.4f', '%.4f', '%s', 
            '%s', '%i'], comments='', delimiter=',', 
            header='Year, Species, Easting, Northing, Age, Sex, Tnights')




########            Main function
#######
def main():
    outDataPath = os.path.join(os.getenv('POFPROJDIR'), 'poa', 'Starlings',
            'starlingData')

    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')

    # initiate params class
    params = Params()
    # initiate rawdata class and object
    eraddata = EradData(params, starlingpath, outDataPath)



if __name__ == '__main__':
    main()

