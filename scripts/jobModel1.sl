#!/bin/bash
#SBATCH -J starling
#SBATCH -A landcare00033 
#SBATCH --mail-type=end
#SBATCH --mail-user=andersond@landcareresearch.co.nz
#SBATCH --time=1:00:00
#SBATCH --mem-per-cpu=25000  
#SBATCH -C wm
#SBATCH -o job-%j.%N.out
#SBATCH -e job-%j.%N.err

#srun startStarlings.py --basicdata=$STARLINGPROJDIR/out_basicdata.pkl --starlingNLOCdata=$STARLINGPROJDIR/out_starlingdata.pkl

#srun startStarlings.py

#srun manipCat.py

srun betaGamma.py
