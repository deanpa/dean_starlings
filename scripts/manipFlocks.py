#!/usr/bin/env python

import os
import numpy as np
import pickle
from flocks import StarlingData
import gdal
import datetime
import calendar
#from numba import jit



def readFromFile(fname):
    """
    Read data out of the first band of the named file
    """
    ds = gdal.Open(fname)
    band = ds.GetRasterBand(1)
    data = band.ReadAsArray()
    del ds
    return data

def getColsAndRows(easting, northing):
    """
    given an array of eastings and an array of northings
    return the cols and rows
    """
    col = numpy.round((easting - MIN_X) / RES).astype(numpy.int)
    row = numpy.round((MAX_Y - northing) / RES).astype(numpy.int)
    return col, row


class RawData(object):
    def __init__(self, surveyFname, trapFname, propertyFname, starlingpath):
        """
        Object to read in telemetry data
        """
###        self.epsg = 28350

        ###########################################################
        # Run rawdata functions
        self.readInData(surveyFname, trapFname, propertyFname, starlingpath)
        self.removeDups()
        self.removeTrapDuplicates()
        self.makePseudoSession()
        self.makeSessions()
        self.getSessionDayPi()
        self.countSurveyInSess()
    
        self.readCovariates(starlingpath)
#        self.make3d_Ndvi()
#        self.makeScaledRasters()
#        self.getCellData()
#        print('survX', self.surveyX[self.surveyX == 0])

    ###############################################################
    ################# Functions

    def readInData(self, surveyFname, trapFname, propertyFname, starlingpath):
        """
        Read in data to be manipulated
        """
        # cat capture data, trap id, week, avail etc
        self.surveydat = np.genfromtxt(surveyFname,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'S10', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8'])
        self.trapdat = np.genfromtxt(trapFname,  delimiter=',', names=True,
            dtype=['S10', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'S10', 'f8', 'f8', 
                    'i8', 'i8', 'i8', 'f8', 'f8'])
        self.propertydat = np.genfromtxt(propertyFname,  delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8', 'f8'])
        # property data
        self.propPropertyid = self.propertydat['propertyid']
        self.propX = self.propertydat['easting']
        self.propY = self.propertydat['northing']
        # trap data



    def removeDups(self):
        """
        remove duplicates and get unique survey data
        """
        allseq = np.arange(len(self.surveydat['propertyid']))
        allreport = self.surveydat['reportid']
        allremove = self.surveydat['removed']
        allobs = self.surveydat['observed']
        ## correct if removed > obs
        tmpRem = allremove.copy()
        tmpObs = allobs.copy()

        allremove[tmpRem > tmpObs] = allobs[tmpRem > tmpObs]
        allobs[tmpRem > tmpObs] = allremove[tmpRem > tmpObs]
#        print('dif', np.min(allobs-allremove))
        allday = self.surveydat['day']
        allmon  = self.surveydat['mon']
        allyear = self.surveydat['year']
        alljul  = self.surveydat['jul']
        alljulYear = self.surveydat['julYear']
        allpropertyid = self.surveydat['propertyid']
        # make empty arrays to fill in
        usurvey = np.unique(allreport)
        self.nSurvey = len(usurvey)
        self.surveyPropertyId = np.zeros(self.nSurvey)
        self.surveyJul = np.zeros(self.nSurvey)
        self.surveyJulYear = np.zeros(self.nSurvey)
        self.surveyYear = np.zeros(self.nSurvey)
        self.surveyMon = np.zeros(self.nSurvey)
        self.surveyDay = np.zeros(self.nSurvey)
        self.surveyObs = np.zeros(self.nSurvey)
        self.surveyRemoved = np.zeros(self.nSurvey)
        self.surveyX = np.zeros(self.nSurvey)
        self.surveyY = np.zeros(self.nSurvey)
        # loop through reports
        cc = 0
        for i in np.arange(self.nSurvey):
            rid =  usurvey[i]
            reportseq = allseq[allreport == rid]
            # if no duplicates for report i
            if np.shape(reportseq) == 1:
                self.surveyPropertyId[i] = allpropertyid[reportseq] 
                self.surveyJul[i] = alljul[reportseq]
                self.surveyYearJul[i] = alljulYear[reportseq]
                self.surveyYear[i] = allyear[reportseq]
                self.surveyMon[i] = allmon[reportseq]
                self.surveyDay[i] = allday[reportseq]
                self.surveyObs[i] = allobs[reportseq]
                self.surveyRemoved[i] = allremove[reportseq]
                propID1 = allpropertyid[reportseq] 
#                print(propID1)
#                print(self.propPropertyid[self.propPropertyid == propID1])
#                print(self.propX[self.propPropertyid == propID1])
                xx1 = self.propX[self.propPropertyid == propID1]
                yy1 = self.propX[self.propPropertyid == propID1]
                self.surveyX[i] = xx1
                self.surveyY[i] = yy1
            # if there are duplicates for report i
            else:
                obsKeep = np.max(allobs[reportseq])
                remKeep = np.max(allremove[reportseq])
                firstseq = reportseq[0]
                pid = allpropertyid[firstseq]
#                print(firstseq)
#                print(pid)
                # fill in data
                self.surveyPropertyId[i] = pid 
                self.surveyJul[i] = alljul[firstseq]
                self.surveyJulYear[i] = alljulYear[firstseq]
                self.surveyYear[i] = allyear[firstseq]
                self.surveyMon[i] = allmon[firstseq]
                self.surveyDay[i] = allday[firstseq]
                self.surveyObs[i] = obsKeep
                self.surveyRemoved[i] = remKeep
                xx = self.propX[self.propPropertyid == pid]
                yy = self.propY[self.propPropertyid == pid]
                self.surveyX[i] = xx
                self.surveyY[i] = yy
        print('nsurvey', self.nSurvey, 'pre N', len(alljul))

    def removeTrapDuplicates(self):
        """
        remove duplicate entries in trap data
        """
        Alltrapid = self.trapdat['trapid']
        AlltrapJul = self.trapdat['jul']
        AlltrapJulYear = self.trapdat['julYear']
        AlltrapMon = self.trapdat['mon']
        AlltrapDay = self.trapdat['day']
        AlltrapYear = self.trapdat['year']
        AlltrapAdult = self.trapdat['adult']
        AlltrapJuvenile = self.trapdat['juvenile']
        AlltrapRemoved = self.trapdat['removed']
        AlltrapX = self.trapdat['easting']
        AlltrapY = self.trapdat['northing']
        # set empty arrays
        self.trapid = np.empty(0, dtype = int)
        self.trapJul = np.empty(0, dtype = int)
        self.trapJulYear = np.empty(0, dtype = int)
        self.trapMon = np.empty(0, dtype = int)
        self.trapDay = np.empty(0, dtype = int)
        self.trapYear = np.empty(0, dtype = int)
        self.trapAdult = np.empty(0, dtype = int)
        self.trapJuvenile = np.empty(0, dtype = int)
        self.trapRemoved = np.empty(0, dtype = int)
        self.trapX = np.empty(0)
        self.trapY = np.empty(0)
        # loop through julian days and get 1 entry per trap per day
        utrapJul = np.unique(AlltrapJul)
#        nutrapJul = len(utrapJul)
        for i in utrapJul:
#            jul_i = nutrapJul[i]
            utrapid_i = np.unique(Alltrapid[AlltrapJul == i])
            julMask = AlltrapJul == i
            # loop thru traps to get one entry per trap
            for j in utrapid_i:
                trapMask = Alltrapid == j
                trapJulMask = trapMask & julMask
                trapid_j = Alltrapid[trapJulMask]
                self.trapid = np.append(self.trapid, j)
                self.trapJul = np.append(self.trapJul, i)
                trapJulYear = AlltrapJulYear[trapJulMask]
                trapMon = AlltrapMon[trapJulMask]
                trapDay = AlltrapDay[trapJulMask]
                trapYear = AlltrapYear[trapJulMask]
                trapAdult = AlltrapAdult[trapJulMask]
                trapJuvenile = AlltrapJuvenile[trapJulMask]
                trapRemoved = AlltrapRemoved[trapJulMask]
                trapX = AlltrapX[trapJulMask]
                trapY = AlltrapY[trapJulMask]
                if not np.isscalar(trapid_j):
                    self.trapJulYear = np.append(self.trapJulYear, trapJulYear[0])
                    self.trapMon = np.append(self.trapMon, trapMon[0])
                    self.trapDay = np.append(self.trapDay, trapDay[0])
                    self.trapYear = np.append(self.trapYear, trapYear[0])
                    self.trapAdult = np.append(self.trapAdult, np.max(trapAdult))
                    self.trapJuvenile = np.append(self.trapJuvenile, np.max(trapJuvenile))
                    self.trapRemoved = np.append(self.trapRemoved, np.max(trapRemoved))
                    self.trapX = np.append(self.trapX, trapX[0])
                    self.trapY = np.append(self.trapY, trapY[0])
                else:
                    self.trapJulYear = np.append(self.trapJulYear, trapJulYear)
                    self.trapMon = np.append(self.trapMon, trapMon)
                    self.trapDay = np.append(self.trapDay, trapDay)
                    self.trapYear = np.append(self.trapYear, trapYear)
                    self.trapAdult = np.append(self.trapAdult, trapAdult)
                    self.trapJuvenile = np.append(self.trapJuvenile, trapJuvenile)
                    self.trapRemoved = np.append(self.trapRemoved, trapRemoved)
                    self.trapX = np.append(self.trapX, trapX)
                    self.trapY = np.append(self.trapY, trapY)
        print('nPre', len(Alltrapid), 'nPost', len(self.trapY))                    


    def makePseudoSession(self):
        """
        make pseudo sessions to distribute new recruits after
             survey and trapping had finished - length 3287
        """
        endDate = datetime.date(2014, 7, 31)
        self.firstDate =  datetime.date(2005, 8, 1)
        datei = self.firstDate 
        self.nRecruitJulYear = (endDate - datei).days +1
        self.recruitSession = np.arange(self.nRecruitJulYear)
#        endDate = datetime.date(2013, 10, 30)
#        datei =  datetime.date(2005, 10, 19) 
        timetuple = datei.timetuple()
        julYri = timetuple.tm_yday
        ## array of within year julian day
        self.recruitJulYear = np.zeros(self.nRecruitJulYear, dtype = int)
        self.recruitJulYear[0] = julYri
        ## array of daypirecruit for wrapped cauchey
        self.daypiRecruit = np.zeros(self.nRecruitJulYear)
        dateYear_i = datei.year
        if calendar.isleap(dateYear_i):
            self.daypiRecruit[0] = julYri / 366. * 2. * np.pi      ## leap year
        else:
            self.daypiRecruit[0] = julYri / 365. * 2. * np.pi      ## non-leap year
        ## array of calendar years
        self.recruitYear = np.zeros(self.nRecruitJulYear, dtype = int)
        self.recruitYear[0] = dateYear_i
        ## indices from 0 to number of years, len all days ~3000
        self.yearRecruitIndx = np.zeros(self.nRecruitJulYear, dtype = int)
        recYrIndx = 0
        carryOn = 1
        ## loop thru all days (see above dates)
        for i in range(1, self.nRecruitJulYear):
            datei = datei + datetime.timedelta(days=1)
            timetuple = datei.timetuple()
            julYri = timetuple.tm_yday
            dateYear_i = datei.year
            if calendar.isleap(dateYear_i):
                self.daypiRecruit[i] = julYri / 366. * 2. * np.pi
            else:
                self.daypiRecruit[i] = julYri / 365. * 2. * np.pi
            self.recruitJulYear[i] = julYri
            self.recruitYear[i] = dateYear_i
            if datei == datetime.date(dateYear_i, 8, 1):
                recYrIndx += 1
            self.yearRecruitIndx[i] = recYrIndx
        self.uYearIndx = np.unique(self.yearRecruitIndx)

    def makeSessions(self):
        """
        make sessions with survey and traps
        """
        ## make julian from start
        minJul = np.min(self.surveyJul)
        # get first survey date
        yy = self.surveyYear[self.surveyJul == minJul]
        mm = self.surveyMon[self.surveyJul == minJul]
        dd = self.surveyDay[self.surveyJul == minJul]
        firstSurveyDate = datetime.date(yy, mm, dd)
        deltaDays = (firstSurveyDate - self.firstDate).days
        print('firstDate', self.firstDate)
        print('firstSurveyDate', firstSurveyDate)
        ## get diff in days from beginning to start survey then add julian days
        self.surveyJul = (self.surveyJul - minJul) + deltaDays
        self.trapJul = (self.trapJul - minJul) + deltaDays 
        fullJul = np.append(self.surveyJul, self.trapJul)
        self.sessionJul = np.unique(fullJul)
        self.nsession = len(self.sessionJul)
        fullYear = np.append(self.surveyYear, self.trapYear)
        self.uYears = np.unique(fullYear)
        self.sessions = np.arange(self.nsession)
        self.nSurvey = len(self.surveyJul)
        self.nTrapEvents = len(self.trapJul)
        fullJulYear = np.append(self.surveyJulYear, self.trapJulYear)
        # empty survey and trap arrays to populate
        self.surveySession = np.zeros(self.nSurvey, dtype = int)   
        self.trapSession = np.zeros(self.nTrapEvents, dtype = int)
        # empty session arrays to populate
        self.sessionJulYear = np.zeros(self.nsession, dtype = int)
        self.sessionYear = np.zeros(self.nsession, dtype = int)
#        self.sessionYearIndx = np.zeros(self.nsession, dtype = int)
        timetuple = firstSurveyDate.timetuple()
        self.sessionJulYear[0] = timetuple.tm_yday
        self.sessionYear[0] = firstSurveyDate.year
        datei = firstSurveyDate
        self.sessionDates = np.empty(self.nsession, dtype = datetime.date)
        self.sessionDates[0] = datei
        ##
        for i in range(1, self.nsession):
            deltaDays = self.sessionJul[i] - self.sessionJul[i - 1]
            datei = datei + datetime.timedelta(days=deltaDays)
            timetuple = datei.timetuple()
            julYri = timetuple.tm_yday
            dateYear_i = datei.year
            self.sessionJulYear[i] = julYri
            self.sessionYear[i] = dateYear_i

            sessJul_i = self.sessionJul[i]
            surveyJulMask = self.surveyJul == sessJul_i
            self.surveySession[surveyJulMask] = i       # len of surveys
            trapJulMask = self.trapJul == sessJul_i
            self.trapSession[trapJulMask] = i           # len trap Events
            self.sessionDates[i] = datei

#            ## get year
#            fullJulMask = fullJul == sessJul_i
#            fullYear_iArray = fullYear[fullJulMask]
#            fullJulYear_iArray = fullJulYear[fullJulMask]
#            if not np.isscalar(fullYear_iArray):
#                fullYear_i = fullYear_iArray[0]
#                fullJulYear_i = fullJulYear_iArray[0]
#            else:
#                fullYear_i = fullYear_iArray
#                fullJulYear_i = fullJulYear_iArray
#            self.sessionYear[i] = fullYear_i
#            self.sessionJulYear[i] = fullJulYear_i
#        self.sessionJulYear = self.sessionJulYear - 1
        self.surveyID = np.arange(self.nSurvey, dtype = int)
#        print('sessJulYr', self.sessionJulYear[:1000])
        
        
        
    def getSessionDayPi(self):
        """
        get daypi values for session julian days for g0 weighting
        """
        self.sessionDayPi = np.zeros(self.nsession)
        for i in range(self.nsession):
            dateYear_i = self.sessionYear[i]
            julYr_i = self.sessionJulYear[i]
            if calendar.isleap(dateYear_i):
                self.sessionDayPi[i] = julYr_i / 366. * 2. * np.pi      ## leap year
            else:
                self.sessionDayPi[i] = julYr_i / 365. * 2. * np.pi      ## non-leap year

    def countSurveyInSess(self):
        """
        (2.1) get max number of reports in a given day
              sets max number of observers per day
        """
        maxSess = 0
        self.maxSurvey = 0
        maxJul = 0
        maxJultrap = 0
        maxSesstrap = 0
        self.maxTrap = 0
        self.sessionNSurveys = np.zeros(self.nsession, dtype = int)
        self.sessionNTraps = np.zeros(self.nsession, dtype = int)
        self.sumTrapRem = np.zeros(self.nsession, dtype = int)
        self.sumShot = np.zeros(self.nsession, dtype = int)
#        self.minNPresent = np.zeros(self.nsession, dtype = int)  # min birds present
        for i in range(self.nsession):
            ## survey data
            survey_i = self.surveyID[self.surveySession == i]
            shot_i = self.surveyRemoved[self.surveySession == i]
            if np.isscalar(survey_i):
                nsurvey_i = 1
            else:
                nsurvey_i = len(survey_i)
            self.sumShot[i] = np.sum(shot_i)
            ## trap data
            trap_i = self.trapX[self.trapSession == i]
            if np.isscalar(trap_i):
                ntrap_i = 1
            else:
                ntrap_i = len(trap_i)
            self.sessionNSurveys[i] = nsurvey_i     # n surveys in sess i
            self.sessionNTraps[i] = ntrap_i         # n traps in sess i
            if nsurvey_i > self.maxSurvey:
                self.maxSurvey = nsurvey_i
                maxSess = i
                maxJul = self.sessionJul[i]
            if ntrap_i > self.maxTrap:
                self.maxTrap = ntrap_i
                maxSesstrap = i
                maxJultrap= self.sessionJul[i]
            surveyObs_i = self.surveyObs[self.surveySession == i]
            if len(surveyObs_i) == 0:
                maxSurveyObs_i = 0
            else:
                maxSurveyObs_i = np.max(surveyObs_i)
            self.sumTrapRem[i] = np.sum(self.trapRemoved[self.trapSession == i])
        self.minNPresent = self.sumShot + self.sumTrapRem
#            self.minNPresent[i] = np.sum(surveyObs_i) + self.sumTrapRem[i]



    def readCovariates(self, starlingpath):
        """
        read in extent mask, swamp and native veg covariates
        """
        self.starlingpath = starlingpath
        (self.extent_xmin, self.extent_ymin, self.extent_xmax, 
                self.extent_ymax) = [774000, 6222000, 1082000, 6376000]
        self.resol = 1000.0
        self.nrows = np.int((self.extent_ymax - self.extent_ymin) / self.resol)
        self.ncols = np.int((self.extent_xmax - self.extent_xmin) / self.resol)
        extentTifFname = os.path.join(self.starlingpath,'extentMaskR1000.tif')
        natVegTifFname = os.path.join(self.starlingpath,'NatVeg_mean_R1000_1KmSearch.tif')
        swampTifFname = os.path.join(self.starlingpath,'swamps_mean_R1000_1KmSearch.tif')
        self.extMask = gdal.Open(extentTifFname).ReadAsArray()
        self.extentMaskBool = self.extMask == 1
        natVeg2D = gdal.Open(natVegTifFname).ReadAsArray()
        swamp2D = gdal.Open(swampTifFname).ReadAsArray()
        self.nCells = np.sum(self.extMask)
        self.logNatVeg_2D = np.zeros_like(self.extMask, dtype = np.float)
        self.logSwamp_2D = np.zeros_like(self.extMask, dtype = np.float)
        self.logNatVeg_2D[self.extentMaskBool] = np.log(natVeg2D[self.extentMaskBool] + 1.0)
        self.logSwamp_2D[self.extentMaskBool] = np.log(swamp2D[self.extentMaskBool] + 1.0)


    def makeScaledRasters(self):
        """
        # make scaled rasters for covariates
        """
        self.scaleX_2D = np.zeros_like(self.extMask, dtype = np.float)
        self.scaleY_2D = np.zeros_like(self.extMask, dtype = np.float)
        self.scaleNatVeg_2D = np.zeros_like(self.extMask, dtype = np.float)
        self.scaleSwamp_2D = np.zeros_like(self.extMask, dtype = np.float)
        ## loop through rasters
        halfres = self.resol / 2.0
        for i in range(self.nrows):
            y = ((self.extent_ymax - halfres) - (self.resol * i)) 
            for j in range(self.ncols):
                x = ((self.extent_xmin + halfres) + (self.resol * j))
                if self.extentMaskBool[i, j]:
                    self.scaleX_2D[i, j] = x
                    self.scaleY_2D[i, j] = y
        meanx = np.mean(self.scaleX_2D[self.extentMaskBool])
        sdx = np.std(self.scaleX_2D[self.extentMaskBool])
        meany = np.mean(self.scaleY_2D[self.extentMaskBool])
        sdy = np.std(self.scaleY_2D[self.extentMaskBool])
        meanveg = np.mean(self.natVeg2D[self.extentMaskBool])
        sdveg = np.std(self.natVeg2D[self.extentMaskBool])
        meanswamp = np.mean(self.swamp2D[self.extentMaskBool])
        sdswamp = np.std(self.swamp2D[self.extentMaskBool])

        self.scaleX_2D[self.extentMaskBool] = ((self.scaleX_2D[self.extentMaskBool] - 
                meanx) / sdx)
        self.scaleY_2D[self.extentMaskBool] = ((self.scaleY_2D[self.extentMaskBool] - 
                meany) / sdy)
        self.scaleNatVeg_2D[self.extentMaskBool] = ((self.natVeg2D[self.extentMaskBool] - 
                meanveg) / sdveg)
        self.scaleSwamp_2D[self.extentMaskBool] = ((self.swamp2D[self.extentMaskBool] - 
                meanswamp) / sdswamp)





    def make3d_Ndvi(self):
        """
        import ndvi tifs and make 2-d array by year (2005 - 2013)
        """
        yrIndx = np.array(['0510', '0610', '0709', '0810', '0910', '1010', '1110', '1210', '1310'])
        self.allYears = np.unique(np.append(self.surveyYear, self.trapYear))
        self.nAllYears = len(self.allYears)
        self.ndvi3D = np.empty((self.nAllYears, self.nrows, self.ncols))
        # loop thru years
        for i in range(self.nAllYears):
            fname = 'ndvi' + yrIndx[i] + '_R1K.tif'
            fname2 = os.path.join(self.starlingpath, fname)
            tmpNdvi = gdal.Open(fname2).ReadAsArray()  #  readFromFile(fname2)
            self.ndvi3D[i] = tmpNdvi

    def getCellData(self):
        """
        make 1-d arrays corresponding to grid cells X and Y
        # also get nat veg and swamp covariates for cells
        """
        self.nCells = np.sum(self.extMask)
        self.cellX = np.empty(self.nCells)
        self.cellY = np.empty(self.nCells)
#        self.rowID = np.empty(self.nCells)
#        self.colID = np.empty(self.nCells)
        self.cellSwamp = np.empty(self.nCells)
        self.cellNatVeg = np.empty(self.nCells)
#        self.cellNdvi = np.empty((self.nCells, self.nAllYears))
        cc = 0
        for i in range((self.nrows)):
            for j in range((self.ncols)):
                if self.extMask[i, j] == 1:
                    self.cellX[cc] = self.extent_xmin + (self.resol * j)
                    self.cellY[cc] = self.extent_ymax - (self.resol * i)
                    self.cellSwamp[cc] = self.swamp2D[i, j] 
                    self.cellNatVeg[cc] = self.natVeg2D[i, j]
#                    self.rowID[cc] = i
#                    self.colID[cc] = j
#                    self.cellNdvi[cc] = self.ndvi3D[:, i, j]
                    cc += 1
#        print('ndvi', self.cellNdvi[0:30])


########            Main function
#######
def main():
    ## paths and data to read in
    starlingpath = os.getenv('STARLINGPROJDIR', default = '../Data')    
    surveyFile = os.path.join(starlingpath,'survey1.csv')
    trapFile = os.path.join(starlingpath,'trapdat4.csv')
    propertyFile = os.path.join(starlingpath,'property1.csv')
    # initiate rawdata class and object 
    rawdata = RawData(surveyFile, trapFile, propertyFile, starlingpath)

    starlingdata = StarlingData(rawdata)
    # pickle basic data to be used in cats.py
    outManipdata = os.path.join(starlingpath,'out_manipStarling.pkl')
    fileobj = open(outManipdata, 'wb')
    pickle.dump(starlingdata, fileobj)
    fileobj.close()

if __name__ == '__main__':
    main()

