#!/usr/bin/env python


#from cats import Gibbs # need this because of pickled results from mcmc
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import datetime
import params
#from starlings import StarlingData

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    """
    e_num = np.exp(-2*rho)
    e_denom = 2 * np.exp(-rho)
    sinh_rho = (1 - e_num) / e_denom
    cosh_rho = (1 + e_num) / e_denom
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc





class ResultsProcessing(object):
    def __init__(self, gibbsobj, params, starlingpath):

        self.params = params
        self.starlingpath = starlingpath
        # get gibbs results
        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)
        self.results = np.hstack([self.gibbsobj.bgibbs, 
                                    np.expand_dims(self.gibbsobj.bPrevgibbs,1), 
                                    np.expand_dims(self.gibbsobj.rgibbs,1), 
                                    self.gibbsobj.rparagibbs,
#                                    np.expand_dims(self.gibbsobj.igibbs,1), 
                                    np.expand_dims(self.gibbsobj.siggibbs,1),
                                    np.expand_dims(self.gibbsobj.g0Obsgibbs,1),
                                    self.gibbsobj.g0ObsWrpCgibbs,
                                    np.expand_dims(self.gibbsobj.g0Trapgibbs,1), 
                                    self.gibbsobj.g0TrapWrpCgibbs,
                                    np.expand_dims(self.gibbsobj.pKillgibbs,1), 
                                    self.gibbsobj.g0KillWrpCgibbs])
        self.npara = self.results.shape[1]
        print('npara', self.npara)
        print('sigma shp', np.shape(self.gibbsobj.siggibbs))
        ######  Run functions
        self.runExploreFunctions()

#class ExploreTrap(object):
#    def __init__(self, starlingtrapdata, starlingpath):
#        self.runExploreFunctions(starlingtrapdata, starlingpath)
    
        #######

    ###### 
    # Functions

    def runExploreFunctions(self):
        """
        wrapper function to run functions in the explore class
        """
        # Run functions
        self.getFullDateArrays()                # get array full date arrays
        self.dataArraysFull()
#        self.plotEffortFX()
        self.makeTableFX()
        self.writeToFileFX()
        self.plotFX()
        self.getNPred_Recruits()
    ##########################################  Functions

    def getFullDateArrays(self):
        """
        get arrays with all days for plotting
        """
        fullJul = np.append(self.gibbsobj.trapJul, self.gibbsobj.surveyJul)
        fullMon = np.append(self.gibbsobj.trapMon, self.gibbsobj.surveyMon)
        fullYear = np.append(self.gibbsobj.trapYear, self.gibbsobj.surveyYear)
        fullDay = np.append(self.gibbsobj.trapDay, self.gibbsobj.surveyDay)
        minJul = np.min(fullJul).astype(int)
        minYear = fullYear[fullJul == minJul].astype(int)
        minMon = fullMon[fullJul == minJul].astype(int)
        minDay = fullDay[fullJul == minJul].astype(int)
        if len(minYear > 1):
            minYear = minYear[0]
            minMon = minMon[0]
            minDay = minDay[0]
        maxJul = np.max(fullJul).astype(int)
        maxYear = fullYear[fullJul == maxJul].astype(int)
        maxMon = fullMon[fullJul == maxJul].astype(int)
        maxDay = fullDay[fullJul == maxJul].astype(int)
        if len(maxYear > 1):
            maxYear = maxYear[0]
            maxMon = maxMon[0]
            maxDay = maxDay[0]
        self.minDate = datetime.date(minYear, minMon, minDay) - datetime.timedelta(days=5)
        self.maxDate = datetime.date(maxYear, maxMon, maxDay) + datetime.timedelta(days=6)
        self.fullDateArray = np.arange(self.minDate, self.maxDate)
        self.fullJulArray = np.arange((minJul-5), (maxJul+6))
        print('minDate:', datetime.date(minYear, minMon, minDay), 
                'maxDate:', datetime.date(maxYear, maxMon, maxDay))


    def dataArraysFull(self):
        """
        populate array for all days in full sequence: 
            n observed; n removed; persondays hunting; traps
        """
        self.nFullJul = len(self.fullJulArray)
        print('nFullJul', self.nFullJul)
        self.effortHunt = np.zeros(self.nFullJul)
        self.effortTrap = np.zeros(self.nFullJul)
        self.nObserved = np.zeros(self.nFullJul)
        self.removeHunt = np.zeros(self.nFullJul)
        self.removeTrap = np.zeros(self.nFullJul)
        # loop thru all jul days
        for i in range(self.nFullJul):
            # get n observed
            jul_i = self.fullJulArray[i]
            self.effortHunt[i] = len(self.gibbsobj.surveyJul[self.gibbsobj.surveyJul == jul_i])
            self.effortTrap[i] = len(self.gibbsobj.trapJul[self.gibbsobj.trapJul == jul_i])
            self.nObserved[i] = (np.sum(self.gibbsobj.surveyObs[self.gibbsobj.surveyJul == jul_i])) #+ 
#                        np.sum(self.gibbsobj.trapRemoved[self.gibbsobj.trapJul == jul_i]))
            self.removeHunt[i] = np.sum(self.gibbsobj.surveyRemoved[self.gibbsobj.surveyJul == jul_i])
            self.removeTrap[i] = np.sum(self.gibbsobj.trapRemoved[self.gibbsobj.trapJul == jul_i]) 
        self.noEffortMask = (self.effortHunt + self.effortTrap) == 0.0
        self.nObserved[self.noEffortMask] = np.nan
        self.removeHunt[self.noEffortMask] = np.nan
        self.removeTrap[self.noEffortMask] = np.nan
        #
        minSurveyJul = np.min(self.gibbsobj.surveyJul)
        maxIndx = minSurveyJul + 75
        rangeArr = np.arange(minSurveyJul, maxIndx).astype(int)
        print('rangeArr', rangeArr)
        julMask = np.in1d(self.gibbsobj.surveyJul, rangeArr)
        print('nobs', self.gibbsobj.surveyObs[julMask], 'jul', self.gibbsobj.surveyJul[julMask])

    def getNPred_Recruits(self):
        """
        from gibbs results, predict Npred and number of recruits by session
        """
        self.getReproductivePopAllYears()        
        self.reproDataAllYears()
        self.NpredAll()

    def getReproductivePopAllYears(self):
        """
        Get reproductive pop for all years
        """
        self.nYear = len(self.gibbsobj.uYearIndx) 
        self.meanN = np.round((self.gibbsobj.sumN / self.ndat), 5)
        Npop = self.meanN.copy()
        self.reproPop = np.zeros(self.nYear)
        ## repro pop in year 0 
        self.reproPop[0] = Npop[0]
        ###################################################
        for i in self.gibbsobj.uYearIndx[1:]:
            nPopi = Npop[self.gibbsobj.reproPeriodIndx == i]
            rPop = np.mean(nPopi)
            self.reproPop[i] = rPop
        print('rPop', self.reproPop)

    def reproDataAllYears(self):
        """
        get number of recruits across years and session
        with specified growth parameter
        """
        self.meanR = np.mean(self.gibbsobj.rgibbs)
        self.meanRPara = np.mean(self.gibbsobj.rparagibbs, axis = 0)
        self.meanImm = 0.0                      ######   np.mean(self.gibbsobj.igibbs)
        self.sessionRecruits = np.zeros(len(self.gibbsobj.yearRecruitIndx))
        self.totalRecruits = np.zeros(self.nYear)
        self.calcRelWrapCauchy()
        for i in self.gibbsobj.uYearIndx:
            self.totalRecruits[i] = (self.reproPop[i] * self.meanR) + self.meanImm
            relwc = self.recruitWrpCauchy[self.gibbsobj.yearRecruitIndx == i]
            sr = (self.totalRecruits[i] * relwc)
#            print('i', i, 'len sr', len(sr), 'len yr rec indx',
#                len(self.basicdata.sessionRecruits[self.basicdata.yearRecruitIndx == i]),
#                'sr', len(sr), 'len relwc', len(relwc))
            self.sessionRecruits[self.gibbsobj.yearRecruitIndx == i] = sr

    def calcRelWrapCauchy(self):
        """
        (7) Calc the rel cauchy value for all sessions for distributing recruits
        """
        self.recruitWrpCauchy = np.zeros(self.gibbsobj.nRecruitJulYear)
        for i in self.gibbsobj.uYearIndx:
            # day pi in year i
            daypiTmp = self.gibbsobj.daypiRecruit[self.gibbsobj.yearRecruitIndx == i]
            # pdf of daypi in year i
            dc = dwrpcauchy(daypiTmp, self.meanRPara[0], self.meanRPara[1])
            # mask of recruit window in year i
            reldc = dc / np.sum(dc)     #  dc.sum()
            self.recruitWrpCauchy[self.gibbsobj.yearRecruitIndx == i] = reldc


    def NpredAll(self):
        """
        calc Npred for all sessions
        """
        self.Npred = np.zeros(self.gibbsobj.nsession)
        self.Npred[0] = self.reproPop[0] + self.sessionRecruits[0]
        self.Npred[1:] = self.meanN[:-1] - self.gibbsobj.removeDat[:-1]
        self.Npred[1:] = self.Npred[1:] + self.sessionRecruits[1 : self.gibbsobj.nsession]
        self.Npred[self.Npred < 0] = .5
    #########################



    def plotEffortFX(self):
        """
        plot n observed, effort, and n removed
        """
        P.figure(figsize=(14, 14))
        P.subplot(5,1,1)
        xDates = self.fullDateArray.astype(datetime.datetime)
        P.plot(xDates, self.nObserved, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.ylabel('Number observed', fontsize = 17)
        # graph effort
        P.subplot(5, 1, 2)
        P.plot(xDates, self.removeHunt, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.ylabel('Number shot', fontsize = 17)
        P.subplot(5, 1, 3)
        P.plot(xDates, self.removeTrap, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.ylabel('Number trapped', fontsize = 17)
        # plot effort
        P.subplot(5, 1, 4)
        P.plot(xDates, self.effortHunt, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.ylabel('Person-days', fontsize = 17)
        # plot effort trap
        P.subplot(5, 1, 5)
        P.plot(xDates, self.effortTrap, color = 'k', linewidth = 3)
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.xlabel('Time (days within years)', fontsize = 17)
        P.ylabel('Trap-days', fontsize = 17)
        EffortFname = os.path.join(self.starlingpath,'starlingEffort.png')
        P.savefig(EffortFname, format='png')

        P.show() 
  
    ###############################  GIBBS RESULTS
#        self.getTrapNightsPerSession()
#        self.NpredAll()
#        self.nPred = np.round(self.nPred, 1)


    @staticmethod
    def quantileFX(a):
        return mquantiles(a, prob=[0.025, 0.5, 0.975], axis = 0)


    def makeTableFX(self):
        resultTable = np.zeros(shape = (4, self.npara))
        resultTable[0:3, :] = np.round(self.quantileFX(self.results), 3)
        resultTable[3, :] = np.round(np.mean(self.results, axis = 0), 3)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])
        self.names = np.array(['Northings', 'Swamp', 'NaturalVeg', 'PrevOccupied', 'PopGrowth',
                'recruit_WrpC_mu', 'recruit_WrpC_rho', 'Sigma', 'g0_Observed', 
                'g0_Obs_WrpC_mu', 'g0_Obs_WrpC_rho', 'g0_Trap', 'g0_Trap_WrpC_mu', 
                'g0_Trap_WrpC_rho', 'Prob_shoot', 'g0_Shoot_WrpC_mu', 'g0_Shoot_WrpC_rho'])
        for i in range(self.npara):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()


    ########            Write result table to file
    ########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
        structured['Names'] = self.names
        tableFname = os.path.join(self.starlingpath, 'summaryTable_Starling_M1.txt')
        np.savetxt(tableFname, structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f'],
                    header='Names Low_CI Median High_CI Mean')



    def plotFX(self):
        plotIndx = np.arange((self.npara + 6), dtype = int)
        nplots = len(plotIndx)  # + 1
        cc = 0
        nfigures = np.int(np.ceil(nplots/6.0))
        ng = np.arange(self.ndat, dtype=int)
#        print(ng)
        #print nfigures, self.ncov
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            #print lastFigure
            for j in range(6):
                P.subplot(2,3,j+1)
                #print cc
                if cc < nplots:
                    if cc < 6:
                        P.plot(ng, self.gibbsobj.Ngibbs[:, cc])
                        P.title('Pop size session ' + str(self.gibbsobj.popIndx[cc]))
                    else:
                        pindx = cc - 6
                        P.plot(ng, self.results[:, plotIndx[pindx]])
                        P.title(self.names[pindx])
                cc = cc + 1
#                if cc == nplots:
#                    P.subplot(2,3, j+2)
#                    P.scatter(self.meanN, self.nPred[0:self.gibbsobj.nsession])
#                    P.ylim(0, 90)
#                    P.xlim(0, 90)
            P.show(block=lastFigure)




    def plotPop_Remove(self):
        """
        plot mean pred population size, recruits, removed and TN for each year
        """
        dates = []
        minDate = datetime.date(2005, 12, 1)
        maxDate = datetime.date(2014, 12, 1)
        for year, month, day in zip(self.gibbsobj.sessionYear,
                            self.gibbsobj.sessionMonth, self.gibbsobj.sessionDay):
            date = datetime.date(int(year), int(month), int(day))
            dates.append(date)
        # make figure
        P.figure(figsize=(14, 6))
        ax = P.gca()
        lns1 = ax.plot(dates, self.nPred[:self.gibbsobj.nsession], label = 'Pop size', color = 'k', linewidth = 3)
        lns2 = ax.plot(dates, self.gibbsobj.removeDat, label = 'Cats removed', color = 'r', linewidth = 3)
        lns3 = ax.plot(dates, self.sessionRecruits, label = 'New recruits', color = 'b', linewidth = 3)
        ax2 = ax.twinx()
        lns4 = ax2.plot(dates, self.TNSession, label = 'Trap nights', color = 'y', linewidth = 3)
        lns = lns1 + lns2 + lns3 + lns4
#        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        ax.set_ylim([0, 80])
        ax.set_xlim(minDate, maxDate)
        ax2.set_ylim(0, 8400)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_ylabel('Number', fontsize = 15)
        ax.set_xlabel('Years (January)', fontsize = 15)
        ax2.set_ylabel('Weekly trap nights', fontsize = 17)
        plotFname = os.path.join(self.predatorpath, 'N_removed_recruit_trapnight.png')
        P.savefig(plotFname, format='png')
        P.show()





def main(params):
    # paths and data to read in
    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')

    # read in the pickled data from manipStarlings
#    starlingTrapFile = os.path.join(starlingpath,'out_manipStarling.pkl')
#    fileobj = open(starlingTrapFile, 'rb')
#    starlingtrapdata = pickle.load(fileobj)
#    fileobj.close()

    # initiate 
    inputGibbs = os.path.join(starlingpath, 'out_gibbs.pkl')
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    resultsobj = ResultsProcessing(gibbsobj, params, starlingpath)

    
if __name__ == '__main__':
    p = params.Params()
    main(p)



