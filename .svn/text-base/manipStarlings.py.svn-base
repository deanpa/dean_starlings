#!/usr/bin/env python

import os
import numpy as np
import pickle
from cats import PredatorTrapData
#from numba import autojit


# special codes for bait type
CODE_E = 0
CODE_F = 1
CODE_RM = 2

# special codes for trap type
CODE_CAGE = 0
CODE_CON = 1
CODE_DOC150 = 2
CODE_DOC250 = 3
CODE_FENN6 = 4
CODE_SX = 1
CODE_TIMMS = 5
CODE_VICTOR = 6

# special codes for months
CODE_jan = 1
CODE_feb = 2
CODE_mar = 3
CODE_apr = 4
CODE_may = 5
CODE_jun = 6
CODE_jul = 7
CODE_aug = 8
CODE_sep = 9
CODE_oct = 10
CODE_nov = 11
CODE_dec = 12

class RawData(object):
    def __init__(self, captFname, trapFname):
        """
        Object to read in cat, trap data
        """
        # cat capture data, trap id, week, avail etc
        self.cat = np.genfromtxt(captFname,  delimiter=',', names=True,
            dtype=['i8', 'S10', 'i8', 'i8', 'S10', 'S10', 'i8','i8','S10', 'S10', 'S10', 'S10','S10', 'S10'])

        self.MONTH = self.cat['MONTH']
        self.DAY = self.cat['DAY']
        self.julianStart = self.cat['ord_day_from_start']
        self.julianYear = self.cat['ord_day_in_year']
        self.nFull = len(self.DAY)
        self.TTYPE = self.cat['TTYPE']
        self.BAIT = self.cat['BAIT']
        self.uTTYPE = np.unique(self.TTYPE)
        self.TRAP = self.cat['TRAP']
        self.uTRAP = np.unique(self.TRAP)
        self.nTTYPE = len(self.uTTYPE)
        self.week = np.ceil(self.julianStart / 7)        
        self.year = self.cat['YEAR']
        self.session = self.week - 1
        self.session = self.session.astype(int)
        self.uSession = np.unique(self.session)
        self.nsession = len(self.uSession)

        print(np.unique(self.MONTH))

        trappedTmp = self.cat['cat_caught']
        # convert strings of captures numbers
        self.trapped = np.zeros(self.nFull, dtype = int)
        self.trapped[trappedTmp == b'TRUE'] = 1

        # number of nights trap is available
        tavail = self.cat['trap_available']
        self.tavail = np.zeros(self.nFull, dtype = int)
        self.tavail[tavail == b'TRUE'] = 1
        self.tavail[self.trapped == 1] = 1

        ###########################################################
        # Trap data - location
        self.trap = np.genfromtxt(trapFname, delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        # number of traps
        self.trapTRAPPre = self.trap['TRAP']
        self.nTrapsPre = len(self.trapTRAPPre)
#        self.trapID = np.arange(self.nTraps)
        # X and Y location of trap data
        self.trapXPre = self.trap['EASTING']
        self.trapYPre = self.trap['NORTHING']

        ###########################################################
        # Run rawdata functions
        self.getBaitNumeric()        
        self.getTrapBaitType()
        self.getCaptTrapid()
        self.getTrapTrapid()
        self.removeDups()
#        self.writeKeep()
        self.dataRemoveDups()
        self.getOneEntryWeek()
        self.oneWeekVariable()
        self.getTrapBaitIndx()
        self.getSessionYearMonth()

    ###############################################################
    ###############################################################
    ################# Functions

    def getBaitNumeric(self):
        """
        get a numeric array for bait type E, F, and RM
        """
        # defaults to 0 = 'E'
        self.baittype = np.zeros(self.nFull, dtype = int)
        self.baittype[self.BAIT == b'"F"'] = CODE_F
        self.baittype[self.BAIT == b'"RM"'] = CODE_RM

    def getTrapBaitType(self):
        """
        get trap-bait type for all data in cat capture data
        """
        # convert string trap types to integers
        self.ttype = np.zeros(self.nFull, dtype = int)
        self.ttype[self.TTYPE == b'"CON"'] = CODE_CON
        self.ttype[self.TTYPE == b'"DOC150"'] = CODE_DOC150
        self.ttype[self.TTYPE == b'"DOC250"'] = CODE_DOC250
        self.ttype[self.TTYPE == b'"FENN6"'] = CODE_FENN6
        self.ttype[self.TTYPE == b'"SX"'] = CODE_SX
        self.ttype[self.TTYPE == b'"TIMMS"'] = CODE_TIMMS
        self.ttype[self.TTYPE == b'"VICTOR"'] = CODE_VICTOR
        self.uttype = np.unique(self.ttype)
        self.DSEQ = np.arange(self.nFull, dtype = int)
        self.ttypebait = np.arange(self.nFull, dtype = int)
        # cycle thru the 7 trap types to get trap-bait types
        self.utraptype = np.unique(self.ttype)      
        self.nutraptype = len(self.utraptype)
        tt = 0
        for i in range(self.nutraptype):
            dseq = self.DSEQ[self.ttype == self.utraptype[i]]
            baitSamp = self.baittype[dseq]
            uBAIT = np.unique(baitSamp)
            nuBAIT = len(uBAIT)
            # cycle thru bait types in traptype i
            for j in range(nuBAIT):                
                bseq = dseq[baitSamp == uBAIT[j]]
                self.ttypebait[bseq] = tt
                tt += 1
            
    def getCaptTrapid(self):
        """
        Get unique trap id by location and ttypebait.
        Make trapid link to the trapdat and captdat
        """
        capt_tid = 0
        self.captTrapID = np.zeros(self.nFull, dtype = int)
        # loop thru traps in capt data
        for i in self.uTRAP:
            uTBT = np.unique(self.ttypebait[self.TRAP == i])
            for j in uTBT:
                dseq2 = self.DSEQ[(self.TRAP == i) & (self.ttypebait == j)]
                self.captTrapID[dseq2] = capt_tid
                capt_tid += 1

    def getTrapTrapid(self):
        """
        get unique trapid for trap data that corresponds to capture data (self.captTrapID)
        """
        self.uTrapid = np.unique(self.captTrapID)       # updated unique traps location combos
        self.nuTrapid = len(self.uTrapid)               # updated number of traps
        self.nuTrap = len(self.uTRAP)                   # number of original traps from data
        # build new trapdata with following variables
        self.trapTRAP = np.empty(1, dtype = int)
        self.trapTrapid = np.empty(1, dtype = int)
        self.trapX = np.empty(1, dtype = int)
        self.trapY = np.empty(1, dtype = int)
        trap_tid = 0
        for i in self.uTRAP:
            utid = np.unique(self.captTrapID[self.TRAP == i])        
            # loop thru trap types in TRAP i
            for j in utid:
                if trap_tid == 0:
                    self.trapTRAP[0] = i
                    self.trapTrapid[0] = trap_tid
                    self.trapX[0] = self.trapXPre[self.trapTRAPPre == i]
                    self.trapY[0] = self.trapYPre[self.trapTRAPPre == i]
                else:
                    self.trapTRAP = np.append(self.trapTRAP,i)
                    self.trapTrapid = np.append(self.trapTrapid, trap_tid)
                    self.trapX = np.append(self.trapX, self.trapXPre[self.trapTRAPPre == i])
                    self.trapY = np.append(self.trapY, self.trapYPre[self.trapTRAPPre == i])
                trap_tid += 1
        self.nTraps = len(self.trapX)


    def removeDups(self):
        """
        Remove duplicate entries for some days - TRAP 2 seems to be the only culprit
        """
        self.keepdat = np.ones(self.nFull, dtype = int)
        # cycle thru 966 traps
        for i in self.uTrapid:
            tday = self.julianStart[self.captTrapID == i]
            uday = np.unique(tday)
            tseq = self.DSEQ[self.captTrapID == i]
            for j in uday:
                dseq = tseq[tday == j]
                # if duplicate entries per day
                if not np.isscalar(dseq):
                    arr1 = np.zeros(len(dseq), dtype = int)
                    arr1[0] = 1
                    self.keepdat[dseq] = arr1

    def dataRemoveDups(self):
        """
        remove duplicates from relevant variables
        """
        keepmask = self.keepdat == 1
        self.week = self.week[keepmask]
        self.captTrapID = self.captTrapID[keepmask]
        self.julianStart = self.julianStart[keepmask]
        self.julianYear = self.julianYear[keepmask]
        self.trapped2 = self.trapped[keepmask]
        self.ttypebait = self.ttypebait[keepmask]
        self.ttype = self.ttype[keepmask]
        self.tavail = self.tavail[keepmask]
        self.year = self.year[keepmask]
        self.MONTH = self.MONTH[keepmask]
        nn = len(self.year)
        self.month = np.zeros(nn)                           #
        self.month[self.MONTH == b'"JANUARY"'] = CODE_jan
        self.month[self.MONTH == b'"FEBRUARY"'] = CODE_feb
        self.month[self.MONTH == b'"MARCH"'] = CODE_mar
        self.month[self.MONTH == b'"APRIL"'] = CODE_apr
        self.month[self.MONTH == b'"MAY"'] = CODE_may
        self.month[self.MONTH == b'"JUNE"'] = CODE_jun
        self.month[self.MONTH == b'"JULY"'] = CODE_jul
        self.month[self.MONTH == b'"AUGUST"'] = CODE_aug
        self.month[self.MONTH == b'"SEPTEMBER'] = CODE_sep
        self.month[self.MONTH == b'"OCTOBER"'] = CODE_oct
        self.month[self.MONTH == b'"NOVEMBER"'] = CODE_nov
        self.month[self.MONTH == b'"DECEMBER"'] = CODE_dec

        print(np.unique(self.month))

#        self.ttype[self.TTYPE == b'"CON"'] = CODE_CON
        


    def getOneEntryWeek(self):
        """
        get a single entry for each trapid for each week
        """
        nn = len(self.week)
        cattrap = np.zeros(nn, dtype = int)
        ndays = np.zeros(nn)
        keepseq = np.zeros(nn, dtype = int)
        dSeq = np.arange(len(self.week), dtype = int)
        cc = 0
        # cycle thru trap id - 966
        for i in self.uTrapid:
            # id 1 of 
            tt = self.ttype[self.captTrapID == i]
            tt = tt[0]
            tSeq = dSeq[self.captTrapID == i]
            weekdat = self.week[self.captTrapID == i]
            uweek = np.unique(weekdat)
            for j in uweek:
                twSeq = tSeq[weekdat == j]
                trapData = self.trapped2[twSeq]
                tavailData = self.tavail[twSeq]
                cattrapWeek = np.sum(trapData)
                tavailWeek = np.sum(tavailData)
                # if kill trap, follow sequence
                if (tt > CODE_CAGE) & (tt < CODE_VICTOR):
                    # if no animals were caught 
                    if cattrapWeek == 0:
                        ndayWeek = tavailWeek
                    # if animals caught, reduce availability
                    else:
                        ndayWeek = tavailWeek/2/cattrapWeek
                # if it was a live trap, reduce availability if animals were caught
                else:
                    ndayWeek = tavailWeek - (.5 * cattrapWeek)
                # populate arrays for trap-week combos
                cattrap[cc] = cattrapWeek
                ndays[cc] = ndayWeek
                keepseq[cc] = twSeq[0]
                cc += 1
        self.cattrap = cattrap[:cc]
        self.nDays = ndays[:cc]
        self.keepseq = keepseq[:cc]


    def oneWeekVariable(self):
        """
        remove duplicates from relevant variables
        """
        self.week = self.week[self.keepseq]
        self.captTrapID = self.captTrapID[self.keepseq]
        self.julianStart = self.julianStart[self.keepseq]
        self.julianYear = self.julianYear[self.keepseq]
        self.ttypebait = self.ttypebait[self.keepseq]
        self.ttype = self.ttype[self.keepseq]
        self.year = self.year[self.keepseq]
        self.month = self.month[self.keepseq]
#        self.nDays = self.nDays[self.keepseq]

    def getSessionYearMonth(self):
        """
        get month and year for each session
        """
        uweek = np.unique(self.week)
        nSess = len(uweek)
        self.sessionYear = np.zeros(nSess, dtype = int)
        self.sessionMonth = np.zeros(nSess, dtype = int)
        for i in range(nSess):
            yy = self.year[self.week == uweek[i]]
            self.sessionYear[i] = yy[0]
            mm = self.month[self.week == uweek[i]]
            self.sessionMonth[i] = mm[0]
        print(self.sessionYear)
        print(self.sessionMonth)


    def getTrapBaitIndx(self):
        """
        make bait-trap type indexing array for g0 updating
        Length is number of traps (self.nTraps)
        """
        # create trapBaitIndx for applying g0
        # g0 are organised in order of unique trapbait combos
        # indices vary from 0 to nTrapBait, not trapbait numbers
        utid = np.unique(self.trapTrapid) 
        self.trapBaitIndx = np.zeros(self.nTraps, dtype = int)
        # cycle through the traps in trap data
        for i in range(self.nTraps):
            arr1 = self.ttypebait[self.captTrapID == self.trapTrapid[i]]
            self.trapBaitIndx[i] = arr1[0]             # len number of rows in trap-cell matrix
#        print('trapTrapid', self.trapTrapid)
#        print('trapBaitIndx', self.trapBaitIndx)        
#        print('cattrap', np.sum(self.cattrap))
#        print('max ndays and len', np.max(self.ndays), len(self.ndays))
#        print('unique self.ttypebait', np.unique(self.ttypebait))
#        print('len self.ttypebait', len(self.ttypebait))
#        print('len self.trapTrapid', len(self.trapTrapid))


########            Pickle results to directory
########
                
########            Main function
#######
def main():
    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    # paths and data to read in
    predatorDatFile = os.path.join(predatorpath,'GAOS_data2.csv')
    trapDatFile = os.path.join(predatorpath,'trapDat2.csv')
    # initiate rawdata class and object 
    rawdata = RawData(predatorDatFile, trapDatFile)

    predatortrapdata = PredatorTrapData(rawdata)
    # pickle basic data to be used in cats.py
    outManipdata = os.path.join(predatorpath,'out_manipdata.pkl')
    fileobj = open(outManipdata, 'wb')
    pickle.dump(predatortrapdata, fileobj)
    fileobj.close()

if __name__ == '__main__':
    main()

