#!/usr/bin/env python

#import sys
#import defol
import numpy as np

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        ##############
        ###############      Initial values parameters for updating
        self.sigma = 240.0
        self.sigma_s = 244.0
        self.sigma_mean = 255
        self.sigma_sd = 40
        self.sigma_search_sd = .5

#        self.g0 = 0.044
#        self.g0_s = 0.043
        self.g0_alpha = 1.0    # beta prior for g0
        self.g0_beta = 15.667    # beta prior for g0
        self.g0Sd = 0.002          # search parameter for g0
        self.g0Multiplier = np.array([0, -.25, .25])
        self.g0MultiPrior = np.array([0, 4.0])

        self.ig =  12/365.0*7.0
        self.i_s =  11/365.0*7.0
        self.imm_shape = .1
        self.imm_scale = .1

        ###################
        ##################      # Reproduction parameters
        self.rg = 0.8
        self.rs = 0.7
        self.r_shape = 3.6     # gamma growth rate priors
        self.r_scale = 2

        # wrp cauchy parameters for distributing recruits
        self.rpara = np.array([np.pi/3, .1])                # real numbers: normally distributed
        self.rpara_s =  np.array([np.pi/3.9, .09])          # greater than 0: gamma distributed
        # wrapped cauchy mu priors from a gamma distribution
        self.mu_wrpc_Priors = np.array([np.pi*2, 3])    
        self.rho_wrpc_Priors = np.array([.05, .05])     # gamma priors for rho    
        
        ######################################
        ######################################
        # modify  variables used for habitat model
        self.xdatDictionary = {'scaleEast' : 0, 'scaleNorth' : 1, 'scaleTuss' : 2}
        self.scaleEast = self.xdatDictionary['scaleEast']
        self.scaleNorth = self.xdatDictionary['scaleNorth']
        self.scaleTuss = self.xdatDictionary['scaleTuss']
        # array of index values to get variables from 2-d array
        self.xdatIndx = np.array([self.scaleEast, self.scaleNorth, self.scaleTuss], dtype = int)    
        ######################################
        ######################################

        self.b = np.array([-.01, .05, -.05])
        # beta priors on habitat coefficients
        self.bPrior = 0.0
        self.bPriorSD = np.sqrt(100)
        self.nbcov = len(self.b)
        self.maxN = 300

        # set days on which to base population growth rate
        self.reproDays = range(365-61, 365-30)         # 1 Nov - 30 Nov
        # set days for low and high g0 values
        self.g0LowDays = np.array([365-92, 365-15])         # days >= 1 Oct to 15 Dec
        self.g0HighDays = np.array([1, 150])                # jan - may

        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 2500         # number of estimates to save for each parameter
        self.thinrate = 5          # thin rate
        self.burnin = 25000             # burn in number of iterations
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################


