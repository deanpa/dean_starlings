#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P
import params
from scipy.special import gamma
from scipy.special import gammaln
from starlings import dwrpcauchy
from numba import jit

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf


@jit
def multiTrial(nTrials, probs):
    rmVari = np.random.multinomial(nTrials, probs)
    return(rmVari)

probs = np.array([.01, .25, .35, .1, .20, .09])
print(multiTrial(10, probs))





class G0Test(object):
    def __init__(self, params):

        self.params = params
#        print(self.params.g0_alpha)
        self.gg = np.arange(.001,.25, .001)
#        self.dpdf = stats.beta.pdf(self.gg, self.params.g0_alpha, self.params.g0_beta)
#        P.plot(self.gg, self.dpdf)
#        P.show()
#        P.cla()
        (a, b) = np.array([1.722, 84.388889])
#        (a, b) = self.getBetaFX(.1, .01)
        print("a and b", a, b)
        print('mean beta', (a/(a+b)))
        print('mode', (a-1.0) / (a + b -2.0))
        self.dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, self.dpdf)
        P.show()
        P.cla()

    @staticmethod
    def getBetaFX(mg0, g0Sd):
        a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
        b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
        return (a, b)

class gammaTest(object):
    def __init__(self):

#        self.params = params
#        print(self.params.g0_alpha)
#        self.rr = np.arange(.005, 1.5, .001)
        self.rr = np.arange(.01, 3., .01)
        sh = 3.0  #2.0  #10. #.05    #3.0      #3.0    #1.2      #.5 #4  #1.    #3.8
        sc = 1/3        #0.45   #.65     #.01  #1/.05  #0.25       #1.0/5.0
        rate = 1.0/sc    #0.5  #40.0     #1.1      #.5 #4  #1.      #2
        self.dpdf = gamma_pdf(self.rr, sh, (sc))           # use shape and scale: sc = 1/rate !!!
#        self.dpdf = stats.gamma.pdf(self.rr, sh, (1/sc))

#        print(self.dpdf)
#        print(self.dpdf2)

        randg = np.random.gamma(sh, sc, size = 2000)          # use shape and scale
#        print('randg', randg)
        print("mean and var rand", np.mean(randg), np.var(randg))
        print("parameter calc mean", sh * sc)
        print("max LL r", self.rr[self.dpdf==np.max(self.dpdf)], ((sh-1)*sc))
        print("max pdf", self.dpdf[self.dpdf==np.max(self.dpdf)])
        
        P.figure(0)
        P.subplot(1,2,1)
        P.hist(randg)
        P.subplot(1,2,2)
        P.plot(self.rr, self.dpdf)
#        P.ylim(0, 20.0)
        P.show()
        P.cla()

class wrpCTest(object):
    def __init__(self):
        days = np.arange(365)
        aa = days/364 * 2. 
        aa = np.arange(0, (np.pi*2), .05)
        mu = 1.4    # np.pi*2
        rho = 3.   #.7
        dwrpc = dwrpcauchy(aa, mu, rho)
        st_dwrpc = (dwrpc/np.max(dwrpc)) * 0.05
        print(dwrpc)
        print('st_dwrpc', st_dwrpc)
        P.figure(0)
#        P.plot(aa, dwrpc)
        P.plot(aa, st_dwrpc, color = 'r')
#        P.ylim(0, 1.0)
        P.show()
        P.cla()

class normalBeta(object):
    def __init__(self):
        M = .4
        V = .01
        dd = logit(np.arange(.001, 1, .001))
        ddpdf = stats.norm.pdf(dd, logit(M), .5)
        l_mu = np.random.normal(logit(M), .05, 2000)
        mu = inv_logit(l_mu)
        print(np.std(mu))
        P.figure(0)
        P.subplot(1,2,1)
        P.hist(mu)
        P.xlim(0,1.0)
        P.subplot(1,2,2)
        P.plot(dd, ddpdf)
#        P.ylim(0, 2.0)
        P.show()
        P.cla()
        

class Poisson(object):
    def __init__(self):
        dat = 91
        datGammaLn = gammaln(dat + 1)
        ll = 178.002847241 

        lpmf = self.ln_poissonPMF(dat, datGammaLn, ll)

        pois = stats.poisson.logpmf(dat, ll)
        print('lpmf', lpmf, 'pois', pois)

    def ln_poissonPMF(self, dat, datGammaLn, ll):
        """
        calc log pmf for poission
        """
        ln_ppmf = ((dat * np.log(ll)) - ll) - datGammaLn
        return(ln_ppmf)







########            Main function
#######
def main():

#    G0Test(params)
#    gammaTest()
#    wrpCTest()
#    normalBeta()
#    Poisson()


if __name__ == '__main__':
    main()

