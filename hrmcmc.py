#!/usr/bin/env python

import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
from numba import autojit
import params
import pickle


def thProbFX(tt, debug = False):
    tt2 = np.exp(tt)
    tt3 = tt2/np.sum(tt2)
    return(tt3)

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

@autojit
def matrixsub(arr1, arr2):
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat


def multinomial_pmf(probs, counts):
    probssum = probs.sum()
    if probssum < 0.999 or probssum > 1.0001:
        raise ValueError("probs must sum to 1")
    if probs.size != counts.size:
        raise ValueError("probs and counts must be the same size")
    return gammaln(counts.sum() + 1.0) - gammaln(counts + 1.0).sum() + np.sum(np.log(probs)*counts)


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    """
    e_num = np.exp(-2*rho)
    e_denom = 2 * np.exp(-rho)
    sinh_rho = (1 - e_num) / e_denom
    cosh_rho = (1 + e_num) / e_denom
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc

class HrData(object):
    def __init__(self, starlingteledata):
        """
        Object to read in cat, trap and covariate-cell data
        Import updatable params from params
        """

        self.starlingteledata = starlingteledata


class StarlingTeleData(object):
    def __init__(self, rawdata):
        """
        Pickle structure from manipTele.py 
        """
        # data associated with telemetry data
        self.id = rawtele.id


########            Main function
#######
def main():

    starlingpath = os.getenv('STARLINGPROJDIR', default = '.')
    # paths and data to read in



    # initiate basicdata class and object when do not read in previous results
    if basicfile is None:
        # read in the pickled capt and trap data from 'manipCats.py'
        predatorTrapFile = os.path.join(predatorpath,'out_manipdata.pkl')  
        fileobj = open(predatorTrapFile, 'rb')
        predatortrapdata = pickle.load(fileobj)
        fileobj.close()
        # initiate basicdata from script
        basicdata = BasicData(predatortrapdata, covDatFile, params)
    # pickled basicdata specified in system variable to be imported
    else:
        # read in pickled results (basicdata) from a previous run
        inputBasicdata = os.path.join(predatorpath, basicfile)
        fileobj = open(inputBasicdata, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()

    if predfile is None:
        # initiate predatordata class
        predatordata = PredatorData(basicdata, params)
    # read in pickled predatordata class
    else:
        inputPreddata = os.path.join(predatorpath, predfile)
        fileobj = open(inputPreddata, 'rb')
        predatordata = pickle.load(fileobj)
        fileobj.close()

if __name__ == '__main__':
    main()
