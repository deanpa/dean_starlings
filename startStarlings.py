#!/usr/bin/env python

import sys
import starlings
import params
import optparse
from manipStarling import StarlingData

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--basicdata", dest="basicdata", help="Input basicdata pkl")
        p.add_option("--starlingNLOCdata", dest="starlingNLOCdata", help="Input starlingNLOCdata pkl")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)

cmdargs = CmdArgs()
print(cmdargs.basicdata)
print(cmdargs.starlingNLOCdata)

params = params.Params()

if len(sys.argv) == 1:
    # no basicdata
    starlings.main(params)

else:
    #then passed the name of the pickle file on the command line
    starlings.main(params, cmdargs.basicdata, cmdargs.starlingNLOCdata)



